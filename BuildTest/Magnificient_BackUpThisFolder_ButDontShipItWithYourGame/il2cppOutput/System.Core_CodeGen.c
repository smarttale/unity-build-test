﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000003 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000004 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000005 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000007 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000008 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DistinctIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000009 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000A TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000B TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000C System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000D System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000E System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000000F TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000010 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000011 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000012 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000013 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000014 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000015 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000016 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000017 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000018 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000019 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001A System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000001B System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000001C System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000001D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000001E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001F System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000020 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000021 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000022 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000023 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000024 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000025 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000026 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000027 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000028 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000029 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000002A System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x0000002B System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000002C System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000002D System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000002E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000002F System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000030 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000031 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000032 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000033 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000034 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000035 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000036 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000037 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000038 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000039 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x0000003A System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000003B System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x0000003C TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000003D System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::.ctor(System.Int32)
// 0x0000003E System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.IDisposable.Dispose()
// 0x0000003F System.Boolean System.Linq.Enumerable_<DistinctIterator>d__68`1::MoveNext()
// 0x00000040 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::<>m__Finally1()
// 0x00000041 TSource System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000042 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.Reset()
// 0x00000043 System.Object System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.get_Current()
// 0x00000044 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000045 System.Collections.IEnumerator System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000046 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x00000047 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x00000048 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x00000049 System.Void System.Linq.Set`1::Resize()
// 0x0000004A System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x0000004B System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x0000004C TElement[] System.Linq.Buffer`1::ToArray()
// 0x0000004D System.Void System.Collections.Generic.BitHelper::.ctor(System.Int32*,System.Int32)
extern void BitHelper__ctor_m7D010B426005B48D8C03139EB9248E927293BF3F (void);
// 0x0000004E System.Void System.Collections.Generic.BitHelper::.ctor(System.Int32[],System.Int32)
extern void BitHelper__ctor_m8F1FB12BE0C611E499E3E03A151A52FE02A468C5 (void);
// 0x0000004F System.Void System.Collections.Generic.BitHelper::MarkBit(System.Int32)
extern void BitHelper_MarkBit_mADF9FABA576B57C4BB5BD616ACAA28C613045802 (void);
// 0x00000050 System.Boolean System.Collections.Generic.BitHelper::IsMarked(System.Int32)
extern void BitHelper_IsMarked_m2DB877B8A728264DE8A02E20BDFA4C886CAB4345 (void);
// 0x00000051 System.Int32 System.Collections.Generic.BitHelper::ToIntArrayLength(System.Int32)
extern void BitHelper_ToIntArrayLength_mAFCF611F107D1DDA8FA965FA24A027715E1D3991 (void);
// 0x00000052 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000053 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000054 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000055 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000056 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000057 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000058 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000059 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x0000005A System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000005B System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000005C System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x0000005D System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000005E System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005F System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000060 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000061 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000062 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000063 System.Void System.Collections.Generic.HashSet`1::IntersectWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000064 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000065 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000066 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x00000067 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000068 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000069 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000006A System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000006B System.Void System.Collections.Generic.HashSet`1::IntersectWithHashSetWithSameEC(System.Collections.Generic.HashSet`1<T>)
// 0x0000006C System.Void System.Collections.Generic.HashSet`1::IntersectWithEnumerable(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000006D System.Int32 System.Collections.Generic.HashSet`1::InternalIndexOf(T)
// 0x0000006E System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x0000006F System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000070 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000071 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000072 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x00000073 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000074 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000075 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
// 0x00000076 System.Void System.Collections.Generic.ICollectionDebugView`1::.ctor(System.Collections.Generic.ICollection`1<T>)
// 0x00000077 T[] System.Collections.Generic.ICollectionDebugView`1::get_Items()
static Il2CppMethodPointer s_methodPointers[119] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BitHelper__ctor_m7D010B426005B48D8C03139EB9248E927293BF3F,
	BitHelper__ctor_m8F1FB12BE0C611E499E3E03A151A52FE02A468C5,
	BitHelper_MarkBit_mADF9FABA576B57C4BB5BD616ACAA28C613045802,
	BitHelper_IsMarked_m2DB877B8A728264DE8A02E20BDFA4C886CAB4345,
	BitHelper_ToIntArrayLength_mAFCF611F107D1DDA8FA965FA24A027715E1D3991,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[119] = 
{
	0,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	64,
	136,
	32,
	30,
	21,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[32] = 
{
	{ 0x02000004, { 46, 4 } },
	{ 0x02000005, { 50, 9 } },
	{ 0x02000006, { 61, 7 } },
	{ 0x02000007, { 70, 10 } },
	{ 0x02000008, { 82, 11 } },
	{ 0x02000009, { 96, 9 } },
	{ 0x0200000A, { 108, 12 } },
	{ 0x0200000B, { 123, 1 } },
	{ 0x0200000C, { 124, 2 } },
	{ 0x0200000D, { 126, 11 } },
	{ 0x0200000E, { 137, 8 } },
	{ 0x02000010, { 145, 4 } },
	{ 0x02000012, { 149, 34 } },
	{ 0x02000014, { 183, 2 } },
	{ 0x02000015, { 185, 2 } },
	{ 0x06000003, { 0, 10 } },
	{ 0x06000004, { 10, 10 } },
	{ 0x06000005, { 20, 5 } },
	{ 0x06000006, { 25, 5 } },
	{ 0x06000007, { 30, 1 } },
	{ 0x06000008, { 31, 2 } },
	{ 0x06000009, { 33, 3 } },
	{ 0x0600000A, { 36, 3 } },
	{ 0x0600000B, { 39, 3 } },
	{ 0x0600000C, { 42, 1 } },
	{ 0x0600000D, { 43, 3 } },
	{ 0x0600001D, { 59, 2 } },
	{ 0x06000022, { 68, 2 } },
	{ 0x06000027, { 80, 2 } },
	{ 0x0600002D, { 93, 3 } },
	{ 0x06000032, { 105, 3 } },
	{ 0x06000037, { 120, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[187] = 
{
	{ (Il2CppRGCTXDataType)2, 18784 },
	{ (Il2CppRGCTXDataType)3, 14744 },
	{ (Il2CppRGCTXDataType)2, 18785 },
	{ (Il2CppRGCTXDataType)2, 18786 },
	{ (Il2CppRGCTXDataType)3, 14745 },
	{ (Il2CppRGCTXDataType)2, 18787 },
	{ (Il2CppRGCTXDataType)2, 18788 },
	{ (Il2CppRGCTXDataType)3, 14746 },
	{ (Il2CppRGCTXDataType)2, 18789 },
	{ (Il2CppRGCTXDataType)3, 14747 },
	{ (Il2CppRGCTXDataType)2, 18790 },
	{ (Il2CppRGCTXDataType)3, 14748 },
	{ (Il2CppRGCTXDataType)2, 18791 },
	{ (Il2CppRGCTXDataType)2, 18792 },
	{ (Il2CppRGCTXDataType)3, 14749 },
	{ (Il2CppRGCTXDataType)2, 18793 },
	{ (Il2CppRGCTXDataType)2, 18794 },
	{ (Il2CppRGCTXDataType)3, 14750 },
	{ (Il2CppRGCTXDataType)2, 18795 },
	{ (Il2CppRGCTXDataType)3, 14751 },
	{ (Il2CppRGCTXDataType)2, 18796 },
	{ (Il2CppRGCTXDataType)3, 14752 },
	{ (Il2CppRGCTXDataType)3, 14753 },
	{ (Il2CppRGCTXDataType)2, 14664 },
	{ (Il2CppRGCTXDataType)3, 14754 },
	{ (Il2CppRGCTXDataType)2, 18797 },
	{ (Il2CppRGCTXDataType)3, 14755 },
	{ (Il2CppRGCTXDataType)3, 14756 },
	{ (Il2CppRGCTXDataType)2, 14671 },
	{ (Il2CppRGCTXDataType)3, 14757 },
	{ (Il2CppRGCTXDataType)3, 14758 },
	{ (Il2CppRGCTXDataType)2, 18798 },
	{ (Il2CppRGCTXDataType)3, 14759 },
	{ (Il2CppRGCTXDataType)2, 18799 },
	{ (Il2CppRGCTXDataType)3, 14760 },
	{ (Il2CppRGCTXDataType)3, 14761 },
	{ (Il2CppRGCTXDataType)2, 14680 },
	{ (Il2CppRGCTXDataType)2, 18800 },
	{ (Il2CppRGCTXDataType)3, 14762 },
	{ (Il2CppRGCTXDataType)2, 14683 },
	{ (Il2CppRGCTXDataType)2, 18801 },
	{ (Il2CppRGCTXDataType)3, 14763 },
	{ (Il2CppRGCTXDataType)2, 14686 },
	{ (Il2CppRGCTXDataType)2, 14688 },
	{ (Il2CppRGCTXDataType)2, 18802 },
	{ (Il2CppRGCTXDataType)3, 14764 },
	{ (Il2CppRGCTXDataType)3, 14765 },
	{ (Il2CppRGCTXDataType)3, 14766 },
	{ (Il2CppRGCTXDataType)2, 14693 },
	{ (Il2CppRGCTXDataType)3, 14767 },
	{ (Il2CppRGCTXDataType)3, 14768 },
	{ (Il2CppRGCTXDataType)2, 14705 },
	{ (Il2CppRGCTXDataType)2, 18803 },
	{ (Il2CppRGCTXDataType)3, 14769 },
	{ (Il2CppRGCTXDataType)3, 14770 },
	{ (Il2CppRGCTXDataType)2, 14707 },
	{ (Il2CppRGCTXDataType)2, 18677 },
	{ (Il2CppRGCTXDataType)3, 14771 },
	{ (Il2CppRGCTXDataType)3, 14772 },
	{ (Il2CppRGCTXDataType)2, 18804 },
	{ (Il2CppRGCTXDataType)3, 14773 },
	{ (Il2CppRGCTXDataType)3, 14774 },
	{ (Il2CppRGCTXDataType)2, 14717 },
	{ (Il2CppRGCTXDataType)2, 18805 },
	{ (Il2CppRGCTXDataType)3, 14775 },
	{ (Il2CppRGCTXDataType)3, 14776 },
	{ (Il2CppRGCTXDataType)3, 13432 },
	{ (Il2CppRGCTXDataType)3, 14777 },
	{ (Il2CppRGCTXDataType)2, 18806 },
	{ (Il2CppRGCTXDataType)3, 14778 },
	{ (Il2CppRGCTXDataType)3, 14779 },
	{ (Il2CppRGCTXDataType)2, 14729 },
	{ (Il2CppRGCTXDataType)2, 18807 },
	{ (Il2CppRGCTXDataType)3, 14780 },
	{ (Il2CppRGCTXDataType)3, 14781 },
	{ (Il2CppRGCTXDataType)3, 14782 },
	{ (Il2CppRGCTXDataType)3, 14783 },
	{ (Il2CppRGCTXDataType)3, 14784 },
	{ (Il2CppRGCTXDataType)3, 13438 },
	{ (Il2CppRGCTXDataType)3, 14785 },
	{ (Il2CppRGCTXDataType)2, 18808 },
	{ (Il2CppRGCTXDataType)3, 14786 },
	{ (Il2CppRGCTXDataType)3, 14787 },
	{ (Il2CppRGCTXDataType)2, 14742 },
	{ (Il2CppRGCTXDataType)2, 18809 },
	{ (Il2CppRGCTXDataType)3, 14788 },
	{ (Il2CppRGCTXDataType)3, 14789 },
	{ (Il2CppRGCTXDataType)2, 14744 },
	{ (Il2CppRGCTXDataType)2, 18810 },
	{ (Il2CppRGCTXDataType)3, 14790 },
	{ (Il2CppRGCTXDataType)3, 14791 },
	{ (Il2CppRGCTXDataType)2, 18811 },
	{ (Il2CppRGCTXDataType)3, 14792 },
	{ (Il2CppRGCTXDataType)3, 14793 },
	{ (Il2CppRGCTXDataType)2, 18812 },
	{ (Il2CppRGCTXDataType)3, 14794 },
	{ (Il2CppRGCTXDataType)3, 14795 },
	{ (Il2CppRGCTXDataType)2, 14759 },
	{ (Il2CppRGCTXDataType)2, 18813 },
	{ (Il2CppRGCTXDataType)3, 14796 },
	{ (Il2CppRGCTXDataType)3, 14797 },
	{ (Il2CppRGCTXDataType)3, 14798 },
	{ (Il2CppRGCTXDataType)3, 13449 },
	{ (Il2CppRGCTXDataType)2, 18814 },
	{ (Il2CppRGCTXDataType)3, 14799 },
	{ (Il2CppRGCTXDataType)3, 14800 },
	{ (Il2CppRGCTXDataType)2, 18815 },
	{ (Il2CppRGCTXDataType)3, 14801 },
	{ (Il2CppRGCTXDataType)3, 14802 },
	{ (Il2CppRGCTXDataType)2, 14775 },
	{ (Il2CppRGCTXDataType)2, 18816 },
	{ (Il2CppRGCTXDataType)3, 14803 },
	{ (Il2CppRGCTXDataType)3, 14804 },
	{ (Il2CppRGCTXDataType)3, 14805 },
	{ (Il2CppRGCTXDataType)3, 14806 },
	{ (Il2CppRGCTXDataType)3, 14807 },
	{ (Il2CppRGCTXDataType)3, 14808 },
	{ (Il2CppRGCTXDataType)3, 13455 },
	{ (Il2CppRGCTXDataType)2, 18817 },
	{ (Il2CppRGCTXDataType)3, 14809 },
	{ (Il2CppRGCTXDataType)3, 14810 },
	{ (Il2CppRGCTXDataType)2, 18818 },
	{ (Il2CppRGCTXDataType)3, 14811 },
	{ (Il2CppRGCTXDataType)3, 14812 },
	{ (Il2CppRGCTXDataType)3, 14813 },
	{ (Il2CppRGCTXDataType)3, 14814 },
	{ (Il2CppRGCTXDataType)3, 14815 },
	{ (Il2CppRGCTXDataType)2, 18819 },
	{ (Il2CppRGCTXDataType)3, 14816 },
	{ (Il2CppRGCTXDataType)2, 14812 },
	{ (Il2CppRGCTXDataType)2, 14804 },
	{ (Il2CppRGCTXDataType)3, 14817 },
	{ (Il2CppRGCTXDataType)3, 14818 },
	{ (Il2CppRGCTXDataType)2, 14803 },
	{ (Il2CppRGCTXDataType)2, 18820 },
	{ (Il2CppRGCTXDataType)3, 14819 },
	{ (Il2CppRGCTXDataType)3, 14820 },
	{ (Il2CppRGCTXDataType)3, 14821 },
	{ (Il2CppRGCTXDataType)2, 18821 },
	{ (Il2CppRGCTXDataType)2, 18822 },
	{ (Il2CppRGCTXDataType)3, 14822 },
	{ (Il2CppRGCTXDataType)3, 14823 },
	{ (Il2CppRGCTXDataType)2, 14816 },
	{ (Il2CppRGCTXDataType)3, 14824 },
	{ (Il2CppRGCTXDataType)2, 14817 },
	{ (Il2CppRGCTXDataType)2, 18823 },
	{ (Il2CppRGCTXDataType)2, 14828 },
	{ (Il2CppRGCTXDataType)2, 14826 },
	{ (Il2CppRGCTXDataType)2, 18824 },
	{ (Il2CppRGCTXDataType)3, 14825 },
	{ (Il2CppRGCTXDataType)2, 18825 },
	{ (Il2CppRGCTXDataType)3, 14826 },
	{ (Il2CppRGCTXDataType)3, 14827 },
	{ (Il2CppRGCTXDataType)3, 14828 },
	{ (Il2CppRGCTXDataType)2, 14836 },
	{ (Il2CppRGCTXDataType)3, 14829 },
	{ (Il2CppRGCTXDataType)3, 14830 },
	{ (Il2CppRGCTXDataType)2, 14839 },
	{ (Il2CppRGCTXDataType)3, 14831 },
	{ (Il2CppRGCTXDataType)1, 18826 },
	{ (Il2CppRGCTXDataType)2, 14838 },
	{ (Il2CppRGCTXDataType)3, 14832 },
	{ (Il2CppRGCTXDataType)1, 14838 },
	{ (Il2CppRGCTXDataType)1, 14836 },
	{ (Il2CppRGCTXDataType)2, 18827 },
	{ (Il2CppRGCTXDataType)2, 14838 },
	{ (Il2CppRGCTXDataType)2, 14841 },
	{ (Il2CppRGCTXDataType)2, 14840 },
	{ (Il2CppRGCTXDataType)2, 14856 },
	{ (Il2CppRGCTXDataType)3, 14833 },
	{ (Il2CppRGCTXDataType)2, 14842 },
	{ (Il2CppRGCTXDataType)3, 14834 },
	{ (Il2CppRGCTXDataType)2, 14842 },
	{ (Il2CppRGCTXDataType)3, 14835 },
	{ (Il2CppRGCTXDataType)3, 14836 },
	{ (Il2CppRGCTXDataType)3, 14837 },
	{ (Il2CppRGCTXDataType)3, 14838 },
	{ (Il2CppRGCTXDataType)3, 14839 },
	{ (Il2CppRGCTXDataType)3, 14840 },
	{ (Il2CppRGCTXDataType)3, 14841 },
	{ (Il2CppRGCTXDataType)3, 14842 },
	{ (Il2CppRGCTXDataType)3, 14843 },
	{ (Il2CppRGCTXDataType)2, 14837 },
	{ (Il2CppRGCTXDataType)3, 14844 },
	{ (Il2CppRGCTXDataType)2, 14852 },
	{ (Il2CppRGCTXDataType)2, 14863 },
	{ (Il2CppRGCTXDataType)2, 14865 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	119,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	32,
	s_rgctxIndices,
	187,
	s_rgctxValues,
	NULL,
};

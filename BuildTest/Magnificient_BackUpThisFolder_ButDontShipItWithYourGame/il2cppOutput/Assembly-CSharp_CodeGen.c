﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Foobar::Start()
extern void Foobar_Start_mC2C0FF354B7E8828CA311DD340D0233E747DE015 (void);
// 0x00000002 System.Void Foobar::Update()
extern void Foobar_Update_mE1A07DCC513447E00A318A50FF6CBFF7B3668F1F (void);
// 0x00000003 System.Void Foobar::.ctor()
extern void Foobar__ctor_mFC97970F08CB1F0CA0B3BE29EBFB84D861C2B29C (void);
// 0x00000004 System.Void InteractableManager::Awake()
extern void InteractableManager_Awake_m9C79B1D8F3B8E8D2EC725074DE1054EF463CF274 (void);
// 0x00000005 System.Void InteractableManager::Update()
extern void InteractableManager_Update_mE726A8640AAAD5787CAC4A16BE5B5D6703D46F73 (void);
// 0x00000006 System.Void InteractableManager::ChangeColor()
extern void InteractableManager_ChangeColor_mF26CE67D625AE6B65DAF50A1C9B696412D3D28DF (void);
// 0x00000007 System.Void InteractableManager::ChangeImage()
extern void InteractableManager_ChangeImage_mE7AB89591F2D9B46E125EB21CACF34B93036E8F3 (void);
// 0x00000008 System.Void InteractableManager::ActualImageChange(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Sprite>)
extern void InteractableManager_ActualImageChange_m74197666775C8A99DF4D16BBE6F681F5BA3371C2 (void);
// 0x00000009 System.Void InteractableManager::ChangeText(System.String)
extern void InteractableManager_ChangeText_mF037BAA1D832799BA1842AF6CB845ECC772EC7DA (void);
// 0x0000000A System.Void InteractableManager::.ctor()
extern void InteractableManager__ctor_m3B9D60AAF0CC7BB1D02CB02C00DFB5369F7E48B5 (void);
// 0x0000000B System.Void DirectoryExtensions::CreateRecursiveDirectory(System.String,System.String)
extern void DirectoryExtensions_CreateRecursiveDirectory_m616B47F56FD0227B3CDB970FF82BC9FAA39F5B7D (void);
// 0x0000000C System.String StringExtensions::get__ApplicationDataPath()
extern void StringExtensions_get__ApplicationDataPath_mD82CC9508C8BC5A07D98BCBDABE53CA334B4084D (void);
// 0x0000000D System.String StringExtensions::ConvertFullPathToProjectPath(System.String)
extern void StringExtensions_ConvertFullPathToProjectPath_mD68E61EBEC2359E0AC53ACFCDE5ACB0643B92C80 (void);
// 0x0000000E System.String StringExtensions::GetPathAfterSubfolder(System.String,System.String)
extern void StringExtensions_GetPathAfterSubfolder_m408DE54CF98EDFC05B8D0B67C823753ED6D393CF (void);
// 0x0000000F System.String StringExtensions::ConvertToFullPath(System.String)
extern void StringExtensions_ConvertToFullPath_mD55399BE7BE78C4A1ECA5AD77239B2AD8B81D5FA (void);
// 0x00000010 System.String StringExtensions::RemoveFinalFolderSlash(System.String)
extern void StringExtensions_RemoveFinalFolderSlash_mB4C8B2FC60EF19DD546AB78AF74A7835589712E2 (void);
// 0x00000011 System.Void StringExtensions::.cctor()
extern void StringExtensions__cctor_m31FF6560F3D55DCD941AA3255C9ED39518CCF6BF (void);
// 0x00000012 System.Void KilianHelper::CreateRecursiveDirectory(System.String,KilianHelper_PathType)
extern void KilianHelper_CreateRecursiveDirectory_mDBB5374D1C853E079C14C9413F2B6102EB223562 (void);
// 0x00000013 System.Void TestingTest::Awake()
extern void TestingTest_Awake_m53B51F30831105F0F7BF6A841D4EEA732586B8F7 (void);
// 0x00000014 System.Void TestingTest::.ctor()
extern void TestingTest__ctor_m581809B9FD46E5111CC06565B081F2FA6A9E46B4 (void);
static Il2CppMethodPointer s_methodPointers[20] = 
{
	Foobar_Start_mC2C0FF354B7E8828CA311DD340D0233E747DE015,
	Foobar_Update_mE1A07DCC513447E00A318A50FF6CBFF7B3668F1F,
	Foobar__ctor_mFC97970F08CB1F0CA0B3BE29EBFB84D861C2B29C,
	InteractableManager_Awake_m9C79B1D8F3B8E8D2EC725074DE1054EF463CF274,
	InteractableManager_Update_mE726A8640AAAD5787CAC4A16BE5B5D6703D46F73,
	InteractableManager_ChangeColor_mF26CE67D625AE6B65DAF50A1C9B696412D3D28DF,
	InteractableManager_ChangeImage_mE7AB89591F2D9B46E125EB21CACF34B93036E8F3,
	InteractableManager_ActualImageChange_m74197666775C8A99DF4D16BBE6F681F5BA3371C2,
	InteractableManager_ChangeText_mF037BAA1D832799BA1842AF6CB845ECC772EC7DA,
	InteractableManager__ctor_m3B9D60AAF0CC7BB1D02CB02C00DFB5369F7E48B5,
	DirectoryExtensions_CreateRecursiveDirectory_m616B47F56FD0227B3CDB970FF82BC9FAA39F5B7D,
	StringExtensions_get__ApplicationDataPath_mD82CC9508C8BC5A07D98BCBDABE53CA334B4084D,
	StringExtensions_ConvertFullPathToProjectPath_mD68E61EBEC2359E0AC53ACFCDE5ACB0643B92C80,
	StringExtensions_GetPathAfterSubfolder_m408DE54CF98EDFC05B8D0B67C823753ED6D393CF,
	StringExtensions_ConvertToFullPath_mD55399BE7BE78C4A1ECA5AD77239B2AD8B81D5FA,
	StringExtensions_RemoveFinalFolderSlash_mB4C8B2FC60EF19DD546AB78AF74A7835589712E2,
	StringExtensions__cctor_m31FF6560F3D55DCD941AA3255C9ED39518CCF6BF,
	KilianHelper_CreateRecursiveDirectory_mDBB5374D1C853E079C14C9413F2B6102EB223562,
	TestingTest_Awake_m53B51F30831105F0F7BF6A841D4EEA732586B8F7,
	TestingTest__ctor_m581809B9FD46E5111CC06565B081F2FA6A9E46B4,
};
static const int32_t s_InvokerIndices[20] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1836,
	122,
	23,
	134,
	4,
	0,
	1,
	0,
	0,
	3,
	324,
	23,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	20,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};

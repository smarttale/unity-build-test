﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void DelegateList`1::.ctor(System.Func`2<System.Action`1<T>,System.Collections.Generic.LinkedListNode`1<System.Action`1<T>>>,System.Action`1<System.Collections.Generic.LinkedListNode`1<System.Action`1<T>>>)
// 0x00000002 System.Int32 DelegateList`1::get_Count()
// 0x00000003 System.Void DelegateList`1::Add(System.Action`1<T>)
// 0x00000004 System.Void DelegateList`1::Remove(System.Action`1<T>)
// 0x00000005 System.Void DelegateList`1::Invoke(T)
// 0x00000006 System.Void DelegateList`1::Clear()
// 0x00000007 DelegateList`1<T> DelegateList`1::CreateWithGlobalCache()
// 0x00000008 System.Void ListWithEvents`1::add_OnElementAdded(System.Action`1<T>)
// 0x00000009 System.Void ListWithEvents`1::remove_OnElementAdded(System.Action`1<T>)
// 0x0000000A System.Void ListWithEvents`1::add_OnElementRemoved(System.Action`1<T>)
// 0x0000000B System.Void ListWithEvents`1::remove_OnElementRemoved(System.Action`1<T>)
// 0x0000000C System.Void ListWithEvents`1::InvokeAdded(T)
// 0x0000000D System.Void ListWithEvents`1::InvokeRemoved(T)
// 0x0000000E T ListWithEvents`1::get_Item(System.Int32)
// 0x0000000F System.Void ListWithEvents`1::set_Item(System.Int32,T)
// 0x00000010 System.Int32 ListWithEvents`1::get_Count()
// 0x00000011 System.Boolean ListWithEvents`1::get_IsReadOnly()
// 0x00000012 System.Void ListWithEvents`1::Add(T)
// 0x00000013 System.Void ListWithEvents`1::Clear()
// 0x00000014 System.Boolean ListWithEvents`1::Contains(T)
// 0x00000015 System.Void ListWithEvents`1::CopyTo(T[],System.Int32)
// 0x00000016 System.Collections.Generic.IEnumerator`1<T> ListWithEvents`1::GetEnumerator()
// 0x00000017 System.Int32 ListWithEvents`1::IndexOf(T)
// 0x00000018 System.Void ListWithEvents`1::Insert(System.Int32,T)
// 0x00000019 System.Boolean ListWithEvents`1::Remove(T)
// 0x0000001A System.Void ListWithEvents`1::RemoveAt(System.Int32)
// 0x0000001B System.Collections.IEnumerator ListWithEvents`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000001C System.Void ListWithEvents`1::.ctor()
// 0x0000001D System.Void MonoBehaviourCallbackHooks::add_OnUpdateDelegate(System.Action`1<System.Single>)
extern void MonoBehaviourCallbackHooks_add_OnUpdateDelegate_mF0A7D9A91F2790E2B5E411D31ECC9B5B4B3DCD5D (void);
// 0x0000001E System.Void MonoBehaviourCallbackHooks::remove_OnUpdateDelegate(System.Action`1<System.Single>)
extern void MonoBehaviourCallbackHooks_remove_OnUpdateDelegate_m00215EA5426D369E8D7C7198819192B433D0E99C (void);
// 0x0000001F System.Void MonoBehaviourCallbackHooks::Awake()
extern void MonoBehaviourCallbackHooks_Awake_mB89D60777166A27B306C9145D100DBF6625F4B9D (void);
// 0x00000020 System.Void MonoBehaviourCallbackHooks::Update()
extern void MonoBehaviourCallbackHooks_Update_mCC7B226851A48F80003FEB67801607356597F5A6 (void);
// 0x00000021 System.Void MonoBehaviourCallbackHooks::.ctor()
extern void MonoBehaviourCallbackHooks__ctor_mD71C42E2866DE9CE172E4E61EB0214A9D4BFEB6E (void);
// 0x00000022 System.Void UnityEngine.ResourceManagement.ChainOperation`2::.ctor()
// 0x00000023 System.String UnityEngine.ResourceManagement.ChainOperation`2::get_DebugName()
// 0x00000024 System.Void UnityEngine.ResourceManagement.ChainOperation`2::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000025 System.Void UnityEngine.ResourceManagement.ChainOperation`2::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000026 System.Void UnityEngine.ResourceManagement.ChainOperation`2::Execute()
// 0x00000027 System.Void UnityEngine.ResourceManagement.ChainOperation`2::OnWrappedCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x00000028 System.Void UnityEngine.ResourceManagement.ChainOperation`2::Destroy()
// 0x00000029 System.Single UnityEngine.ResourceManagement.ChainOperation`2::get_Progress()
// 0x0000002A System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::.ctor()
// 0x0000002B System.String UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::get_DebugName()
// 0x0000002C System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000002D System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x0000002E System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::Execute()
// 0x0000002F System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::OnWrappedCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x00000030 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::Destroy()
// 0x00000031 System.Single UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::get_Progress()
// 0x00000032 System.Action`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception> UnityEngine.ResourceManagement.ResourceManager::get_ExceptionHandler()
extern void ResourceManager_get_ExceptionHandler_m1594AC5FC006CC770B2A0E15C739EB845A9C8306 (void);
// 0x00000033 System.Void UnityEngine.ResourceManagement.ResourceManager::set_ExceptionHandler(System.Action`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception>)
extern void ResourceManager_set_ExceptionHandler_m2CD5AEF214219E421AF7BAA9057274336F72C6AF (void);
// 0x00000034 System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String> UnityEngine.ResourceManagement.ResourceManager::get_InternalIdTransformFunc()
extern void ResourceManager_get_InternalIdTransformFunc_mB5CBFF62F538F2E9387BC31FA5DF4FDBC39813AB (void);
// 0x00000035 System.Void UnityEngine.ResourceManagement.ResourceManager::set_InternalIdTransformFunc(System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String>)
extern void ResourceManager_set_InternalIdTransformFunc_m8F62669979EAECF8ED0E7FA2CDE7D219826A250B (void);
// 0x00000036 System.String UnityEngine.ResourceManagement.ResourceManager::TransformInternalId(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceManager_TransformInternalId_m5B3836837396CC2CBF26C9D1B77809A73A835669 (void);
// 0x00000037 System.Int32 UnityEngine.ResourceManagement.ResourceManager::get_OperationCacheCount()
extern void ResourceManager_get_OperationCacheCount_m4E3CB779AE01BD54B04544B20A761188B7C3DC40 (void);
// 0x00000038 System.Int32 UnityEngine.ResourceManagement.ResourceManager::get_InstanceOperationCount()
extern void ResourceManager_get_InstanceOperationCount_m7FF092E82B500F42DD10FBB204AA6D5113BF979D (void);
// 0x00000039 System.Void UnityEngine.ResourceManagement.ResourceManager::AddUpdateReceiver(UnityEngine.ResourceManagement.IUpdateReceiver)
extern void ResourceManager_AddUpdateReceiver_mD35433816B5B3C8F3D986F8E9DE70872341CE4F3 (void);
// 0x0000003A System.Void UnityEngine.ResourceManagement.ResourceManager::RemoveUpdateReciever(UnityEngine.ResourceManagement.IUpdateReceiver)
extern void ResourceManager_RemoveUpdateReciever_m53AAAD4F31B434164DCF15F4CF787FD104F490D8 (void);
// 0x0000003B UnityEngine.ResourceManagement.Util.IAllocationStrategy UnityEngine.ResourceManagement.ResourceManager::get_Allocator()
extern void ResourceManager_get_Allocator_m617C32C69B3AF34BA88127E5EE31B8B2731AE685 (void);
// 0x0000003C System.Void UnityEngine.ResourceManagement.ResourceManager::set_Allocator(UnityEngine.ResourceManagement.Util.IAllocationStrategy)
extern void ResourceManager_set_Allocator_m605775DC52E410670040187897F0C17ED45BC6FB (void);
// 0x0000003D System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider> UnityEngine.ResourceManagement.ResourceManager::get_ResourceProviders()
extern void ResourceManager_get_ResourceProviders_m0B0336D1A403DDDB560EB832375B0205AC2B914F (void);
// 0x0000003E UnityEngine.Networking.CertificateHandler UnityEngine.ResourceManagement.ResourceManager::get_CertificateHandlerInstance()
extern void ResourceManager_get_CertificateHandlerInstance_m2E6389B8CA321D6850C1EF792F7F30CB85E6AD53 (void);
// 0x0000003F System.Void UnityEngine.ResourceManagement.ResourceManager::set_CertificateHandlerInstance(UnityEngine.Networking.CertificateHandler)
extern void ResourceManager_set_CertificateHandlerInstance_m58A06D27892464F5DCB0CF162A4491F63D75BD95 (void);
// 0x00000040 System.Void UnityEngine.ResourceManagement.ResourceManager::.ctor(UnityEngine.ResourceManagement.Util.IAllocationStrategy)
extern void ResourceManager__ctor_m8FC110F5D15AE6BB0225B82EE23B6FE589384A63 (void);
// 0x00000041 System.Void UnityEngine.ResourceManagement.ResourceManager::OnObjectAdded(System.Object)
extern void ResourceManager_OnObjectAdded_mB1214C5B5A091942F1BC9F78791FEFF111A5FA3D (void);
// 0x00000042 System.Void UnityEngine.ResourceManagement.ResourceManager::OnObjectRemoved(System.Object)
extern void ResourceManager_OnObjectRemoved_m65985C8F6D034E3125CC0A590CC5C986BEAC36C6 (void);
// 0x00000043 System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterForCallbacks()
extern void ResourceManager_RegisterForCallbacks_m446989CFD30CEAE21A30EDBC7CF5F19293BD57CB (void);
// 0x00000044 System.Void UnityEngine.ResourceManagement.ResourceManager::ClearDiagnosticsCallback()
extern void ResourceManager_ClearDiagnosticsCallback_m10B6405E9C6F22FB92FC2F4DF590237A2129D696 (void);
// 0x00000045 System.Void UnityEngine.ResourceManagement.ResourceManager::ClearDiagnosticCallbacks()
extern void ResourceManager_ClearDiagnosticCallbacks_mFCFC4DB25C14D5D116EF3C20048126747AB38B4F (void);
// 0x00000046 System.Void UnityEngine.ResourceManagement.ResourceManager::UnregisterDiagnosticCallback(System.Action`1<UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext>)
extern void ResourceManager_UnregisterDiagnosticCallback_m2F39BED801D8540D2BC2E4DFC0585A9B7C06F77A (void);
// 0x00000047 System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterDiagnosticCallback(System.Action`4<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventType,System.Int32,System.Object>)
extern void ResourceManager_RegisterDiagnosticCallback_m670CAA7C86A37081D1D46BB9ACF98BA9DB7E04EA (void);
// 0x00000048 System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterDiagnosticCallback(System.Action`1<UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext>)
extern void ResourceManager_RegisterDiagnosticCallback_m5668FFB5E61FADC8859DE00A516B53183A493D83 (void);
// 0x00000049 System.Void UnityEngine.ResourceManagement.ResourceManager::PostDiagnosticEvent(UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext)
extern void ResourceManager_PostDiagnosticEvent_m7F57B61B13435BAFBF1D99DCC0D1B6579E904218 (void);
// 0x0000004A UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider UnityEngine.ResourceManagement.ResourceManager::GetResourceProvider(System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceManager_GetResourceProvider_m5C1B277D488DA44F80077476F23451C8B9BB9840 (void);
// 0x0000004B System.Type UnityEngine.ResourceManagement.ResourceManager::GetDefaultTypeForLocation(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceManager_GetDefaultTypeForLocation_mACD27E5C1DB210607222FC55B6728A9FB96C9B00 (void);
// 0x0000004C System.Int32 UnityEngine.ResourceManagement.ResourceManager::CalculateLocationsHash(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Type)
extern void ResourceManager_CalculateLocationsHash_mE546C5ECB0C13363246BED722B7E5FC899918BA0 (void);
// 0x0000004D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.ResourceManager::ProvideResource(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Type)
extern void ResourceManager_ProvideResource_mF4AA96051EB4B7F521F7C6D3D9463FD5F8265BEA (void);
// 0x0000004E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::ProvideResource(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x0000004F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::StartOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x00000050 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.ResourceManager::StartOperation(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManager_StartOperation_m5F3A0530300FE62FCCA0AA55ADB4AF825AC0E630 (void);
// 0x00000051 System.Void UnityEngine.ResourceManagement.ResourceManager::OnInstanceOperationDestroy(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_OnInstanceOperationDestroy_m51046A99199FE1384F610C8B2D4F9BB45909282F (void);
// 0x00000052 System.Void UnityEngine.ResourceManagement.ResourceManager::OnOperationDestroyNonCached(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_OnOperationDestroyNonCached_m6BDCF67A3CFC9BDD9A33B7F8693B8E583C00182E (void);
// 0x00000053 System.Void UnityEngine.ResourceManagement.ResourceManager::OnOperationDestroyCached(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_OnOperationDestroyCached_m68BA43A0BC60683A5FB3DC9CCFC003D03C4CF383 (void);
// 0x00000054 T UnityEngine.ResourceManagement.ResourceManager::CreateOperation(System.Type,System.Int32,System.Int32,System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x00000055 System.Void UnityEngine.ResourceManagement.ResourceManager::AddOperationToCache(System.Int32,UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_AddOperationToCache_m30550FE4400DF6AA39E06D36EC605D74DC01A1DF (void);
// 0x00000056 System.Boolean UnityEngine.ResourceManagement.ResourceManager::RemoveOperationFromCache(System.Int32)
extern void ResourceManager_RemoveOperationFromCache_m2AA7127F4700DA736A52B34F6EA954E8EEE247BF (void);
// 0x00000057 System.Boolean UnityEngine.ResourceManagement.ResourceManager::IsOperationCached(System.Int32)
extern void ResourceManager_IsOperationCached_m0D86DF2785B2965D4670DB2D11C0279C5D4A6D48 (void);
// 0x00000058 System.Int32 UnityEngine.ResourceManagement.ResourceManager::CachedOperationCount()
extern void ResourceManager_CachedOperationCount_m4B99483D311D33CE8FF7E38F87116FD602BF7F77 (void);
// 0x00000059 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateCompletedOperation(TObject,System.String)
// 0x0000005A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateCompletedOperation(TObject,System.Boolean,System.String)
// 0x0000005B System.Void UnityEngine.ResourceManagement.ResourceManager::Release(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManager_Release_m1BF99536656867FEEFBEF3A3F4A290AA66402EB7 (void);
// 0x0000005C System.Void UnityEngine.ResourceManagement.ResourceManager::Acquire(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManager_Acquire_m071783042B4ACA5A506D43DCAF92DE8EBF94E5C2 (void);
// 0x0000005D UnityEngine.ResourceManagement.AsyncOperations.GroupOperation UnityEngine.ResourceManagement.ResourceManager::AcquireGroupOpFromCache(System.Int32)
extern void ResourceManager_AcquireGroupOpFromCache_m951C52A488C4CFADF4561328E73E8787D2434A4C (void);
// 0x0000005E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::CreateGroupOperation(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>)
// 0x0000005F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::CreateGenericGroupOperation(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>,System.Boolean)
extern void ResourceManager_CreateGenericGroupOperation_m637EFAA4DDCCCB4DB5BDE75BBB4B53C61848D8F2 (void);
// 0x00000060 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::ProvideResourceGroupCached(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Int32,System.Type,System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void ResourceManager_ProvideResourceGroupCached_m7DA704CDBEB10A82FFCC259463A2A92EA00C62AC (void);
// 0x00000061 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.ResourceManagement.ResourceManager::ProvideResources(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Action`1<TObject>)
// 0x00000062 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateChainOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000063 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateChainOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000064 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceManager::ProvideScene(UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void ResourceManager_ProvideScene_mB6328DC75FFFD9CEF4B60511F895F1E2CF709A5D (void);
// 0x00000065 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceManager::ReleaseScene(UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void ResourceManager_ReleaseScene_m459CB8CAAB851FA3495F881AEFAD1C3FF29106FA (void);
// 0x00000066 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject> UnityEngine.ResourceManagement.ResourceManager::ProvideInstance(UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters)
extern void ResourceManager_ProvideInstance_m9C1635807FC0ED58FB88BEF2B803BC9C1AAD14F3 (void);
// 0x00000067 System.Void UnityEngine.ResourceManagement.ResourceManager::CleanupSceneInstances(UnityEngine.SceneManagement.Scene)
extern void ResourceManager_CleanupSceneInstances_m9B491B22C7532958A6751CDD08384E7474D1E8CC (void);
// 0x00000068 System.Void UnityEngine.ResourceManagement.ResourceManager::ExecuteDeferredCallbacks()
extern void ResourceManager_ExecuteDeferredCallbacks_mA834F0420F4C269DC21BCB39AFF67C51325AE373 (void);
// 0x00000069 System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterForDeferredCallback(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Boolean)
extern void ResourceManager_RegisterForDeferredCallback_mDE63306BA1891888B6E2C57E74D4CF8AF8F51952 (void);
// 0x0000006A System.Void UnityEngine.ResourceManagement.ResourceManager::Update(System.Single)
extern void ResourceManager_Update_mA2F03AF254D8794D37A7821F905AA8FF8EC97FFF (void);
// 0x0000006B System.Void UnityEngine.ResourceManagement.ResourceManager::Dispose()
extern void ResourceManager_Dispose_m57783F295F3FE72496F8B2CC2BC06C671643CF53 (void);
// 0x0000006C System.Void UnityEngine.ResourceManagement.ResourceManager::.cctor()
extern void ResourceManager__cctor_m75EFB1F6063CB219CCEBBC30CF586872EEAC4F82 (void);
// 0x0000006D System.Void UnityEngine.ResourceManagement.ResourceManager::<.ctor>b__45_0(UnityEngine.ResourceManagement.IUpdateReceiver)
extern void ResourceManager_U3C_ctorU3Eb__45_0_m264F9BEE44E4B9D4756AE7CB0D09A4C167CE6874 (void);
// 0x0000006E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::get_OperationHandle()
extern void DiagnosticEventContext_get_OperationHandle_m870675A1B170E90324CB13D4320F2DEF29D6A1BD_AdjustorThunk (void);
// 0x0000006F UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventType UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::get_Type()
extern void DiagnosticEventContext_get_Type_m3C77CCA8778F7D55230D59FD0B6DEB37F910B15E_AdjustorThunk (void);
// 0x00000070 System.Int32 UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::get_EventValue()
extern void DiagnosticEventContext_get_EventValue_m186FB7D8DFACBD40B9A7FC254685B544C5FC5448_AdjustorThunk (void);
// 0x00000071 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::get_Location()
extern void DiagnosticEventContext_get_Location_mC7B8C8151070981ACEF34A2110E16024587691B7_AdjustorThunk (void);
// 0x00000072 System.Object UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::get_Context()
extern void DiagnosticEventContext_get_Context_mB145AF762ACC916F5CBCD03B392B36CB6BBD5D91_AdjustorThunk (void);
// 0x00000073 System.String UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::get_Error()
extern void DiagnosticEventContext_get_Error_m6758C6A5F0F1C59C8AFB8A5D5FEF7547C806D808_AdjustorThunk (void);
// 0x00000074 System.Void UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::.ctor(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventType,System.Int32,System.String,System.Object)
extern void DiagnosticEventContext__ctor_m46A56FA95388629125EDEE74C865060584274238_AdjustorThunk (void);
// 0x00000075 System.Void UnityEngine.ResourceManagement.ResourceManager_CompletedOperation`1::.ctor()
// 0x00000076 System.Void UnityEngine.ResourceManagement.ResourceManager_CompletedOperation`1::Init(TObject,System.Boolean,System.String)
// 0x00000077 System.Void UnityEngine.ResourceManagement.ResourceManager_CompletedOperation`1::Execute()
// 0x00000078 System.Void UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::Init(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>)
extern void InstanceOperation_Init_mE5EE6B621B9B927D75F2281EDDEC91E5DE16A9CA (void);
// 0x00000079 System.Void UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void InstanceOperation_GetDependencies_m26D3CB8DE0FCFC784B2C9663A482F66BD92C72E5 (void);
// 0x0000007A System.String UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::get_DebugName()
extern void InstanceOperation_get_DebugName_mE303D6480456152374BCE927EE4737D33E70E7FE (void);
// 0x0000007B UnityEngine.SceneManagement.Scene UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::InstanceScene()
extern void InstanceOperation_InstanceScene_m6AF4F1151492C24B07C214A98DBAB5CBE64E3DD6 (void);
// 0x0000007C System.Void UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::Destroy()
extern void InstanceOperation_Destroy_mC17ABC3FF150BAE7C7C6DD031B741D1099C174AE (void);
// 0x0000007D System.Single UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::get_Progress()
extern void InstanceOperation_get_Progress_mFB3EC02597F244CEE84815611AFD4074E1028883 (void);
// 0x0000007E System.Void UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::Execute()
extern void InstanceOperation_Execute_mAED30650EF9D31AB1A5CC87410964DFB4149BF2C (void);
// 0x0000007F System.Void UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::.ctor()
extern void InstanceOperation__ctor_mBE633769231DE956B1D301282CC4D488F52495C8 (void);
// 0x00000080 System.Void UnityEngine.ResourceManagement.ResourceManager_<>c__DisplayClass80_0`1::.ctor()
// 0x00000081 System.Void UnityEngine.ResourceManagement.ResourceManager_<>c__DisplayClass80_0`1::<ProvideResources>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x00000082 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.ResourceManagement.ResourceManager_<>c__DisplayClass80_0`1::<ProvideResources>b__1(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
// 0x00000083 System.Void UnityEngine.ResourceManagement.IUpdateReceiver::Update(System.Single)
// 0x00000084 System.Boolean UnityEngine.ResourceManagement.WebRequestQueueOperation::get_IsDone()
extern void WebRequestQueueOperation_get_IsDone_mA24CD38542F301A96BA73352520D86348D483079 (void);
// 0x00000085 System.Void UnityEngine.ResourceManagement.WebRequestQueueOperation::.ctor(UnityEngine.Networking.UnityWebRequest)
extern void WebRequestQueueOperation__ctor_mE3E3145649464B791DDC02D0BA75AEC6A5CA0B7E (void);
// 0x00000086 System.Void UnityEngine.ResourceManagement.WebRequestQueueOperation::Complete(UnityEngine.Networking.UnityWebRequestAsyncOperation)
extern void WebRequestQueueOperation_Complete_mBF0118DBD2907500B5A87F494B14672DBF534763 (void);
// 0x00000087 UnityEngine.ResourceManagement.WebRequestQueueOperation UnityEngine.ResourceManagement.WebRequestQueue::QueueRequest(UnityEngine.Networking.UnityWebRequest)
extern void WebRequestQueue_QueueRequest_m54F1A56E5082365292141AC67CEFEBEEA52F04AE (void);
// 0x00000088 System.Void UnityEngine.ResourceManagement.WebRequestQueue::OnWebAsyncOpComplete(UnityEngine.AsyncOperation)
extern void WebRequestQueue_OnWebAsyncOpComplete_m27F37E235E00801CCD54A4D8ED49538371FC0180 (void);
// 0x00000089 System.Void UnityEngine.ResourceManagement.WebRequestQueue::.cctor()
extern void WebRequestQueue__cctor_m3B750ADE0715FB14DEBF388CA01266DF0B455925 (void);
// 0x0000008A System.Void UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::.ctor()
extern void ResourceManagerException__ctor_m846D7FCC2C06373694800B5220DF73E6D26F6BA7 (void);
// 0x0000008B System.Void UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::.ctor(System.String)
extern void ResourceManagerException__ctor_m9181C112F2C8E32E77CEDE21C4B5DF0F1CBB5948 (void);
// 0x0000008C System.Void UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::.ctor(System.String,System.Exception)
extern void ResourceManagerException__ctor_mE76839DF077E4AB88010D559E270AB53D2509A66 (void);
// 0x0000008D System.Void UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void ResourceManagerException__ctor_mCE13F6D2BC0FAE0B8B66A9C13CFABA3EF7879F04 (void);
// 0x0000008E UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::get_Location()
extern void UnknownResourceProviderException_get_Location_mC37FB4E0339053A4F3C5DFDEFAC15F8B4E1A7CA6 (void);
// 0x0000008F System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::set_Location(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void UnknownResourceProviderException_set_Location_mDE78145787A8CDF8058E1FE350F79F23C6E0121E (void);
// 0x00000090 System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void UnknownResourceProviderException__ctor_mA08582F29A5DF6471FEE1170D7EB38CAFEAB5C5F (void);
// 0x00000091 System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor()
extern void UnknownResourceProviderException__ctor_m4B7176BAE46F30C73450E5306F911BD53EDD151E (void);
// 0x00000092 System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor(System.String)
extern void UnknownResourceProviderException__ctor_m365634FE703F611EDC55F3E4142B4E69EB72F682 (void);
// 0x00000093 System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor(System.String,System.Exception)
extern void UnknownResourceProviderException__ctor_m682FE80405FA7A8FAFEBBA1F7605379338B7BB97 (void);
// 0x00000094 System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void UnknownResourceProviderException__ctor_m0729FF6A25B6CB353ECE181A7C907E15B64879FF (void);
// 0x00000095 System.String UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::get_Message()
extern void UnknownResourceProviderException_get_Message_mF411A2558453686665445094929B223C43FB6C80 (void);
// 0x00000096 System.String UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::ToString()
extern void UnknownResourceProviderException_ToString_m8E3FCBA2D2A41E951C60958FC604DC7D4A83C26E (void);
// 0x00000097 System.Collections.Generic.LinkedListNode`1<UnityEngine.ResourceManagement.Util.DelayedActionManager_DelegateInfo> UnityEngine.ResourceManagement.Util.DelayedActionManager::GetNode(UnityEngine.ResourceManagement.Util.DelayedActionManager_DelegateInfo&)
extern void DelayedActionManager_GetNode_m0DE3584622A131A6793E55EACD16CA4019947088 (void);
// 0x00000098 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::Clear()
extern void DelayedActionManager_Clear_mCE2BE0F40CA9566F3AD0FBC7506D2AAE6FC99380 (void);
// 0x00000099 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::DestroyWhenComplete()
extern void DelayedActionManager_DestroyWhenComplete_mA1D76FBA64E4BBB878914741B403BF2904F9DA01 (void);
// 0x0000009A System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::AddAction(System.Delegate,System.Single,System.Object[])
extern void DelayedActionManager_AddAction_m50BBD4B75371CB8B2A2230DF09216C2F85CF9262 (void);
// 0x0000009B System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::AddActionInternal(System.Delegate,System.Single,System.Object[])
extern void DelayedActionManager_AddActionInternal_m613DDDB8D8F5A44CBE0793FEDE3EA80E0FD0AC0B (void);
// 0x0000009C System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::Awake()
extern void DelayedActionManager_Awake_m7F2528CF205F37FB341CE53F10EAF90D5FB13260 (void);
// 0x0000009D System.Boolean UnityEngine.ResourceManagement.Util.DelayedActionManager::get_IsActive()
extern void DelayedActionManager_get_IsActive_mF1D78D5B0C9E9EA99B953C6F26868C53A02AC32F (void);
// 0x0000009E System.Boolean UnityEngine.ResourceManagement.Util.DelayedActionManager::Wait(System.Single,System.Single)
extern void DelayedActionManager_Wait_m18E9BC16EFA84B02DCFCF3C56C31EF2F4B356F8F (void);
// 0x0000009F System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::LateUpdate()
extern void DelayedActionManager_LateUpdate_m54908B1CB07F9901C208ECD97A82BB62A9B70D91 (void);
// 0x000000A0 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::InternalLateUpdate(System.Single)
extern void DelayedActionManager_InternalLateUpdate_mD6B2568B45B54B79717B73422415240BFA28F37C (void);
// 0x000000A1 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::OnApplicationQuit()
extern void DelayedActionManager_OnApplicationQuit_m209580E2C7D3265295F1B1048880C96416F5E198 (void);
// 0x000000A2 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager::.ctor()
extern void DelayedActionManager__ctor_mC4435A115C4DD5A9112A7B40CFD330F7D8340320 (void);
// 0x000000A3 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager_DelegateInfo::.ctor(System.Delegate,System.Single,System.Object[])
extern void DelegateInfo__ctor_mBB59764DCA627416685191C4BFFDF7E6B3F9EEB0_AdjustorThunk (void);
// 0x000000A4 System.Single UnityEngine.ResourceManagement.Util.DelayedActionManager_DelegateInfo::get_InvocationTime()
extern void DelegateInfo_get_InvocationTime_mD426371CDDBF73985C41649D481C1C8032731780_AdjustorThunk (void);
// 0x000000A5 System.String UnityEngine.ResourceManagement.Util.DelayedActionManager_DelegateInfo::ToString()
extern void DelegateInfo_ToString_m088DA64541A8A73664E1C101FE2726B1325AC5BC_AdjustorThunk (void);
// 0x000000A6 System.Void UnityEngine.ResourceManagement.Util.DelayedActionManager_DelegateInfo::Invoke()
extern void DelegateInfo_Invoke_m9010FEA078121055ACEBF2CD5049762D439DDD8F_AdjustorThunk (void);
// 0x000000A7 System.Boolean UnityEngine.ResourceManagement.Util.IInitializableObject::Initialize(System.String,System.String)
// 0x000000A8 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.ResourceManagement.Util.IInitializableObject::InitializeAsync(UnityEngine.ResourceManagement.ResourceManager,System.String,System.String)
// 0x000000A9 System.String UnityEngine.ResourceManagement.Util.IObjectInitializationDataProvider::get_Name()
// 0x000000AA UnityEngine.ResourceManagement.Util.ObjectInitializationData UnityEngine.ResourceManagement.Util.IObjectInitializationDataProvider::CreateObjectInitializationData()
// 0x000000AB System.Object UnityEngine.ResourceManagement.Util.IAllocationStrategy::New(System.Type,System.Int32)
// 0x000000AC System.Void UnityEngine.ResourceManagement.Util.IAllocationStrategy::Release(System.Int32,System.Object)
// 0x000000AD System.Object UnityEngine.ResourceManagement.Util.DefaultAllocationStrategy::New(System.Type,System.Int32)
extern void DefaultAllocationStrategy_New_m08D4B93377956FCA42653F39C2675229759A80D1 (void);
// 0x000000AE System.Void UnityEngine.ResourceManagement.Util.DefaultAllocationStrategy::Release(System.Int32,System.Object)
extern void DefaultAllocationStrategy_Release_m8AD07C016B04002C080A2745A6892A6852DD6D06 (void);
// 0x000000AF System.Void UnityEngine.ResourceManagement.Util.DefaultAllocationStrategy::.ctor()
extern void DefaultAllocationStrategy__ctor_mAA0485502871FF659EED040E6835C0C9053344B9 (void);
// 0x000000B0 System.Void UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void LRUCacheAllocationStrategy__ctor_m6AB5B8D1E389B9300ABFC78F7C1B9F33FE0977C9 (void);
// 0x000000B1 System.Collections.Generic.List`1<System.Object> UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::GetPool()
extern void LRUCacheAllocationStrategy_GetPool_mB24B50035BCC44F88A78479974237784205D6914 (void);
// 0x000000B2 System.Void UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::ReleasePool(System.Collections.Generic.List`1<System.Object>)
extern void LRUCacheAllocationStrategy_ReleasePool_mA171571331A0C7B2C747FAC28483247D5F83B674 (void);
// 0x000000B3 System.Object UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::New(System.Type,System.Int32)
extern void LRUCacheAllocationStrategy_New_m85F22B7C23EB9A073651189EA98D08B0292AF3F4 (void);
// 0x000000B4 System.Void UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::Release(System.Int32,System.Object)
extern void LRUCacheAllocationStrategy_Release_m37E39356812AF84792B4E23D0D34453DEB84E528 (void);
// 0x000000B5 System.Void UnityEngine.ResourceManagement.Util.SerializedTypeRestrictionAttribute::.ctor()
extern void SerializedTypeRestrictionAttribute__ctor_m4060A47D9718A425D3C39090B02C1967B3A1D0E4 (void);
// 0x000000B6 System.Collections.Generic.LinkedListNode`1<T> UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::Acquire(T)
// 0x000000B7 System.Void UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::Release(System.Collections.Generic.LinkedListNode`1<T>)
// 0x000000B8 System.Int32 UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::get_CreatedNodeCount()
// 0x000000B9 System.Int32 UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::get_CachedNodeCount()
// 0x000000BA System.Void UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::.ctor()
// 0x000000BB System.Collections.Generic.LinkedListNode`1<T> UnityEngine.ResourceManagement.Util.GlobalLinkedListNodeCache`1::Acquire(T)
// 0x000000BC System.Void UnityEngine.ResourceManagement.Util.GlobalLinkedListNodeCache`1::Release(System.Collections.Generic.LinkedListNode`1<T>)
// 0x000000BD System.String UnityEngine.ResourceManagement.Util.SerializedType::get_AssemblyName()
extern void SerializedType_get_AssemblyName_m2DBDDEF51A9338BF298BCBBD2EE398D72E963635_AdjustorThunk (void);
// 0x000000BE System.String UnityEngine.ResourceManagement.Util.SerializedType::get_ClassName()
extern void SerializedType_get_ClassName_m06DB26816F632CCAF380F818EA666D85C409D8B2_AdjustorThunk (void);
// 0x000000BF System.String UnityEngine.ResourceManagement.Util.SerializedType::ToString()
extern void SerializedType_ToString_m85C9B44C29867CD01DAA7CB5A365577818B4212A_AdjustorThunk (void);
// 0x000000C0 System.Type UnityEngine.ResourceManagement.Util.SerializedType::get_Value()
extern void SerializedType_get_Value_m412D3B47698A74EFA8A3B29B4D102842714BD6B0_AdjustorThunk (void);
// 0x000000C1 System.Void UnityEngine.ResourceManagement.Util.SerializedType::set_Value(System.Type)
extern void SerializedType_set_Value_m0E1021AEE9B0E8B98800E79734E8B140032B9E54_AdjustorThunk (void);
// 0x000000C2 System.Boolean UnityEngine.ResourceManagement.Util.SerializedType::get_ValueChanged()
extern void SerializedType_get_ValueChanged_m1B7B62A60583EE0D14418C391367985DBE6E7562_AdjustorThunk (void);
// 0x000000C3 System.Void UnityEngine.ResourceManagement.Util.SerializedType::set_ValueChanged(System.Boolean)
extern void SerializedType_set_ValueChanged_m7EB0C0A5F56B0594CC3578191F4E26CAC1D08D7D_AdjustorThunk (void);
// 0x000000C4 System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::get_Id()
extern void ObjectInitializationData_get_Id_m1455892EB3BA6CD3ED9F9A73F6401285E5486C65_AdjustorThunk (void);
// 0x000000C5 UnityEngine.ResourceManagement.Util.SerializedType UnityEngine.ResourceManagement.Util.ObjectInitializationData::get_ObjectType()
extern void ObjectInitializationData_get_ObjectType_mB1B59BD388442F732B6FFA421AE1ED0AE3AE4532_AdjustorThunk (void);
// 0x000000C6 System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::get_Data()
extern void ObjectInitializationData_get_Data_m026CC155470843EE675C47D80DDE91CFA7B05AA0_AdjustorThunk (void);
// 0x000000C7 System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::ToString()
extern void ObjectInitializationData_ToString_m43DC1ED8748A5B962C707EA72E529243ABDA37D6_AdjustorThunk (void);
// 0x000000C8 TObject UnityEngine.ResourceManagement.Util.ObjectInitializationData::CreateInstance(System.String)
// 0x000000C9 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.Util.ObjectInitializationData::GetAsyncInitHandle(UnityEngine.ResourceManagement.ResourceManager,System.String)
extern void ObjectInitializationData_GetAsyncInitHandle_m3050F492A6972087E9AA5371C7946CBE5F29AC42_AdjustorThunk (void);
// 0x000000CA System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::ExtractKeyAndSubKey(System.Object,System.String&,System.String&)
extern void ResourceManagerConfig_ExtractKeyAndSubKey_m48B90CB062DF102F56698425FE9F75795239ED15 (void);
// 0x000000CB System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::IsPathRemote(System.String)
extern void ResourceManagerConfig_IsPathRemote_m4A0EC7B1CEFE8C1B172D2D7CA1FCC2EDDF09FA12 (void);
// 0x000000CC System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::ShouldPathUseWebRequest(System.String)
extern void ResourceManagerConfig_ShouldPathUseWebRequest_mC7BD63058F88117DA2622430B22DB7B80D138D76 (void);
// 0x000000CD System.Array UnityEngine.ResourceManagement.Util.ResourceManagerConfig::CreateArrayResult(System.Type,UnityEngine.Object[])
extern void ResourceManagerConfig_CreateArrayResult_mE028A19D4D9E4908BE5911B8E7A21A85D18237B6 (void);
// 0x000000CE TObject UnityEngine.ResourceManagement.Util.ResourceManagerConfig::CreateArrayResult(UnityEngine.Object[])
// 0x000000CF System.Collections.IList UnityEngine.ResourceManagement.Util.ResourceManagerConfig::CreateListResult(System.Type,UnityEngine.Object[])
extern void ResourceManagerConfig_CreateListResult_mE82A475A890F6091076CAC624DF32496CD75CF1F (void);
// 0x000000D0 TObject UnityEngine.ResourceManagement.Util.ResourceManagerConfig::CreateListResult(UnityEngine.Object[])
// 0x000000D1 System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::IsInstance()
// 0x000000D2 UnityEngine.AssetBundle UnityEngine.ResourceManagement.ResourceProviders.IAssetBundleResource::GetAssetBundle()
// 0x000000D3 System.String UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_Hash()
extern void AssetBundleRequestOptions_get_Hash_mB4B18C05B59265AA39B3A02C0688DE1D1C88711A (void);
// 0x000000D4 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_Hash(System.String)
extern void AssetBundleRequestOptions_set_Hash_mE0924B6592A1DFC9D594E11FF135AD1E87B41D3F (void);
// 0x000000D5 System.UInt32 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_Crc()
extern void AssetBundleRequestOptions_get_Crc_m2BA7D512A54EA6075A0CF024D012B6433C8A9A4E (void);
// 0x000000D6 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_Crc(System.UInt32)
extern void AssetBundleRequestOptions_set_Crc_m2FB9FEAAEBC071272C46D9F8C02AE3BC5FBCD635 (void);
// 0x000000D7 System.Int32 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_Timeout()
extern void AssetBundleRequestOptions_get_Timeout_m42D9843C4E83D152DAE0CE4CD3E8E65E0623080B (void);
// 0x000000D8 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_Timeout(System.Int32)
extern void AssetBundleRequestOptions_set_Timeout_m40BFCFEF67A5C0D73A9F289EF8AA6A885479FC50 (void);
// 0x000000D9 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_ChunkedTransfer()
extern void AssetBundleRequestOptions_get_ChunkedTransfer_m5D4E14BA8EB284968D3BD0CFC5473B69A1FB04CD (void);
// 0x000000DA System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_ChunkedTransfer(System.Boolean)
extern void AssetBundleRequestOptions_set_ChunkedTransfer_m790165DE615D3A3D0CE204FEE2F4133D719CE499 (void);
// 0x000000DB System.Int32 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_RedirectLimit()
extern void AssetBundleRequestOptions_get_RedirectLimit_m099ACB2C16C55D0C615FF15E71908E3125D7F8AC (void);
// 0x000000DC System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_RedirectLimit(System.Int32)
extern void AssetBundleRequestOptions_set_RedirectLimit_m0B11D7F0C5B94AD17B0C7E1729130E8B5BE5D7E7 (void);
// 0x000000DD System.Int32 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_RetryCount()
extern void AssetBundleRequestOptions_get_RetryCount_m19487390C594350D557346716F1CB3C0B38C2B3F (void);
// 0x000000DE System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_RetryCount(System.Int32)
extern void AssetBundleRequestOptions_set_RetryCount_mA0A092315042B6625A1B8FBC5383491B7C414CD3 (void);
// 0x000000DF System.String UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_BundleName()
extern void AssetBundleRequestOptions_get_BundleName_mC794C3CB5F0DD2ED320B34F44E2F586A55C24CF8 (void);
// 0x000000E0 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_BundleName(System.String)
extern void AssetBundleRequestOptions_set_BundleName_m9C66005D818F3CFDA4B79FDF4011CF27F0C2A72E (void);
// 0x000000E1 System.Int64 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::get_BundleSize()
extern void AssetBundleRequestOptions_get_BundleSize_m01BFA314FF232985809F91FA0D1711984582AD5E (void);
// 0x000000E2 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::set_BundleSize(System.Int64)
extern void AssetBundleRequestOptions_set_BundleSize_m26D31A8E9D68404B7F5977B490BFE815D5D01D02 (void);
// 0x000000E3 System.Int64 UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::ComputeSize(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceManager)
extern void AssetBundleRequestOptions_ComputeSize_m2E82047D611D69C7AADC6FD0151E2EF610E4D202 (void);
// 0x000000E4 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleRequestOptions::.ctor()
extern void AssetBundleRequestOptions__ctor_m478D494DCB048FCC2462FE9D87ED34991E0D0869 (void);
// 0x000000E5 UnityEngine.Networking.UnityWebRequest UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::CreateWebRequest(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void AssetBundleResource_CreateWebRequest_m008113D1236E539144C558988C2D6F9D71881962 (void);
// 0x000000E6 System.Single UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::PercentComplete()
extern void AssetBundleResource_PercentComplete_m83436C87C7EE6E0FF6C28FB7E05B841308DFA8AB (void);
// 0x000000E7 UnityEngine.AssetBundle UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::GetAssetBundle()
extern void AssetBundleResource_GetAssetBundle_mEDCC866A7AF1A532F40B40B821DD457AC73FB8D0 (void);
// 0x000000E8 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void AssetBundleResource_Start_m165368F7AD26E5F1C74190C95817A9D5757D2BA4 (void);
// 0x000000E9 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::BeginOperation()
extern void AssetBundleResource_BeginOperation_m8A9873499D80D88F50FC5F20FA039F0AEA50DC12 (void);
// 0x000000EA System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::LocalRequestOperationCompleted(UnityEngine.AsyncOperation)
extern void AssetBundleResource_LocalRequestOperationCompleted_m5B5121EDE02A34809BF275AD11A3D8785686FF5F (void);
// 0x000000EB System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::WebRequestOperationCompleted(UnityEngine.AsyncOperation)
extern void AssetBundleResource_WebRequestOperationCompleted_m36359C93B7E68673166D558680F003D314E4AD1C (void);
// 0x000000EC System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::Unload()
extern void AssetBundleResource_Unload_m141896873F3F31F8C587361583C8F71DC3F7049E (void);
// 0x000000ED System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::.ctor()
extern void AssetBundleResource__ctor_mD5B4085DF1B5D6935198B89C651FDF6818C43835 (void);
// 0x000000EE System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleResource::<BeginOperation>b__11_0(UnityEngine.Networking.UnityWebRequestAsyncOperation)
extern void AssetBundleResource_U3CBeginOperationU3Eb__11_0_mA2C74889E0B832B91D5FD9B4FEEA711083732D23 (void);
// 0x000000EF System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void AssetBundleProvider_Provide_mD46D28C6CA670CA740FE87912320632FDB659C1E (void);
// 0x000000F0 System.Type UnityEngine.ResourceManagement.ResourceProviders.AssetBundleProvider::GetDefaultType(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void AssetBundleProvider_GetDefaultType_m79830BEE6CE701C7DF95E65F33C5AF8B74A83852 (void);
// 0x000000F1 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleProvider::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
extern void AssetBundleProvider_Release_m48DDAE8401ADD5932DAAC61C4D161155FF43B3E6 (void);
// 0x000000F2 System.Void UnityEngine.ResourceManagement.ResourceProviders.AssetBundleProvider::.ctor()
extern void AssetBundleProvider__ctor_m5C55E1DE6E0E02A232B307350CA4D76C187402B1 (void);
// 0x000000F3 System.Void UnityEngine.ResourceManagement.ResourceProviders.AtlasSpriteProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void AtlasSpriteProvider_Provide_mF045676D19D97CD57A7BC0807027C8BB78217E15 (void);
// 0x000000F4 System.Void UnityEngine.ResourceManagement.ResourceProviders.AtlasSpriteProvider::.ctor()
extern void AtlasSpriteProvider__ctor_m05488EDAFF03B092471403E9999CD0266C5AACED (void);
// 0x000000F5 System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void BundledAssetProvider_Provide_m8C2EDDF78D3CCC187CC41F63E8FC695597C4A12E (void);
// 0x000000F6 System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider::.ctor()
extern void BundledAssetProvider__ctor_mC25705358F17D4412D359E73C5F4578CF85192EB (void);
// 0x000000F7 UnityEngine.AssetBundle UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider_InternalOp::LoadBundleFromDependecies(System.Collections.Generic.IList`1<System.Object>)
extern void InternalOp_LoadBundleFromDependecies_m5270426EB07F453668EE5ABB2C20FEE5F866C350 (void);
// 0x000000F8 System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider_InternalOp::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void InternalOp_Start_mA10ADF4F26C477CD58C6E8F808165CCF1270A74D (void);
// 0x000000F9 System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider_InternalOp::ActionComplete(UnityEngine.AsyncOperation)
extern void InternalOp_ActionComplete_m2F628707CEA05C810AE258C5549C47EECABBCA9D (void);
// 0x000000FA System.Single UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider_InternalOp::ProgressCallback()
extern void InternalOp_ProgressCallback_mFD402D64CE77420108145A6E6DB428E914D7BF57 (void);
// 0x000000FB System.Void UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider_InternalOp::.ctor()
extern void InternalOp__ctor_mB8936B16062FBD1BCDE173A781968358E492FB7F (void);
// 0x000000FC UnityEngine.Vector3 UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_Position()
extern void InstantiationParameters_get_Position_m9569A28B575846AD814790114A6CFAB8FC3E32DC_AdjustorThunk (void);
// 0x000000FD UnityEngine.Quaternion UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_Rotation()
extern void InstantiationParameters_get_Rotation_m155362C3F3442CA4CC53C063FA8B4D09B753B012_AdjustorThunk (void);
// 0x000000FE UnityEngine.Transform UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_Parent()
extern void InstantiationParameters_get_Parent_mD6BC05225FF561D2123B1745B838D147D9B6A621_AdjustorThunk (void);
// 0x000000FF System.Boolean UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_InstantiateInWorldPosition()
extern void InstantiationParameters_get_InstantiateInWorldPosition_mF6ACF2A60E6C89C55CC36D82220910EEA9CC65F4_AdjustorThunk (void);
// 0x00000100 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::get_SetPositionRotation()
extern void InstantiationParameters_get_SetPositionRotation_m3B5F3C4065F65ED2D4FD75B3B81D311B15EB70D1_AdjustorThunk (void);
// 0x00000101 System.Void UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::.ctor(UnityEngine.Transform,System.Boolean)
extern void InstantiationParameters__ctor_m89D89058D329E9C651528936FD098282F1589225_AdjustorThunk (void);
// 0x00000102 System.Void UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern void InstantiationParameters__ctor_mE86F10CB3D40AC7EA1883000E381AEAEA1E16A33_AdjustorThunk (void);
// 0x00000103 TObject UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters::Instantiate(TObject)
// 0x00000104 UnityEngine.GameObject UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider::ProvideInstance(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters)
// 0x00000105 System.Void UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider::ReleaseInstance(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.GameObject)
// 0x00000106 System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::.ctor(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation)
extern void ProvideHandle__ctor_mB9CCF5CA6F8DD98AB72B4659076385051038795C_AdjustorThunk (void);
// 0x00000107 UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_InternalOp()
extern void ProvideHandle_get_InternalOp_mDCF56F543329C2078BA6B8CE99C39B6EF3BEA41F_AdjustorThunk (void);
// 0x00000108 UnityEngine.ResourceManagement.ResourceManager UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_ResourceManager()
extern void ProvideHandle_get_ResourceManager_mD9EAFA3333B5A459E1AD6EA76E15558C531ACD76_AdjustorThunk (void);
// 0x00000109 System.Type UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_Type()
extern void ProvideHandle_get_Type_mC36FCA0E5806E401F98BC3D30511DE5015A81EFF_AdjustorThunk (void);
// 0x0000010A UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_Location()
extern void ProvideHandle_get_Location_m38F31BC3081090D72C7EFA9C6CDE73ED45B7D978_AdjustorThunk (void);
// 0x0000010B System.Int32 UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_DependencyCount()
extern void ProvideHandle_get_DependencyCount_m56B0737197C7470C2A5FF8E8E2BBDA151E572BA4_AdjustorThunk (void);
// 0x0000010C TDepObject UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::GetDependency(System.Int32)
// 0x0000010D System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::GetDependencies(System.Collections.Generic.IList`1<System.Object>)
extern void ProvideHandle_GetDependencies_mC65A5900B3EA9F0874C3D19BEFFC022DFC9AD8E8_AdjustorThunk (void);
// 0x0000010E System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::SetProgressCallback(System.Func`1<System.Single>)
extern void ProvideHandle_SetProgressCallback_m3B3F019C3AFF1A6B0F37B5B28F9A1121EBB73FD2_AdjustorThunk (void);
// 0x0000010F System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::Complete(T,System.Boolean,System.Exception)
// 0x00000110 System.String UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::get_ProviderId()
// 0x00000111 System.Type UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::GetDefaultType(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x00000112 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::CanProvide(System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x00000113 System.Void UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
// 0x00000114 System.Void UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
// 0x00000115 UnityEngine.ResourceManagement.ResourceProviders.ProviderBehaviourFlags UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::get_BehaviourFlags()
// 0x00000116 UnityEngine.SceneManagement.Scene UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::get_Scene()
extern void SceneInstance_get_Scene_mFF895316B46CBB8126ABE0FE8F85985D1232CCA3_AdjustorThunk (void);
// 0x00000117 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::set_Scene(UnityEngine.SceneManagement.Scene)
extern void SceneInstance_set_Scene_m8AD506582C450F4C678C7265ED0A94FFDA9179E5_AdjustorThunk (void);
// 0x00000118 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::Activate()
extern void SceneInstance_Activate_m52A0E7A28EEA007FF2529340E980D798374E5B0E_AdjustorThunk (void);
// 0x00000119 UnityEngine.AsyncOperation UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::ActivateAsync()
extern void SceneInstance_ActivateAsync_m0F5677DAD4ED2EC1EB027E261B4EBF40418A3DC3_AdjustorThunk (void);
// 0x0000011A System.Int32 UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::GetHashCode()
extern void SceneInstance_GetHashCode_m9B65494AC6ED027B091B9FF2DE58DEE6E46FFF0B_AdjustorThunk (void);
// 0x0000011B System.Boolean UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::Equals(System.Object)
extern void SceneInstance_Equals_mFD3D668A674D5838BF6F9ED892C0452E48729A43_AdjustorThunk (void);
// 0x0000011C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider::ProvideScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
// 0x0000011D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider::ReleaseScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
// 0x0000011E UnityEngine.GameObject UnityEngine.ResourceManagement.ResourceProviders.InstanceProvider::ProvideInstance(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters)
extern void InstanceProvider_ProvideInstance_m530D2258769CC2B97647ECFBCF34659BD93C2C6B (void);
// 0x0000011F System.Void UnityEngine.ResourceManagement.ResourceProviders.InstanceProvider::ReleaseInstance(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.GameObject)
extern void InstanceProvider_ReleaseInstance_m9DB74D96301B8DD3263AF1AB860E46D85EE08690 (void);
// 0x00000120 System.Void UnityEngine.ResourceManagement.ResourceProviders.InstanceProvider::.ctor()
extern void InstanceProvider__ctor_m35BAD5E188BFB18C9BDCC7AE47F6B9338A7B5D2F (void);
// 0x00000121 System.Object UnityEngine.ResourceManagement.ResourceProviders.JsonAssetProvider::Convert(System.Type,System.String)
extern void JsonAssetProvider_Convert_m97EC94F47C2FA697A38522205E3C972497113AE2 (void);
// 0x00000122 System.Void UnityEngine.ResourceManagement.ResourceProviders.JsonAssetProvider::.ctor()
extern void JsonAssetProvider__ctor_m15637BC90EF129F6F31918CDAE728E5D9560F303 (void);
// 0x00000123 System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void LegacyResourcesProvider_Provide_m4253335CC4BDC849FAD018D5BBDBD3392D03691F (void);
// 0x00000124 System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
extern void LegacyResourcesProvider_Release_m330D49267272717059BD526433B358E581083253 (void);
// 0x00000125 System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider::.ctor()
extern void LegacyResourcesProvider__ctor_m578CAD18E82E4F66CEFCA1A30B00338FB677A4C6 (void);
// 0x00000126 System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider_InternalOp::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void InternalOp_Start_m8CF6A6B5D9675C182B0AAB762BC3E30752CA8EA5 (void);
// 0x00000127 System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider_InternalOp::AsyncOperationCompleted(UnityEngine.AsyncOperation)
extern void InternalOp_AsyncOperationCompleted_m5821F76BA226C04C5188E80E48DC97EF186A72DA (void);
// 0x00000128 System.Single UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider_InternalOp::PercentComplete()
extern void InternalOp_PercentComplete_m3E24E8827F98571B4532D1A36382047CC296EBD8 (void);
// 0x00000129 System.Void UnityEngine.ResourceManagement.ResourceProviders.LegacyResourcesProvider_InternalOp::.ctor()
extern void InternalOp__ctor_mF1AFFC0E9D8BF2049B3255E65A87883BDDE2EF04 (void);
// 0x0000012A System.String UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::get_ProviderId()
extern void ResourceProviderBase_get_ProviderId_m39F6890BE274231ADB091ED135F06B475C7AD842 (void);
// 0x0000012B System.Boolean UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::Initialize(System.String,System.String)
extern void ResourceProviderBase_Initialize_mB88D098C7CA16F9B29CD25F127E586C60271DEC6 (void);
// 0x0000012C System.Boolean UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::CanProvide(System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceProviderBase_CanProvide_m124E66DC2469547DA7E58A5396A1FCAC3B470D85 (void);
// 0x0000012D System.String UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::ToString()
extern void ResourceProviderBase_ToString_m4ACCDF1D1E881C7CC1537F638B29F9BCC9654065 (void);
// 0x0000012E System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::Release(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Object)
extern void ResourceProviderBase_Release_mA4EA7CE2F575DA1E9B1A6B8EA9F21896DDC411A7 (void);
// 0x0000012F System.Type UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::GetDefaultType(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceProviderBase_GetDefaultType_m00E90DFA2818C81077829B7AE9F9847834520E02 (void);
// 0x00000130 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
// 0x00000131 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::InitializeAsync(UnityEngine.ResourceManagement.ResourceManager,System.String,System.String)
extern void ResourceProviderBase_InitializeAsync_mA971647D0097330ACCD75D035481D16D8B8EEFFA (void);
// 0x00000132 UnityEngine.ResourceManagement.ResourceProviders.ProviderBehaviourFlags UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider.get_BehaviourFlags()
extern void ResourceProviderBase_UnityEngine_ResourceManagement_ResourceProviders_IResourceProvider_get_BehaviourFlags_mFDEC1DF913DFD712F248FAD2A36DB610F7580F25 (void);
// 0x00000133 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::.ctor()
extern void ResourceProviderBase__ctor_mA8589431AF866EE209169DA0E84D70845D0F7C1A (void);
// 0x00000134 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase_BaseInitAsyncOp::Init(System.Func`1<System.Boolean>)
extern void BaseInitAsyncOp_Init_m84C2E47DCA1E087F49D35279878445687E3E16F4 (void);
// 0x00000135 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase_BaseInitAsyncOp::Execute()
extern void BaseInitAsyncOp_Execute_mB78A5ED65E7B68D61754EFBF6A73055F64F7A129 (void);
// 0x00000136 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase_BaseInitAsyncOp::.ctor()
extern void BaseInitAsyncOp__ctor_m7D92010EA1BB2C67B605DC95E8D91E500AC7FEDA (void);
// 0x00000137 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m3B6FAF943AAB23C2BD7DF7E6AB9D1B3699703856 (void);
// 0x00000138 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase_<>c__DisplayClass10_0::<InitializeAsync>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CInitializeAsyncU3Eb__0_m19A259CF5D733F72D104DEFAD3348199EDF35F35 (void);
// 0x00000139 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.SceneProvider::ProvideScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void SceneProvider_ProvideScene_mCB31C5F58277AC5B97E3B0E10317FE34EDFD58F8 (void);
// 0x0000013A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.SceneProvider::ReleaseScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void SceneProvider_ReleaseScene_m1A7A7B799DFEF64C68E7101546579CC907E0048A (void);
// 0x0000013B System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider::.ctor()
extern void SceneProvider__ctor_m1B35416831C52CC189753B5624976389772A45F7 (void);
// 0x0000013C System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::.ctor(UnityEngine.ResourceManagement.ResourceManager)
extern void SceneOp__ctor_m5B0A2397AF0B1ACF6E2FD5323BA6EC60B4DA0147 (void);
// 0x0000013D System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::Init(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
extern void SceneOp_Init_mB918CB5783BF2635D2970748F35C9D2DE689304A (void);
// 0x0000013E System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void SceneOp_GetDependencies_mF478AD3651950802CCDD736B1FBC44C32FE692EB (void);
// 0x0000013F System.String UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::get_DebugName()
extern void SceneOp_get_DebugName_m7260EF67264EC70C626B11F4FFB8E73A0965610D (void);
// 0x00000140 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::Execute()
extern void SceneOp_Execute_mCAB604D31CBC76E96284377779B79C74E78E2C6E (void);
// 0x00000141 UnityEngine.ResourceManagement.ResourceProviders.SceneInstance UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::InternalLoadScene(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Boolean,UnityEngine.SceneManagement.LoadSceneMode,System.Boolean,System.Int32)
extern void SceneOp_InternalLoadScene_m9C5B15DCB1405A84EA255ECE59B90D419D6C5653 (void);
// 0x00000142 UnityEngine.AsyncOperation UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::InternalLoad(System.String,System.Boolean,UnityEngine.SceneManagement.LoadSceneMode)
extern void SceneOp_InternalLoad_m157B04FAA5544C6CB3221855FD67178212D6E201 (void);
// 0x00000143 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::Destroy()
extern void SceneOp_Destroy_m701CC2DE4463C54C16757EB3D0A0A94E71077696 (void);
// 0x00000144 System.Single UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::get_Progress()
extern void SceneOp_get_Progress_m378936898C1A6DB059D093337B1DCC400689FB5B (void);
// 0x00000145 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_SceneOp::UnityEngine.ResourceManagement.IUpdateReceiver.Update(System.Single)
extern void SceneOp_UnityEngine_ResourceManagement_IUpdateReceiver_Update_m27FD39CB9D11C53DD786CDB722DD249F7D35217B (void);
// 0x00000146 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_UnloadSceneOp::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void UnloadSceneOp_Init_mC30E36921F3E896B3B6E16765113955E285015A6 (void);
// 0x00000147 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_UnloadSceneOp::Execute()
extern void UnloadSceneOp_Execute_m1DFB8028F12E302AFE304F67E5BAAB3820B0EC4D (void);
// 0x00000148 System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_UnloadSceneOp::UnloadSceneCompleted(UnityEngine.AsyncOperation)
extern void UnloadSceneOp_UnloadSceneCompleted_m357E9D44463E1A1E3E87A29FFF300C11D3E862F9 (void);
// 0x00000149 System.Single UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_UnloadSceneOp::get_Progress()
extern void UnloadSceneOp_get_Progress_m42967C82BB0C649B805E107569933C78AAE18628 (void);
// 0x0000014A System.Void UnityEngine.ResourceManagement.ResourceProviders.SceneProvider_UnloadSceneOp::.ctor()
extern void UnloadSceneOp__ctor_m051B216E7A8E5F7ED061FF3D0986D0BD9E9DF7A5 (void);
// 0x0000014B System.Boolean UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::get_IgnoreFailures()
extern void TextDataProvider_get_IgnoreFailures_m57066898F24CD1829FBD8BB87E0DEAA1CDC20C8B (void);
// 0x0000014C System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::set_IgnoreFailures(System.Boolean)
extern void TextDataProvider_set_IgnoreFailures_m5787E1C9DA529EF415D30ADDA2B74DBE78E6D825 (void);
// 0x0000014D System.Object UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::Convert(System.Type,System.String)
extern void TextDataProvider_Convert_mEBE0237D88E5C2D2F50D7441367D63906D0CDF75 (void);
// 0x0000014E System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void TextDataProvider_Provide_m97C0755B7CB3C55D2D5CE99A26A0A650F887A69B (void);
// 0x0000014F System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::.ctor()
extern void TextDataProvider__ctor_m7AC08BFDB0D65AFFF44638ADB897882432BF92DB (void);
// 0x00000150 System.Single UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider_InternalOp::GetPercentComplete()
extern void InternalOp_GetPercentComplete_m9ED8C8B9A92A008FC832003493795968212F040F (void);
// 0x00000151 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider_InternalOp::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle,UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider,System.Boolean)
extern void InternalOp_Start_m6A88CC1BB80FA2A3AFB0762B7DB973DA7BDBDEE8 (void);
// 0x00000152 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider_InternalOp::RequestOperation_completed(UnityEngine.AsyncOperation)
extern void InternalOp_RequestOperation_completed_m888C6797A4FF695796EAE9B6329A6B1EE896604B (void);
// 0x00000153 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider_InternalOp::.ctor()
extern void InternalOp__ctor_m761A51452CEFD8FAD7575FC1940F3A2E0709DB54 (void);
// 0x00000154 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider_InternalOp::<Start>b__6_0(UnityEngine.Networking.UnityWebRequestAsyncOperation)
extern void InternalOp_U3CStartU3Eb__6_0_mF09E720D8E29AF360526ABC2C782EB4F43543992 (void);
// 0x00000155 System.Int64 UnityEngine.ResourceManagement.ResourceLocations.ILocationSizeData::ComputeSize(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.ResourceManager)
// 0x00000156 System.String UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_InternalId()
// 0x00000157 System.String UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_ProviderId()
// 0x00000158 System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation> UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_Dependencies()
// 0x00000159 System.Int32 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::Hash(System.Type)
// 0x0000015A System.Int32 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_DependencyHashCode()
// 0x0000015B System.Boolean UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_HasDependencies()
// 0x0000015C System.Object UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_Data()
// 0x0000015D System.String UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_PrimaryKey()
// 0x0000015E System.Type UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_ResourceType()
// 0x0000015F System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_InternalId()
extern void ResourceLocationBase_get_InternalId_mDD026BD02BC9C4BFE13018163D5C3333F87AC6CF (void);
// 0x00000160 System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_ProviderId()
extern void ResourceLocationBase_get_ProviderId_m3C0A8FFDD223BA3DB83C2BF36FA16A58F7D56603 (void);
// 0x00000161 System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation> UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_Dependencies()
extern void ResourceLocationBase_get_Dependencies_mF557A83CF1A117CBCE78814D350BC09135BF4DF9 (void);
// 0x00000162 System.Boolean UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_HasDependencies()
extern void ResourceLocationBase_get_HasDependencies_mF968DBFE910DC0F2653AE1D3582149AF8D25DDD7 (void);
// 0x00000163 System.Object UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_Data()
extern void ResourceLocationBase_get_Data_m1FBDAD7E83CA4FE35DF61546CB4864F234760358 (void);
// 0x00000164 System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::set_Data(System.Object)
extern void ResourceLocationBase_set_Data_mD899B389E585E1F781CD02CC2EEDFD38D80664BC (void);
// 0x00000165 System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_PrimaryKey()
extern void ResourceLocationBase_get_PrimaryKey_m2C276908F4E098BCED0F019A30872A7D2A02ACED (void);
// 0x00000166 System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::set_PrimaryKey(System.String)
extern void ResourceLocationBase_set_PrimaryKey_m989397E9A03B8CFDCBD151E087125BCC0C949145 (void);
// 0x00000167 System.Int32 UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_DependencyHashCode()
extern void ResourceLocationBase_get_DependencyHashCode_m1D0F112D82E12492981006CC695486AA85CF7A47 (void);
// 0x00000168 System.Type UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_ResourceType()
extern void ResourceLocationBase_get_ResourceType_m1C6C8FA00487D034B8661EBB5323DEA49E76F631 (void);
// 0x00000169 System.Int32 UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::Hash(System.Type)
extern void ResourceLocationBase_Hash_mFD5291AA954FA36C6C833C3940F8A72A9C2AED77 (void);
// 0x0000016A System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::ToString()
extern void ResourceLocationBase_ToString_m61190854FAFA7A73E0A9DE1647806A6491BE94A7 (void);
// 0x0000016B System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::.ctor(System.String,System.String,System.String,System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation[])
extern void ResourceLocationBase__ctor_m8487509965168D66DD0079D6A096845204088A7F (void);
// 0x0000016C System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::ComputeDependencyHash()
extern void ResourceLocationBase_ComputeDependencyHash_m9BB3035DD2ACAA3680109BD2DEB756F2DC1C1963 (void);
// 0x0000016D System.String UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Graph()
extern void DiagnosticEvent_get_Graph_m710E1F0680B4D39DE0264D295E28650421AEEF6D_AdjustorThunk (void);
// 0x0000016E System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_ObjectId()
extern void DiagnosticEvent_get_ObjectId_mCA40B5EFDB9F507CAB17F2E19DA3F62D68B45537_AdjustorThunk (void);
// 0x0000016F System.String UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_DisplayName()
extern void DiagnosticEvent_get_DisplayName_mCBA5175616B9E1B45C12C51A8FBD01B88E5A78EE_AdjustorThunk (void);
// 0x00000170 System.Int32[] UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Dependencies()
extern void DiagnosticEvent_get_Dependencies_m2751628387892A62E54CD3A32FD77DDD18626B27_AdjustorThunk (void);
// 0x00000171 System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Stream()
extern void DiagnosticEvent_get_Stream_mD35CADC32CA28797FEB25F744975712D808874D2_AdjustorThunk (void);
// 0x00000172 System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Frame()
extern void DiagnosticEvent_get_Frame_m80FE7279F8A049A54DF5D34EC80B0B8F4D33F9EB_AdjustorThunk (void);
// 0x00000173 System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Value()
extern void DiagnosticEvent_get_Value_mA831F1827F784283AF76FBB76D768ED1C00B9410_AdjustorThunk (void);
// 0x00000174 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::.ctor(System.String,System.String,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[])
extern void DiagnosticEvent__ctor_m7010F47B7515B5EA548729B2F3E25EBFAEAEB217_AdjustorThunk (void);
// 0x00000175 System.Byte[] UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::Serialize()
extern void DiagnosticEvent_Serialize_m02EC58094A8703C0C89430022D36A3EE3C304607_AdjustorThunk (void);
// 0x00000176 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::Deserialize(System.Byte[])
extern void DiagnosticEvent_Deserialize_m80E559A631C1CADB8B5EE0B03A70D790C1459F3F (void);
// 0x00000177 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::FindOrCreateGlobalInstance()
extern void DiagnosticEventCollector_FindOrCreateGlobalInstance_m606D0B93253933F64A4F126E952F4AACAD95F1A6 (void);
// 0x00000178 System.Guid UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::get_PlayerConnectionGuid()
extern void DiagnosticEventCollector_get_PlayerConnectionGuid_mCDC64A92E85E48453A28C92A4EF9652C49E593C1 (void);
// 0x00000179 System.Boolean UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::RegisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>,System.Boolean,System.Boolean)
extern void DiagnosticEventCollector_RegisterEventHandler_mD8110A8AE08413AA61A28352CD219880663BE7C3 (void);
// 0x0000017A System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::RegisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>)
extern void DiagnosticEventCollector_RegisterEventHandler_m1BE4579F60E54AE14C6D406DC1B20F4799FCF556 (void);
// 0x0000017B System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::UnregisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>)
extern void DiagnosticEventCollector_UnregisterEventHandler_m39277B914A0C123866E0FA4320B82CA7662B5725 (void);
// 0x0000017C System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::PostEvent(UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent)
extern void DiagnosticEventCollector_PostEvent_m66DEEEEFF15701593B3B600159AE12085330B151 (void);
// 0x0000017D System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::Awake()
extern void DiagnosticEventCollector_Awake_m24F99AB24727466D99C550C3442FEF7DCE311696 (void);
// 0x0000017E System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::Update()
extern void DiagnosticEventCollector_Update_mCB7D61516ECB014E98D126B8977C47ED433D44C2 (void);
// 0x0000017F System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::OnApplicationQuit()
extern void DiagnosticEventCollector_OnApplicationQuit_m89244AE9D73C45E66F8B6ACE666D7E4F17DC08FB (void);
// 0x00000180 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector::.ctor()
extern void DiagnosticEventCollector__ctor_m31BB8D4B02E0F321D9D94F54D6DBBF07B4A91581 (void);
// 0x00000181 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector_<>c::.cctor()
extern void U3CU3Ec__cctor_m8F10B57D6DAF8E389CD8661515EB79DBC504BC37 (void);
// 0x00000182 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector_<>c::.ctor()
extern void U3CU3Ec__ctor_m8D57B362949EF3873FC890D681A810C942A0E6E2 (void);
// 0x00000183 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollector_<>c::<Awake>b__12_0(UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent)
extern void U3CU3Ec_U3CAwakeU3Eb__12_0_mB96A865172FE3E0ACDEA9DAE045B1F8270A467E0 (void);
// 0x00000184 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.ICachable::get_Hash()
// 0x00000185 System.Void UnityEngine.ResourceManagement.AsyncOperations.ICachable::set_Hash(System.Int32)
// 0x00000186 System.Object UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::GetResultAsObject()
// 0x00000187 System.Type UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_ResultType()
// 0x00000188 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Version()
// 0x00000189 System.String UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_DebugName()
// 0x0000018A System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::DecrementReferenceCount()
// 0x0000018B System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::IncrementReferenceCount()
// 0x0000018C System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_ReferenceCount()
// 0x0000018D System.Single UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_PercentComplete()
// 0x0000018E UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Status()
// 0x0000018F System.Exception UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_OperationException()
// 0x00000190 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_IsDone()
// 0x00000191 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::set_OnDestroy(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x00000192 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000193 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000194 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000195 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000196 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000197 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::InvokeCompletionEvent()
// 0x00000198 System.Threading.Tasks.Task`1<System.Object> UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Task()
// 0x00000199 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::Start(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,DelegateList`1<System.Single>)
// 0x0000019A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Handle()
// 0x0000019B System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Execute()
// 0x0000019C System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Destroy()
// 0x0000019D System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Progress()
// 0x0000019E System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_DebugName()
// 0x0000019F System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001A0 TObject UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Result()
// 0x000001A1 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_Result(TObject)
// 0x000001A2 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Version()
// 0x000001A3 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_CompletedEventHasListeners()
// 0x000001A4 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_DestroyedEventHasListeners()
// 0x000001A5 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_CompletedTypelessEventHasListeners()
// 0x000001A6 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_OnDestroy(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x000001A7 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_ReferenceCount()
// 0x000001A8 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::.ctor()
// 0x000001A9 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::ShortenPath(System.String,System.Boolean)
// 0x000001AA System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::IncrementReferenceCount()
// 0x000001AB System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::DecrementReferenceCount()
// 0x000001AC System.Threading.WaitHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_WaitHandle()
// 0x000001AD System.Threading.Tasks.Task`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Task()
// 0x000001AE System.Threading.Tasks.Task`1<System.Object> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Task()
// 0x000001AF System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::ToString()
// 0x000001B0 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::RegisterForDeferredCallbackEvent(System.Boolean)
// 0x000001B1 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x000001B2 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::remove_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x000001B3 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001B4 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001B5 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001B6 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001B7 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Status()
// 0x000001B8 System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_OperationException()
// 0x000001B9 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_OperationException(System.Exception)
// 0x000001BA System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::MoveNext()
// 0x000001BB System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Reset()
// 0x000001BC System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Current()
// 0x000001BD System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_IsDone()
// 0x000001BE System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_PercentComplete()
// 0x000001BF System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::InvokeCompletionEvent()
// 0x000001C0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Handle()
// 0x000001C1 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UpdateCallback(System.Single)
// 0x000001C2 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Complete(TObject,System.Boolean,System.String)
// 0x000001C3 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Start(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,DelegateList`1<System.Single>)
// 0x000001C4 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::InvokeExecute()
// 0x000001C5 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001C6 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001C7 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001C8 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001C9 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Version()
// 0x000001CA System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_ReferenceCount()
// 0x000001CB System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_PercentComplete()
// 0x000001CC UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Status()
// 0x000001CD System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_OperationException()
// 0x000001CE System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_IsDone()
// 0x000001CF UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Handle()
// 0x000001D0 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.set_OnDestroy(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x000001D1 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_DebugName()
// 0x000001D2 System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.GetResultAsObject()
// 0x000001D3 System.Type UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_ResultType()
// 0x000001D4 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001D5 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.DecrementReferenceCount()
// 0x000001D6 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.IncrementReferenceCount()
// 0x000001D7 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.InvokeCompletionEvent()
// 0x000001D8 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.Start(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,DelegateList`1<System.Single>)
// 0x000001D9 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::<.ctor>b__33_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x000001DA System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1_<>c__DisplayClass41_0::.ctor()
// 0x000001DB TObject UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1_<>c__DisplayClass41_0::<get_Task>b__0(System.Object)
// 0x000001DC UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::op_Implicit(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x000001DD System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject>)
// 0x000001DE System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
// 0x000001DF System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Int32)
// 0x000001E0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::Acquire()
// 0x000001E1 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x000001E2 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::remove_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x000001E3 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001E4 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001E5 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_DebugName()
// 0x000001E6 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001E7 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000001E8 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::Equals(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x000001E9 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::GetHashCode()
// 0x000001EA UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_InternalOp()
// 0x000001EB System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_IsDone()
// 0x000001EC System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::IsValid()
// 0x000001ED System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_OperationException()
// 0x000001EE System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_PercentComplete()
// 0x000001EF System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_ReferenceCount()
// 0x000001F0 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::Release()
// 0x000001F1 TObject UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_Result()
// 0x000001F2 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_Status()
// 0x000001F3 System.Threading.Tasks.Task`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_Task()
// 0x000001F4 System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::System.Collections.IEnumerator.get_Current()
// 0x000001F5 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::System.Collections.IEnumerator.MoveNext()
// 0x000001F6 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::System.Collections.IEnumerator.Reset()
// 0x000001F7 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void AsyncOperationHandle__ctor_m5CAF8329748264C0D78320A562DAEAC3AE5869C8_AdjustorThunk (void);
// 0x000001F8 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Int32)
extern void AsyncOperationHandle__ctor_m625B3C164EFE603B9C7F4444294CAAE1752B8A0F_AdjustorThunk (void);
// 0x000001F9 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::Acquire()
extern void AsyncOperationHandle_Acquire_m3F33353A41ED6BF37906DEA40CAA59E1C5755707_AdjustorThunk (void);
// 0x000001FA System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_add_Completed_mDC2AF6183B32C55FF890E226583D7E4A0FB35F09_AdjustorThunk (void);
// 0x000001FB System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::remove_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_remove_Completed_mDF8507A627AF13A1381BE1BF4C26890047E86EA5_AdjustorThunk (void);
// 0x000001FC UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<T> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::Convert()
// 0x000001FD System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_DebugName()
extern void AsyncOperationHandle_get_DebugName_m0324680BFDD4A65C585B3EA9E3F244186552C0C6_AdjustorThunk (void);
// 0x000001FE System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_add_Destroyed_mAF6A1270B2A69BE84318FEB73D36A19437EE3008_AdjustorThunk (void);
// 0x000001FF System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_remove_Destroyed_m6AB38E753AF3537B4F8DB8DC8D85FD6DF068D55D_AdjustorThunk (void);
// 0x00000200 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_GetDependencies_m35D1693217A8FCD56ECDAA6C933DF2A085AEE16E_AdjustorThunk (void);
// 0x00000201 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::GetHashCode()
extern void AsyncOperationHandle_GetHashCode_mACAE683D3FAFE3F49C50C2E9F64D97C1F5D1624B_AdjustorThunk (void);
// 0x00000202 UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_InternalOp()
extern void AsyncOperationHandle_get_InternalOp_m4EA78DBA5091741358AF08A77C4F74B7EB00463B_AdjustorThunk (void);
// 0x00000203 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_IsDone()
extern void AsyncOperationHandle_get_IsDone_mAC819A5E254D87077EB8D9BE8F36AFC30EBA0C4B_AdjustorThunk (void);
// 0x00000204 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::IsValid()
extern void AsyncOperationHandle_IsValid_m2D904B19EBBC50451B441161A762EE7703E5463C_AdjustorThunk (void);
// 0x00000205 System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_OperationException()
extern void AsyncOperationHandle_get_OperationException_mC27EB297729268618ADC7DDC3504B01CF3CA9695_AdjustorThunk (void);
// 0x00000206 System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_PercentComplete()
extern void AsyncOperationHandle_get_PercentComplete_m8020EBDC295B192EAA493D4E571E2501FE7AFA28_AdjustorThunk (void);
// 0x00000207 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_ReferenceCount()
extern void AsyncOperationHandle_get_ReferenceCount_m5D87AFB468776BBA67295B48381C78452566245D_AdjustorThunk (void);
// 0x00000208 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::Release()
extern void AsyncOperationHandle_Release_mF869257E566A31A9F6906CD0CC44DF88228C625C_AdjustorThunk (void);
// 0x00000209 System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_Result()
extern void AsyncOperationHandle_get_Result_m28D82A4A3AB9E7635A56AB16B7AE9FB631B682FC_AdjustorThunk (void);
// 0x0000020A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_Status()
extern void AsyncOperationHandle_get_Status_m518EE3BF10E74A8895F7E5699D5F1F2EAD5C0458_AdjustorThunk (void);
// 0x0000020B System.Threading.Tasks.Task`1<System.Object> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_Task()
extern void AsyncOperationHandle_get_Task_m2213154D1F81A13237EDBA72185106E6F772DB9F_AdjustorThunk (void);
// 0x0000020C System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::System.Collections.IEnumerator.get_Current()
extern void AsyncOperationHandle_System_Collections_IEnumerator_get_Current_m0D8EC2DBF3380292BD75E7776B65EA38EDEBF1FD_AdjustorThunk (void);
// 0x0000020D System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::System.Collections.IEnumerator.MoveNext()
extern void AsyncOperationHandle_System_Collections_IEnumerator_MoveNext_m25C17DDC68A5908D26C0D82A0EB210209B607725_AdjustorThunk (void);
// 0x0000020E System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::System.Collections.IEnumerator.Reset()
extern void AsyncOperationHandle_System_Collections_IEnumerator_Reset_m4375BF3606971221ACE039AA1254743A85B27C80_AdjustorThunk (void);
// 0x0000020F System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::.ctor()
extern void GroupOperation__ctor_m8BA1E7986541D04D4627F113549B525B69A19DCD (void);
// 0x00000210 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::UnityEngine.ResourceManagement.AsyncOperations.ICachable.get_Hash()
extern void GroupOperation_UnityEngine_ResourceManagement_AsyncOperations_ICachable_get_Hash_m8493D72E3565265872163D1571E41BA8198DFD76 (void);
// 0x00000211 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::UnityEngine.ResourceManagement.AsyncOperations.ICachable.set_Hash(System.Int32)
extern void GroupOperation_UnityEngine_ResourceManagement_AsyncOperations_ICachable_set_Hash_mFB6CA7F3685470BB694F99E1465FF8C110637FD3 (void);
// 0x00000212 System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::GetDependentOps()
extern void GroupOperation_GetDependentOps_m46BB9E741E15BEBC2AFC3FF3D650BCB4C90A2428 (void);
// 0x00000213 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void GroupOperation_GetDependencies_mC1D69E1844CED59D542EBCB9253ABDCF45DF8AF9 (void);
// 0x00000214 System.String UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::get_DebugName()
extern void GroupOperation_get_DebugName_m9832D4D4CA8BDD84DB23334C9941A0E525A0A318 (void);
// 0x00000215 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::Execute()
extern void GroupOperation_Execute_mFCA40CC8EC876FEE011CFFEEA02A3CDBE0D0B61A (void);
// 0x00000216 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::CompleteIfDependenciesComplete()
extern void GroupOperation_CompleteIfDependenciesComplete_m7227335A4896D7F6DAD05E27EF1DAE7D6CAFC1FC (void);
// 0x00000217 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::Destroy()
extern void GroupOperation_Destroy_mF4C74B691E92BFC415938EBE09CFB456AD36FA73 (void);
// 0x00000218 System.Single UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::get_Progress()
extern void GroupOperation_get_Progress_m06AE4CE1CC3A9487D832554002641CBA8E94DFDB (void);
// 0x00000219 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::Init(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void GroupOperation_Init_m1C27898EF932F693729AB928C9DDC933361219CB (void);
// 0x0000021A System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::OnOperationCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void GroupOperation_OnOperationCompleted_m98CA3532152B8FE12B9E48B75361E9BF4FE5F5B5 (void);
// 0x0000021B System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::Init(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
// 0x0000021C System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_ProvideHandleVersion()
// 0x0000021D UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_Location()
// 0x0000021E System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_DependencyCount()
// 0x0000021F System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::GetDependencies(System.Collections.Generic.IList`1<System.Object>)
// 0x00000220 TDepObject UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::GetDependency(System.Int32)
// 0x00000221 System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::SetProgressCallback(System.Func`1<System.Single>)
// 0x00000222 System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::ProviderCompleted(T,System.Boolean,System.Exception)
// 0x00000223 System.Type UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_RequestedType()
// 0x00000224 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::UnityEngine.ResourceManagement.AsyncOperations.ICachable.get_Hash()
// 0x00000225 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::UnityEngine.ResourceManagement.AsyncOperations.ICachable.set_Hash(System.Int32)
// 0x00000226 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_ProvideHandleVersion()
// 0x00000227 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_Location()
// 0x00000228 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::.ctor()
// 0x00000229 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x0000022A System.String UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_DebugName()
// 0x0000022B System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::GetDependencies(System.Collections.Generic.IList`1<System.Object>)
// 0x0000022C System.Type UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_RequestedType()
// 0x0000022D System.Int32 UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_DependencyCount()
// 0x0000022E TDepObject UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::GetDependency(System.Int32)
// 0x0000022F System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::SetProgressCallback(System.Func`1<System.Single>)
// 0x00000230 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::ProviderCompleted(T,System.Boolean,System.Exception)
// 0x00000231 System.Single UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::get_Progress()
// 0x00000232 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::Execute()
// 0x00000233 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::Init(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
// 0x00000234 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::Destroy()
static Il2CppMethodPointer s_methodPointers[564] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MonoBehaviourCallbackHooks_add_OnUpdateDelegate_mF0A7D9A91F2790E2B5E411D31ECC9B5B4B3DCD5D,
	MonoBehaviourCallbackHooks_remove_OnUpdateDelegate_m00215EA5426D369E8D7C7198819192B433D0E99C,
	MonoBehaviourCallbackHooks_Awake_mB89D60777166A27B306C9145D100DBF6625F4B9D,
	MonoBehaviourCallbackHooks_Update_mCC7B226851A48F80003FEB67801607356597F5A6,
	MonoBehaviourCallbackHooks__ctor_mD71C42E2866DE9CE172E4E61EB0214A9D4BFEB6E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ResourceManager_get_ExceptionHandler_m1594AC5FC006CC770B2A0E15C739EB845A9C8306,
	ResourceManager_set_ExceptionHandler_m2CD5AEF214219E421AF7BAA9057274336F72C6AF,
	ResourceManager_get_InternalIdTransformFunc_mB5CBFF62F538F2E9387BC31FA5DF4FDBC39813AB,
	ResourceManager_set_InternalIdTransformFunc_m8F62669979EAECF8ED0E7FA2CDE7D219826A250B,
	ResourceManager_TransformInternalId_m5B3836837396CC2CBF26C9D1B77809A73A835669,
	ResourceManager_get_OperationCacheCount_m4E3CB779AE01BD54B04544B20A761188B7C3DC40,
	ResourceManager_get_InstanceOperationCount_m7FF092E82B500F42DD10FBB204AA6D5113BF979D,
	ResourceManager_AddUpdateReceiver_mD35433816B5B3C8F3D986F8E9DE70872341CE4F3,
	ResourceManager_RemoveUpdateReciever_m53AAAD4F31B434164DCF15F4CF787FD104F490D8,
	ResourceManager_get_Allocator_m617C32C69B3AF34BA88127E5EE31B8B2731AE685,
	ResourceManager_set_Allocator_m605775DC52E410670040187897F0C17ED45BC6FB,
	ResourceManager_get_ResourceProviders_m0B0336D1A403DDDB560EB832375B0205AC2B914F,
	ResourceManager_get_CertificateHandlerInstance_m2E6389B8CA321D6850C1EF792F7F30CB85E6AD53,
	ResourceManager_set_CertificateHandlerInstance_m58A06D27892464F5DCB0CF162A4491F63D75BD95,
	ResourceManager__ctor_m8FC110F5D15AE6BB0225B82EE23B6FE589384A63,
	ResourceManager_OnObjectAdded_mB1214C5B5A091942F1BC9F78791FEFF111A5FA3D,
	ResourceManager_OnObjectRemoved_m65985C8F6D034E3125CC0A590CC5C986BEAC36C6,
	ResourceManager_RegisterForCallbacks_m446989CFD30CEAE21A30EDBC7CF5F19293BD57CB,
	ResourceManager_ClearDiagnosticsCallback_m10B6405E9C6F22FB92FC2F4DF590237A2129D696,
	ResourceManager_ClearDiagnosticCallbacks_mFCFC4DB25C14D5D116EF3C20048126747AB38B4F,
	ResourceManager_UnregisterDiagnosticCallback_m2F39BED801D8540D2BC2E4DFC0585A9B7C06F77A,
	ResourceManager_RegisterDiagnosticCallback_m670CAA7C86A37081D1D46BB9ACF98BA9DB7E04EA,
	ResourceManager_RegisterDiagnosticCallback_m5668FFB5E61FADC8859DE00A516B53183A493D83,
	ResourceManager_PostDiagnosticEvent_m7F57B61B13435BAFBF1D99DCC0D1B6579E904218,
	ResourceManager_GetResourceProvider_m5C1B277D488DA44F80077476F23451C8B9BB9840,
	ResourceManager_GetDefaultTypeForLocation_mACD27E5C1DB210607222FC55B6728A9FB96C9B00,
	ResourceManager_CalculateLocationsHash_mE546C5ECB0C13363246BED722B7E5FC899918BA0,
	ResourceManager_ProvideResource_mF4AA96051EB4B7F521F7C6D3D9463FD5F8265BEA,
	NULL,
	NULL,
	ResourceManager_StartOperation_m5F3A0530300FE62FCCA0AA55ADB4AF825AC0E630,
	ResourceManager_OnInstanceOperationDestroy_m51046A99199FE1384F610C8B2D4F9BB45909282F,
	ResourceManager_OnOperationDestroyNonCached_m6BDCF67A3CFC9BDD9A33B7F8693B8E583C00182E,
	ResourceManager_OnOperationDestroyCached_m68BA43A0BC60683A5FB3DC9CCFC003D03C4CF383,
	NULL,
	ResourceManager_AddOperationToCache_m30550FE4400DF6AA39E06D36EC605D74DC01A1DF,
	ResourceManager_RemoveOperationFromCache_m2AA7127F4700DA736A52B34F6EA954E8EEE247BF,
	ResourceManager_IsOperationCached_m0D86DF2785B2965D4670DB2D11C0279C5D4A6D48,
	ResourceManager_CachedOperationCount_m4B99483D311D33CE8FF7E38F87116FD602BF7F77,
	NULL,
	NULL,
	ResourceManager_Release_m1BF99536656867FEEFBEF3A3F4A290AA66402EB7,
	ResourceManager_Acquire_m071783042B4ACA5A506D43DCAF92DE8EBF94E5C2,
	ResourceManager_AcquireGroupOpFromCache_m951C52A488C4CFADF4561328E73E8787D2434A4C,
	NULL,
	ResourceManager_CreateGenericGroupOperation_m637EFAA4DDCCCB4DB5BDE75BBB4B53C61848D8F2,
	ResourceManager_ProvideResourceGroupCached_m7DA704CDBEB10A82FFCC259463A2A92EA00C62AC,
	NULL,
	NULL,
	NULL,
	ResourceManager_ProvideScene_mB6328DC75FFFD9CEF4B60511F895F1E2CF709A5D,
	ResourceManager_ReleaseScene_m459CB8CAAB851FA3495F881AEFAD1C3FF29106FA,
	ResourceManager_ProvideInstance_m9C1635807FC0ED58FB88BEF2B803BC9C1AAD14F3,
	ResourceManager_CleanupSceneInstances_m9B491B22C7532958A6751CDD08384E7474D1E8CC,
	ResourceManager_ExecuteDeferredCallbacks_mA834F0420F4C269DC21BCB39AFF67C51325AE373,
	ResourceManager_RegisterForDeferredCallback_mDE63306BA1891888B6E2C57E74D4CF8AF8F51952,
	ResourceManager_Update_mA2F03AF254D8794D37A7821F905AA8FF8EC97FFF,
	ResourceManager_Dispose_m57783F295F3FE72496F8B2CC2BC06C671643CF53,
	ResourceManager__cctor_m75EFB1F6063CB219CCEBBC30CF586872EEAC4F82,
	ResourceManager_U3C_ctorU3Eb__45_0_m264F9BEE44E4B9D4756AE7CB0D09A4C167CE6874,
	DiagnosticEventContext_get_OperationHandle_m870675A1B170E90324CB13D4320F2DEF29D6A1BD_AdjustorThunk,
	DiagnosticEventContext_get_Type_m3C77CCA8778F7D55230D59FD0B6DEB37F910B15E_AdjustorThunk,
	DiagnosticEventContext_get_EventValue_m186FB7D8DFACBD40B9A7FC254685B544C5FC5448_AdjustorThunk,
	DiagnosticEventContext_get_Location_mC7B8C8151070981ACEF34A2110E16024587691B7_AdjustorThunk,
	DiagnosticEventContext_get_Context_mB145AF762ACC916F5CBCD03B392B36CB6BBD5D91_AdjustorThunk,
	DiagnosticEventContext_get_Error_m6758C6A5F0F1C59C8AFB8A5D5FEF7547C806D808_AdjustorThunk,
	DiagnosticEventContext__ctor_m46A56FA95388629125EDEE74C865060584274238_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	InstanceOperation_Init_mE5EE6B621B9B927D75F2281EDDEC91E5DE16A9CA,
	InstanceOperation_GetDependencies_m26D3CB8DE0FCFC784B2C9663A482F66BD92C72E5,
	InstanceOperation_get_DebugName_mE303D6480456152374BCE927EE4737D33E70E7FE,
	InstanceOperation_InstanceScene_m6AF4F1151492C24B07C214A98DBAB5CBE64E3DD6,
	InstanceOperation_Destroy_mC17ABC3FF150BAE7C7C6DD031B741D1099C174AE,
	InstanceOperation_get_Progress_mFB3EC02597F244CEE84815611AFD4074E1028883,
	InstanceOperation_Execute_mAED30650EF9D31AB1A5CC87410964DFB4149BF2C,
	InstanceOperation__ctor_mBE633769231DE956B1D301282CC4D488F52495C8,
	NULL,
	NULL,
	NULL,
	NULL,
	WebRequestQueueOperation_get_IsDone_mA24CD38542F301A96BA73352520D86348D483079,
	WebRequestQueueOperation__ctor_mE3E3145649464B791DDC02D0BA75AEC6A5CA0B7E,
	WebRequestQueueOperation_Complete_mBF0118DBD2907500B5A87F494B14672DBF534763,
	WebRequestQueue_QueueRequest_m54F1A56E5082365292141AC67CEFEBEEA52F04AE,
	WebRequestQueue_OnWebAsyncOpComplete_m27F37E235E00801CCD54A4D8ED49538371FC0180,
	WebRequestQueue__cctor_m3B750ADE0715FB14DEBF388CA01266DF0B455925,
	ResourceManagerException__ctor_m846D7FCC2C06373694800B5220DF73E6D26F6BA7,
	ResourceManagerException__ctor_m9181C112F2C8E32E77CEDE21C4B5DF0F1CBB5948,
	ResourceManagerException__ctor_mE76839DF077E4AB88010D559E270AB53D2509A66,
	ResourceManagerException__ctor_mCE13F6D2BC0FAE0B8B66A9C13CFABA3EF7879F04,
	UnknownResourceProviderException_get_Location_mC37FB4E0339053A4F3C5DFDEFAC15F8B4E1A7CA6,
	UnknownResourceProviderException_set_Location_mDE78145787A8CDF8058E1FE350F79F23C6E0121E,
	UnknownResourceProviderException__ctor_mA08582F29A5DF6471FEE1170D7EB38CAFEAB5C5F,
	UnknownResourceProviderException__ctor_m4B7176BAE46F30C73450E5306F911BD53EDD151E,
	UnknownResourceProviderException__ctor_m365634FE703F611EDC55F3E4142B4E69EB72F682,
	UnknownResourceProviderException__ctor_m682FE80405FA7A8FAFEBBA1F7605379338B7BB97,
	UnknownResourceProviderException__ctor_m0729FF6A25B6CB353ECE181A7C907E15B64879FF,
	UnknownResourceProviderException_get_Message_mF411A2558453686665445094929B223C43FB6C80,
	UnknownResourceProviderException_ToString_m8E3FCBA2D2A41E951C60958FC604DC7D4A83C26E,
	DelayedActionManager_GetNode_m0DE3584622A131A6793E55EACD16CA4019947088,
	DelayedActionManager_Clear_mCE2BE0F40CA9566F3AD0FBC7506D2AAE6FC99380,
	DelayedActionManager_DestroyWhenComplete_mA1D76FBA64E4BBB878914741B403BF2904F9DA01,
	DelayedActionManager_AddAction_m50BBD4B75371CB8B2A2230DF09216C2F85CF9262,
	DelayedActionManager_AddActionInternal_m613DDDB8D8F5A44CBE0793FEDE3EA80E0FD0AC0B,
	DelayedActionManager_Awake_m7F2528CF205F37FB341CE53F10EAF90D5FB13260,
	DelayedActionManager_get_IsActive_mF1D78D5B0C9E9EA99B953C6F26868C53A02AC32F,
	DelayedActionManager_Wait_m18E9BC16EFA84B02DCFCF3C56C31EF2F4B356F8F,
	DelayedActionManager_LateUpdate_m54908B1CB07F9901C208ECD97A82BB62A9B70D91,
	DelayedActionManager_InternalLateUpdate_mD6B2568B45B54B79717B73422415240BFA28F37C,
	DelayedActionManager_OnApplicationQuit_m209580E2C7D3265295F1B1048880C96416F5E198,
	DelayedActionManager__ctor_mC4435A115C4DD5A9112A7B40CFD330F7D8340320,
	DelegateInfo__ctor_mBB59764DCA627416685191C4BFFDF7E6B3F9EEB0_AdjustorThunk,
	DelegateInfo_get_InvocationTime_mD426371CDDBF73985C41649D481C1C8032731780_AdjustorThunk,
	DelegateInfo_ToString_m088DA64541A8A73664E1C101FE2726B1325AC5BC_AdjustorThunk,
	DelegateInfo_Invoke_m9010FEA078121055ACEBF2CD5049762D439DDD8F_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DefaultAllocationStrategy_New_m08D4B93377956FCA42653F39C2675229759A80D1,
	DefaultAllocationStrategy_Release_m8AD07C016B04002C080A2745A6892A6852DD6D06,
	DefaultAllocationStrategy__ctor_mAA0485502871FF659EED040E6835C0C9053344B9,
	LRUCacheAllocationStrategy__ctor_m6AB5B8D1E389B9300ABFC78F7C1B9F33FE0977C9,
	LRUCacheAllocationStrategy_GetPool_mB24B50035BCC44F88A78479974237784205D6914,
	LRUCacheAllocationStrategy_ReleasePool_mA171571331A0C7B2C747FAC28483247D5F83B674,
	LRUCacheAllocationStrategy_New_m85F22B7C23EB9A073651189EA98D08B0292AF3F4,
	LRUCacheAllocationStrategy_Release_m37E39356812AF84792B4E23D0D34453DEB84E528,
	SerializedTypeRestrictionAttribute__ctor_m4060A47D9718A425D3C39090B02C1967B3A1D0E4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SerializedType_get_AssemblyName_m2DBDDEF51A9338BF298BCBBD2EE398D72E963635_AdjustorThunk,
	SerializedType_get_ClassName_m06DB26816F632CCAF380F818EA666D85C409D8B2_AdjustorThunk,
	SerializedType_ToString_m85C9B44C29867CD01DAA7CB5A365577818B4212A_AdjustorThunk,
	SerializedType_get_Value_m412D3B47698A74EFA8A3B29B4D102842714BD6B0_AdjustorThunk,
	SerializedType_set_Value_m0E1021AEE9B0E8B98800E79734E8B140032B9E54_AdjustorThunk,
	SerializedType_get_ValueChanged_m1B7B62A60583EE0D14418C391367985DBE6E7562_AdjustorThunk,
	SerializedType_set_ValueChanged_m7EB0C0A5F56B0594CC3578191F4E26CAC1D08D7D_AdjustorThunk,
	ObjectInitializationData_get_Id_m1455892EB3BA6CD3ED9F9A73F6401285E5486C65_AdjustorThunk,
	ObjectInitializationData_get_ObjectType_mB1B59BD388442F732B6FFA421AE1ED0AE3AE4532_AdjustorThunk,
	ObjectInitializationData_get_Data_m026CC155470843EE675C47D80DDE91CFA7B05AA0_AdjustorThunk,
	ObjectInitializationData_ToString_m43DC1ED8748A5B962C707EA72E529243ABDA37D6_AdjustorThunk,
	NULL,
	ObjectInitializationData_GetAsyncInitHandle_m3050F492A6972087E9AA5371C7946CBE5F29AC42_AdjustorThunk,
	ResourceManagerConfig_ExtractKeyAndSubKey_m48B90CB062DF102F56698425FE9F75795239ED15,
	ResourceManagerConfig_IsPathRemote_m4A0EC7B1CEFE8C1B172D2D7CA1FCC2EDDF09FA12,
	ResourceManagerConfig_ShouldPathUseWebRequest_mC7BD63058F88117DA2622430B22DB7B80D138D76,
	ResourceManagerConfig_CreateArrayResult_mE028A19D4D9E4908BE5911B8E7A21A85D18237B6,
	NULL,
	ResourceManagerConfig_CreateListResult_mE82A475A890F6091076CAC624DF32496CD75CF1F,
	NULL,
	NULL,
	NULL,
	AssetBundleRequestOptions_get_Hash_mB4B18C05B59265AA39B3A02C0688DE1D1C88711A,
	AssetBundleRequestOptions_set_Hash_mE0924B6592A1DFC9D594E11FF135AD1E87B41D3F,
	AssetBundleRequestOptions_get_Crc_m2BA7D512A54EA6075A0CF024D012B6433C8A9A4E,
	AssetBundleRequestOptions_set_Crc_m2FB9FEAAEBC071272C46D9F8C02AE3BC5FBCD635,
	AssetBundleRequestOptions_get_Timeout_m42D9843C4E83D152DAE0CE4CD3E8E65E0623080B,
	AssetBundleRequestOptions_set_Timeout_m40BFCFEF67A5C0D73A9F289EF8AA6A885479FC50,
	AssetBundleRequestOptions_get_ChunkedTransfer_m5D4E14BA8EB284968D3BD0CFC5473B69A1FB04CD,
	AssetBundleRequestOptions_set_ChunkedTransfer_m790165DE615D3A3D0CE204FEE2F4133D719CE499,
	AssetBundleRequestOptions_get_RedirectLimit_m099ACB2C16C55D0C615FF15E71908E3125D7F8AC,
	AssetBundleRequestOptions_set_RedirectLimit_m0B11D7F0C5B94AD17B0C7E1729130E8B5BE5D7E7,
	AssetBundleRequestOptions_get_RetryCount_m19487390C594350D557346716F1CB3C0B38C2B3F,
	AssetBundleRequestOptions_set_RetryCount_mA0A092315042B6625A1B8FBC5383491B7C414CD3,
	AssetBundleRequestOptions_get_BundleName_mC794C3CB5F0DD2ED320B34F44E2F586A55C24CF8,
	AssetBundleRequestOptions_set_BundleName_m9C66005D818F3CFDA4B79FDF4011CF27F0C2A72E,
	AssetBundleRequestOptions_get_BundleSize_m01BFA314FF232985809F91FA0D1711984582AD5E,
	AssetBundleRequestOptions_set_BundleSize_m26D31A8E9D68404B7F5977B490BFE815D5D01D02,
	AssetBundleRequestOptions_ComputeSize_m2E82047D611D69C7AADC6FD0151E2EF610E4D202,
	AssetBundleRequestOptions__ctor_m478D494DCB048FCC2462FE9D87ED34991E0D0869,
	AssetBundleResource_CreateWebRequest_m008113D1236E539144C558988C2D6F9D71881962,
	AssetBundleResource_PercentComplete_m83436C87C7EE6E0FF6C28FB7E05B841308DFA8AB,
	AssetBundleResource_GetAssetBundle_mEDCC866A7AF1A532F40B40B821DD457AC73FB8D0,
	AssetBundleResource_Start_m165368F7AD26E5F1C74190C95817A9D5757D2BA4,
	AssetBundleResource_BeginOperation_m8A9873499D80D88F50FC5F20FA039F0AEA50DC12,
	AssetBundleResource_LocalRequestOperationCompleted_m5B5121EDE02A34809BF275AD11A3D8785686FF5F,
	AssetBundleResource_WebRequestOperationCompleted_m36359C93B7E68673166D558680F003D314E4AD1C,
	AssetBundleResource_Unload_m141896873F3F31F8C587361583C8F71DC3F7049E,
	AssetBundleResource__ctor_mD5B4085DF1B5D6935198B89C651FDF6818C43835,
	AssetBundleResource_U3CBeginOperationU3Eb__11_0_mA2C74889E0B832B91D5FD9B4FEEA711083732D23,
	AssetBundleProvider_Provide_mD46D28C6CA670CA740FE87912320632FDB659C1E,
	AssetBundleProvider_GetDefaultType_m79830BEE6CE701C7DF95E65F33C5AF8B74A83852,
	AssetBundleProvider_Release_m48DDAE8401ADD5932DAAC61C4D161155FF43B3E6,
	AssetBundleProvider__ctor_m5C55E1DE6E0E02A232B307350CA4D76C187402B1,
	AtlasSpriteProvider_Provide_mF045676D19D97CD57A7BC0807027C8BB78217E15,
	AtlasSpriteProvider__ctor_m05488EDAFF03B092471403E9999CD0266C5AACED,
	BundledAssetProvider_Provide_m8C2EDDF78D3CCC187CC41F63E8FC695597C4A12E,
	BundledAssetProvider__ctor_mC25705358F17D4412D359E73C5F4578CF85192EB,
	InternalOp_LoadBundleFromDependecies_m5270426EB07F453668EE5ABB2C20FEE5F866C350,
	InternalOp_Start_mA10ADF4F26C477CD58C6E8F808165CCF1270A74D,
	InternalOp_ActionComplete_m2F628707CEA05C810AE258C5549C47EECABBCA9D,
	InternalOp_ProgressCallback_mFD402D64CE77420108145A6E6DB428E914D7BF57,
	InternalOp__ctor_mB8936B16062FBD1BCDE173A781968358E492FB7F,
	InstantiationParameters_get_Position_m9569A28B575846AD814790114A6CFAB8FC3E32DC_AdjustorThunk,
	InstantiationParameters_get_Rotation_m155362C3F3442CA4CC53C063FA8B4D09B753B012_AdjustorThunk,
	InstantiationParameters_get_Parent_mD6BC05225FF561D2123B1745B838D147D9B6A621_AdjustorThunk,
	InstantiationParameters_get_InstantiateInWorldPosition_mF6ACF2A60E6C89C55CC36D82220910EEA9CC65F4_AdjustorThunk,
	InstantiationParameters_get_SetPositionRotation_m3B5F3C4065F65ED2D4FD75B3B81D311B15EB70D1_AdjustorThunk,
	InstantiationParameters__ctor_m89D89058D329E9C651528936FD098282F1589225_AdjustorThunk,
	InstantiationParameters__ctor_mE86F10CB3D40AC7EA1883000E381AEAEA1E16A33_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	ProvideHandle__ctor_mB9CCF5CA6F8DD98AB72B4659076385051038795C_AdjustorThunk,
	ProvideHandle_get_InternalOp_mDCF56F543329C2078BA6B8CE99C39B6EF3BEA41F_AdjustorThunk,
	ProvideHandle_get_ResourceManager_mD9EAFA3333B5A459E1AD6EA76E15558C531ACD76_AdjustorThunk,
	ProvideHandle_get_Type_mC36FCA0E5806E401F98BC3D30511DE5015A81EFF_AdjustorThunk,
	ProvideHandle_get_Location_m38F31BC3081090D72C7EFA9C6CDE73ED45B7D978_AdjustorThunk,
	ProvideHandle_get_DependencyCount_m56B0737197C7470C2A5FF8E8E2BBDA151E572BA4_AdjustorThunk,
	NULL,
	ProvideHandle_GetDependencies_mC65A5900B3EA9F0874C3D19BEFFC022DFC9AD8E8_AdjustorThunk,
	ProvideHandle_SetProgressCallback_m3B3F019C3AFF1A6B0F37B5B28F9A1121EBB73FD2_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SceneInstance_get_Scene_mFF895316B46CBB8126ABE0FE8F85985D1232CCA3_AdjustorThunk,
	SceneInstance_set_Scene_m8AD506582C450F4C678C7265ED0A94FFDA9179E5_AdjustorThunk,
	SceneInstance_Activate_m52A0E7A28EEA007FF2529340E980D798374E5B0E_AdjustorThunk,
	SceneInstance_ActivateAsync_m0F5677DAD4ED2EC1EB027E261B4EBF40418A3DC3_AdjustorThunk,
	SceneInstance_GetHashCode_m9B65494AC6ED027B091B9FF2DE58DEE6E46FFF0B_AdjustorThunk,
	SceneInstance_Equals_mFD3D668A674D5838BF6F9ED892C0452E48729A43_AdjustorThunk,
	NULL,
	NULL,
	InstanceProvider_ProvideInstance_m530D2258769CC2B97647ECFBCF34659BD93C2C6B,
	InstanceProvider_ReleaseInstance_m9DB74D96301B8DD3263AF1AB860E46D85EE08690,
	InstanceProvider__ctor_m35BAD5E188BFB18C9BDCC7AE47F6B9338A7B5D2F,
	JsonAssetProvider_Convert_m97EC94F47C2FA697A38522205E3C972497113AE2,
	JsonAssetProvider__ctor_m15637BC90EF129F6F31918CDAE728E5D9560F303,
	LegacyResourcesProvider_Provide_m4253335CC4BDC849FAD018D5BBDBD3392D03691F,
	LegacyResourcesProvider_Release_m330D49267272717059BD526433B358E581083253,
	LegacyResourcesProvider__ctor_m578CAD18E82E4F66CEFCA1A30B00338FB677A4C6,
	InternalOp_Start_m8CF6A6B5D9675C182B0AAB762BC3E30752CA8EA5,
	InternalOp_AsyncOperationCompleted_m5821F76BA226C04C5188E80E48DC97EF186A72DA,
	InternalOp_PercentComplete_m3E24E8827F98571B4532D1A36382047CC296EBD8,
	InternalOp__ctor_mF1AFFC0E9D8BF2049B3255E65A87883BDDE2EF04,
	ResourceProviderBase_get_ProviderId_m39F6890BE274231ADB091ED135F06B475C7AD842,
	ResourceProviderBase_Initialize_mB88D098C7CA16F9B29CD25F127E586C60271DEC6,
	ResourceProviderBase_CanProvide_m124E66DC2469547DA7E58A5396A1FCAC3B470D85,
	ResourceProviderBase_ToString_m4ACCDF1D1E881C7CC1537F638B29F9BCC9654065,
	ResourceProviderBase_Release_mA4EA7CE2F575DA1E9B1A6B8EA9F21896DDC411A7,
	ResourceProviderBase_GetDefaultType_m00E90DFA2818C81077829B7AE9F9847834520E02,
	NULL,
	ResourceProviderBase_InitializeAsync_mA971647D0097330ACCD75D035481D16D8B8EEFFA,
	ResourceProviderBase_UnityEngine_ResourceManagement_ResourceProviders_IResourceProvider_get_BehaviourFlags_mFDEC1DF913DFD712F248FAD2A36DB610F7580F25,
	ResourceProviderBase__ctor_mA8589431AF866EE209169DA0E84D70845D0F7C1A,
	BaseInitAsyncOp_Init_m84C2E47DCA1E087F49D35279878445687E3E16F4,
	BaseInitAsyncOp_Execute_mB78A5ED65E7B68D61754EFBF6A73055F64F7A129,
	BaseInitAsyncOp__ctor_m7D92010EA1BB2C67B605DC95E8D91E500AC7FEDA,
	U3CU3Ec__DisplayClass10_0__ctor_m3B6FAF943AAB23C2BD7DF7E6AB9D1B3699703856,
	U3CU3Ec__DisplayClass10_0_U3CInitializeAsyncU3Eb__0_m19A259CF5D733F72D104DEFAD3348199EDF35F35,
	SceneProvider_ProvideScene_mCB31C5F58277AC5B97E3B0E10317FE34EDFD58F8,
	SceneProvider_ReleaseScene_m1A7A7B799DFEF64C68E7101546579CC907E0048A,
	SceneProvider__ctor_m1B35416831C52CC189753B5624976389772A45F7,
	SceneOp__ctor_m5B0A2397AF0B1ACF6E2FD5323BA6EC60B4DA0147,
	SceneOp_Init_mB918CB5783BF2635D2970748F35C9D2DE689304A,
	SceneOp_GetDependencies_mF478AD3651950802CCDD736B1FBC44C32FE692EB,
	SceneOp_get_DebugName_m7260EF67264EC70C626B11F4FFB8E73A0965610D,
	SceneOp_Execute_mCAB604D31CBC76E96284377779B79C74E78E2C6E,
	SceneOp_InternalLoadScene_m9C5B15DCB1405A84EA255ECE59B90D419D6C5653,
	SceneOp_InternalLoad_m157B04FAA5544C6CB3221855FD67178212D6E201,
	SceneOp_Destroy_m701CC2DE4463C54C16757EB3D0A0A94E71077696,
	SceneOp_get_Progress_m378936898C1A6DB059D093337B1DCC400689FB5B,
	SceneOp_UnityEngine_ResourceManagement_IUpdateReceiver_Update_m27FD39CB9D11C53DD786CDB722DD249F7D35217B,
	UnloadSceneOp_Init_mC30E36921F3E896B3B6E16765113955E285015A6,
	UnloadSceneOp_Execute_m1DFB8028F12E302AFE304F67E5BAAB3820B0EC4D,
	UnloadSceneOp_UnloadSceneCompleted_m357E9D44463E1A1E3E87A29FFF300C11D3E862F9,
	UnloadSceneOp_get_Progress_m42967C82BB0C649B805E107569933C78AAE18628,
	UnloadSceneOp__ctor_m051B216E7A8E5F7ED061FF3D0986D0BD9E9DF7A5,
	TextDataProvider_get_IgnoreFailures_m57066898F24CD1829FBD8BB87E0DEAA1CDC20C8B,
	TextDataProvider_set_IgnoreFailures_m5787E1C9DA529EF415D30ADDA2B74DBE78E6D825,
	TextDataProvider_Convert_mEBE0237D88E5C2D2F50D7441367D63906D0CDF75,
	TextDataProvider_Provide_m97C0755B7CB3C55D2D5CE99A26A0A650F887A69B,
	TextDataProvider__ctor_m7AC08BFDB0D65AFFF44638ADB897882432BF92DB,
	InternalOp_GetPercentComplete_m9ED8C8B9A92A008FC832003493795968212F040F,
	InternalOp_Start_m6A88CC1BB80FA2A3AFB0762B7DB973DA7BDBDEE8,
	InternalOp_RequestOperation_completed_m888C6797A4FF695796EAE9B6329A6B1EE896604B,
	InternalOp__ctor_m761A51452CEFD8FAD7575FC1940F3A2E0709DB54,
	InternalOp_U3CStartU3Eb__6_0_mF09E720D8E29AF360526ABC2C782EB4F43543992,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ResourceLocationBase_get_InternalId_mDD026BD02BC9C4BFE13018163D5C3333F87AC6CF,
	ResourceLocationBase_get_ProviderId_m3C0A8FFDD223BA3DB83C2BF36FA16A58F7D56603,
	ResourceLocationBase_get_Dependencies_mF557A83CF1A117CBCE78814D350BC09135BF4DF9,
	ResourceLocationBase_get_HasDependencies_mF968DBFE910DC0F2653AE1D3582149AF8D25DDD7,
	ResourceLocationBase_get_Data_m1FBDAD7E83CA4FE35DF61546CB4864F234760358,
	ResourceLocationBase_set_Data_mD899B389E585E1F781CD02CC2EEDFD38D80664BC,
	ResourceLocationBase_get_PrimaryKey_m2C276908F4E098BCED0F019A30872A7D2A02ACED,
	ResourceLocationBase_set_PrimaryKey_m989397E9A03B8CFDCBD151E087125BCC0C949145,
	ResourceLocationBase_get_DependencyHashCode_m1D0F112D82E12492981006CC695486AA85CF7A47,
	ResourceLocationBase_get_ResourceType_m1C6C8FA00487D034B8661EBB5323DEA49E76F631,
	ResourceLocationBase_Hash_mFD5291AA954FA36C6C833C3940F8A72A9C2AED77,
	ResourceLocationBase_ToString_m61190854FAFA7A73E0A9DE1647806A6491BE94A7,
	ResourceLocationBase__ctor_m8487509965168D66DD0079D6A096845204088A7F,
	ResourceLocationBase_ComputeDependencyHash_m9BB3035DD2ACAA3680109BD2DEB756F2DC1C1963,
	DiagnosticEvent_get_Graph_m710E1F0680B4D39DE0264D295E28650421AEEF6D_AdjustorThunk,
	DiagnosticEvent_get_ObjectId_mCA40B5EFDB9F507CAB17F2E19DA3F62D68B45537_AdjustorThunk,
	DiagnosticEvent_get_DisplayName_mCBA5175616B9E1B45C12C51A8FBD01B88E5A78EE_AdjustorThunk,
	DiagnosticEvent_get_Dependencies_m2751628387892A62E54CD3A32FD77DDD18626B27_AdjustorThunk,
	DiagnosticEvent_get_Stream_mD35CADC32CA28797FEB25F744975712D808874D2_AdjustorThunk,
	DiagnosticEvent_get_Frame_m80FE7279F8A049A54DF5D34EC80B0B8F4D33F9EB_AdjustorThunk,
	DiagnosticEvent_get_Value_mA831F1827F784283AF76FBB76D768ED1C00B9410_AdjustorThunk,
	DiagnosticEvent__ctor_m7010F47B7515B5EA548729B2F3E25EBFAEAEB217_AdjustorThunk,
	DiagnosticEvent_Serialize_m02EC58094A8703C0C89430022D36A3EE3C304607_AdjustorThunk,
	DiagnosticEvent_Deserialize_m80E559A631C1CADB8B5EE0B03A70D790C1459F3F,
	DiagnosticEventCollector_FindOrCreateGlobalInstance_m606D0B93253933F64A4F126E952F4AACAD95F1A6,
	DiagnosticEventCollector_get_PlayerConnectionGuid_mCDC64A92E85E48453A28C92A4EF9652C49E593C1,
	DiagnosticEventCollector_RegisterEventHandler_mD8110A8AE08413AA61A28352CD219880663BE7C3,
	DiagnosticEventCollector_RegisterEventHandler_m1BE4579F60E54AE14C6D406DC1B20F4799FCF556,
	DiagnosticEventCollector_UnregisterEventHandler_m39277B914A0C123866E0FA4320B82CA7662B5725,
	DiagnosticEventCollector_PostEvent_m66DEEEEFF15701593B3B600159AE12085330B151,
	DiagnosticEventCollector_Awake_m24F99AB24727466D99C550C3442FEF7DCE311696,
	DiagnosticEventCollector_Update_mCB7D61516ECB014E98D126B8977C47ED433D44C2,
	DiagnosticEventCollector_OnApplicationQuit_m89244AE9D73C45E66F8B6ACE666D7E4F17DC08FB,
	DiagnosticEventCollector__ctor_m31BB8D4B02E0F321D9D94F54D6DBBF07B4A91581,
	U3CU3Ec__cctor_m8F10B57D6DAF8E389CD8661515EB79DBC504BC37,
	U3CU3Ec__ctor_m8D57B362949EF3873FC890D681A810C942A0E6E2,
	U3CU3Ec_U3CAwakeU3Eb__12_0_mB96A865172FE3E0ACDEA9DAE045B1F8270A467E0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AsyncOperationHandle__ctor_m5CAF8329748264C0D78320A562DAEAC3AE5869C8_AdjustorThunk,
	AsyncOperationHandle__ctor_m625B3C164EFE603B9C7F4444294CAAE1752B8A0F_AdjustorThunk,
	AsyncOperationHandle_Acquire_m3F33353A41ED6BF37906DEA40CAA59E1C5755707_AdjustorThunk,
	AsyncOperationHandle_add_Completed_mDC2AF6183B32C55FF890E226583D7E4A0FB35F09_AdjustorThunk,
	AsyncOperationHandle_remove_Completed_mDF8507A627AF13A1381BE1BF4C26890047E86EA5_AdjustorThunk,
	NULL,
	AsyncOperationHandle_get_DebugName_m0324680BFDD4A65C585B3EA9E3F244186552C0C6_AdjustorThunk,
	AsyncOperationHandle_add_Destroyed_mAF6A1270B2A69BE84318FEB73D36A19437EE3008_AdjustorThunk,
	AsyncOperationHandle_remove_Destroyed_m6AB38E753AF3537B4F8DB8DC8D85FD6DF068D55D_AdjustorThunk,
	AsyncOperationHandle_GetDependencies_m35D1693217A8FCD56ECDAA6C933DF2A085AEE16E_AdjustorThunk,
	AsyncOperationHandle_GetHashCode_mACAE683D3FAFE3F49C50C2E9F64D97C1F5D1624B_AdjustorThunk,
	AsyncOperationHandle_get_InternalOp_m4EA78DBA5091741358AF08A77C4F74B7EB00463B_AdjustorThunk,
	AsyncOperationHandle_get_IsDone_mAC819A5E254D87077EB8D9BE8F36AFC30EBA0C4B_AdjustorThunk,
	AsyncOperationHandle_IsValid_m2D904B19EBBC50451B441161A762EE7703E5463C_AdjustorThunk,
	AsyncOperationHandle_get_OperationException_mC27EB297729268618ADC7DDC3504B01CF3CA9695_AdjustorThunk,
	AsyncOperationHandle_get_PercentComplete_m8020EBDC295B192EAA493D4E571E2501FE7AFA28_AdjustorThunk,
	AsyncOperationHandle_get_ReferenceCount_m5D87AFB468776BBA67295B48381C78452566245D_AdjustorThunk,
	AsyncOperationHandle_Release_mF869257E566A31A9F6906CD0CC44DF88228C625C_AdjustorThunk,
	AsyncOperationHandle_get_Result_m28D82A4A3AB9E7635A56AB16B7AE9FB631B682FC_AdjustorThunk,
	AsyncOperationHandle_get_Status_m518EE3BF10E74A8895F7E5699D5F1F2EAD5C0458_AdjustorThunk,
	AsyncOperationHandle_get_Task_m2213154D1F81A13237EDBA72185106E6F772DB9F_AdjustorThunk,
	AsyncOperationHandle_System_Collections_IEnumerator_get_Current_m0D8EC2DBF3380292BD75E7776B65EA38EDEBF1FD_AdjustorThunk,
	AsyncOperationHandle_System_Collections_IEnumerator_MoveNext_m25C17DDC68A5908D26C0D82A0EB210209B607725_AdjustorThunk,
	AsyncOperationHandle_System_Collections_IEnumerator_Reset_m4375BF3606971221ACE039AA1254743A85B27C80_AdjustorThunk,
	GroupOperation__ctor_m8BA1E7986541D04D4627F113549B525B69A19DCD,
	GroupOperation_UnityEngine_ResourceManagement_AsyncOperations_ICachable_get_Hash_m8493D72E3565265872163D1571E41BA8198DFD76,
	GroupOperation_UnityEngine_ResourceManagement_AsyncOperations_ICachable_set_Hash_mFB6CA7F3685470BB694F99E1465FF8C110637FD3,
	GroupOperation_GetDependentOps_m46BB9E741E15BEBC2AFC3FF3D650BCB4C90A2428,
	GroupOperation_GetDependencies_mC1D69E1844CED59D542EBCB9253ABDCF45DF8AF9,
	GroupOperation_get_DebugName_m9832D4D4CA8BDD84DB23334C9941A0E525A0A318,
	GroupOperation_Execute_mFCA40CC8EC876FEE011CFFEEA02A3CDBE0D0B61A,
	GroupOperation_CompleteIfDependenciesComplete_m7227335A4896D7F6DAD05E27EF1DAE7D6CAFC1FC,
	GroupOperation_Destroy_mF4C74B691E92BFC415938EBE09CFB456AD36FA73,
	GroupOperation_get_Progress_m06AE4CE1CC3A9487D832554002641CBA8E94DFDB,
	GroupOperation_Init_m1C27898EF932F693729AB928C9DDC933361219CB,
	GroupOperation_OnOperationCompleted_m98CA3532152B8FE12B9E48B75361E9BF4FE5F5B5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[564] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	26,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4,
	122,
	14,
	26,
	28,
	10,
	10,
	26,
	26,
	14,
	26,
	14,
	14,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	26,
	26,
	26,
	1430,
	113,
	28,
	41,
	1431,
	-1,
	-1,
	1432,
	26,
	26,
	26,
	-1,
	62,
	30,
	30,
	10,
	-1,
	-1,
	1433,
	1433,
	34,
	-1,
	1434,
	1435,
	-1,
	-1,
	-1,
	1436,
	1437,
	1438,
	1439,
	23,
	401,
	290,
	23,
	3,
	26,
	1440,
	10,
	10,
	14,
	14,
	14,
	1441,
	-1,
	-1,
	-1,
	1442,
	26,
	14,
	1261,
	23,
	681,
	23,
	23,
	-1,
	-1,
	-1,
	290,
	114,
	26,
	26,
	0,
	122,
	3,
	23,
	26,
	27,
	171,
	14,
	26,
	26,
	23,
	26,
	27,
	171,
	14,
	14,
	447,
	3,
	23,
	1443,
	1444,
	23,
	49,
	1236,
	23,
	290,
	23,
	23,
	1444,
	681,
	14,
	23,
	115,
	1445,
	14,
	1446,
	58,
	62,
	58,
	62,
	23,
	294,
	14,
	26,
	58,
	62,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	14,
	14,
	14,
	26,
	114,
	31,
	14,
	1447,
	14,
	14,
	-1,
	1431,
	315,
	94,
	94,
	1,
	-1,
	1,
	-1,
	-1,
	14,
	14,
	26,
	10,
	32,
	10,
	32,
	114,
	31,
	10,
	32,
	10,
	32,
	14,
	26,
	142,
	172,
	1448,
	23,
	28,
	681,
	14,
	1449,
	23,
	26,
	26,
	23,
	23,
	26,
	1449,
	28,
	27,
	23,
	1449,
	23,
	1449,
	23,
	0,
	1449,
	26,
	681,
	23,
	1121,
	1278,
	14,
	114,
	114,
	401,
	1450,
	-1,
	1451,
	27,
	27,
	14,
	14,
	14,
	14,
	10,
	-1,
	26,
	26,
	-1,
	14,
	28,
	115,
	1449,
	27,
	10,
	1261,
	1439,
	23,
	14,
	10,
	9,
	1436,
	1437,
	1451,
	27,
	23,
	113,
	23,
	1449,
	27,
	23,
	1449,
	26,
	681,
	23,
	14,
	115,
	115,
	14,
	27,
	28,
	1449,
	1445,
	10,
	23,
	26,
	23,
	23,
	23,
	114,
	1436,
	1437,
	23,
	26,
	1452,
	26,
	14,
	23,
	1453,
	1454,
	23,
	681,
	290,
	1455,
	23,
	26,
	681,
	23,
	114,
	31,
	113,
	1449,
	23,
	681,
	1456,
	26,
	23,
	26,
	1448,
	14,
	14,
	14,
	116,
	10,
	114,
	14,
	14,
	14,
	14,
	14,
	14,
	114,
	14,
	26,
	14,
	26,
	10,
	14,
	116,
	14,
	765,
	23,
	14,
	10,
	14,
	14,
	10,
	10,
	10,
	1457,
	14,
	1458,
	4,
	378,
	1459,
	26,
	26,
	1460,
	23,
	23,
	23,
	23,
	3,
	23,
	1460,
	10,
	32,
	14,
	14,
	10,
	14,
	23,
	23,
	10,
	681,
	10,
	14,
	114,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	14,
	1461,
	1440,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	136,
	1440,
	26,
	26,
	-1,
	14,
	26,
	26,
	26,
	10,
	14,
	114,
	114,
	14,
	681,
	10,
	23,
	14,
	10,
	14,
	14,
	114,
	23,
	23,
	10,
	32,
	14,
	26,
	14,
	23,
	23,
	23,
	681,
	26,
	1433,
	1462,
	10,
	14,
	10,
	26,
	-1,
	26,
	-1,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[31] = 
{
	{ 0x02000002, { 0, 20 } },
	{ 0x02000003, { 20, 23 } },
	{ 0x02000005, { 43, 23 } },
	{ 0x02000006, { 66, 15 } },
	{ 0x0200000A, { 116, 5 } },
	{ 0x0200000C, { 121, 8 } },
	{ 0x0200001A, { 129, 9 } },
	{ 0x0200001B, { 138, 5 } },
	{ 0x02000041, { 156, 50 } },
	{ 0x02000042, { 206, 2 } },
	{ 0x02000043, { 208, 22 } },
	{ 0x02000048, { 232, 10 } },
	{ 0x0600004E, { 81, 2 } },
	{ 0x0600004F, { 83, 2 } },
	{ 0x06000054, { 85, 1 } },
	{ 0x06000059, { 86, 4 } },
	{ 0x0600005A, { 90, 4 } },
	{ 0x0600005E, { 94, 3 } },
	{ 0x06000061, { 97, 9 } },
	{ 0x06000062, { 106, 6 } },
	{ 0x06000063, { 112, 4 } },
	{ 0x060000C8, { 143, 1 } },
	{ 0x060000CE, { 144, 2 } },
	{ 0x060000D0, { 146, 2 } },
	{ 0x060000D1, { 148, 2 } },
	{ 0x06000103, { 150, 4 } },
	{ 0x0600010C, { 154, 1 } },
	{ 0x0600010F, { 155, 1 } },
	{ 0x060001FC, { 230, 2 } },
	{ 0x0600022E, { 242, 1 } },
	{ 0x06000230, { 243, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[247] = 
{
	{ (Il2CppRGCTXDataType)3, 14915 },
	{ (Il2CppRGCTXDataType)3, 14916 },
	{ (Il2CppRGCTXDataType)2, 18850 },
	{ (Il2CppRGCTXDataType)3, 14917 },
	{ (Il2CppRGCTXDataType)3, 14918 },
	{ (Il2CppRGCTXDataType)3, 14919 },
	{ (Il2CppRGCTXDataType)3, 14920 },
	{ (Il2CppRGCTXDataType)3, 14921 },
	{ (Il2CppRGCTXDataType)3, 14922 },
	{ (Il2CppRGCTXDataType)3, 14923 },
	{ (Il2CppRGCTXDataType)3, 14924 },
	{ (Il2CppRGCTXDataType)3, 14925 },
	{ (Il2CppRGCTXDataType)3, 14926 },
	{ (Il2CppRGCTXDataType)2, 16386 },
	{ (Il2CppRGCTXDataType)3, 14927 },
	{ (Il2CppRGCTXDataType)3, 14928 },
	{ (Il2CppRGCTXDataType)2, 16390 },
	{ (Il2CppRGCTXDataType)3, 14929 },
	{ (Il2CppRGCTXDataType)2, 16391 },
	{ (Il2CppRGCTXDataType)3, 14930 },
	{ (Il2CppRGCTXDataType)2, 16397 },
	{ (Il2CppRGCTXDataType)3, 14931 },
	{ (Il2CppRGCTXDataType)3, 14932 },
	{ (Il2CppRGCTXDataType)3, 14933 },
	{ (Il2CppRGCTXDataType)3, 14934 },
	{ (Il2CppRGCTXDataType)3, 14935 },
	{ (Il2CppRGCTXDataType)3, 14936 },
	{ (Il2CppRGCTXDataType)3, 14937 },
	{ (Il2CppRGCTXDataType)2, 18686 },
	{ (Il2CppRGCTXDataType)3, 14938 },
	{ (Il2CppRGCTXDataType)3, 14939 },
	{ (Il2CppRGCTXDataType)3, 14940 },
	{ (Il2CppRGCTXDataType)3, 14941 },
	{ (Il2CppRGCTXDataType)2, 18851 },
	{ (Il2CppRGCTXDataType)3, 14942 },
	{ (Il2CppRGCTXDataType)3, 14943 },
	{ (Il2CppRGCTXDataType)3, 14944 },
	{ (Il2CppRGCTXDataType)3, 14945 },
	{ (Il2CppRGCTXDataType)3, 14946 },
	{ (Il2CppRGCTXDataType)3, 14947 },
	{ (Il2CppRGCTXDataType)3, 14948 },
	{ (Il2CppRGCTXDataType)2, 18852 },
	{ (Il2CppRGCTXDataType)3, 14949 },
	{ (Il2CppRGCTXDataType)3, 14950 },
	{ (Il2CppRGCTXDataType)2, 16410 },
	{ (Il2CppRGCTXDataType)3, 14951 },
	{ (Il2CppRGCTXDataType)2, 18853 },
	{ (Il2CppRGCTXDataType)3, 14952 },
	{ (Il2CppRGCTXDataType)1, 16411 },
	{ (Il2CppRGCTXDataType)1, 16413 },
	{ (Il2CppRGCTXDataType)3, 14953 },
	{ (Il2CppRGCTXDataType)3, 14954 },
	{ (Il2CppRGCTXDataType)2, 16412 },
	{ (Il2CppRGCTXDataType)3, 14955 },
	{ (Il2CppRGCTXDataType)3, 14956 },
	{ (Il2CppRGCTXDataType)3, 14957 },
	{ (Il2CppRGCTXDataType)3, 14958 },
	{ (Il2CppRGCTXDataType)3, 14959 },
	{ (Il2CppRGCTXDataType)3, 14960 },
	{ (Il2CppRGCTXDataType)3, 14961 },
	{ (Il2CppRGCTXDataType)3, 14962 },
	{ (Il2CppRGCTXDataType)3, 14963 },
	{ (Il2CppRGCTXDataType)3, 14964 },
	{ (Il2CppRGCTXDataType)3, 14965 },
	{ (Il2CppRGCTXDataType)3, 14966 },
	{ (Il2CppRGCTXDataType)3, 14967 },
	{ (Il2CppRGCTXDataType)3, 14968 },
	{ (Il2CppRGCTXDataType)2, 16422 },
	{ (Il2CppRGCTXDataType)3, 14969 },
	{ (Il2CppRGCTXDataType)2, 18854 },
	{ (Il2CppRGCTXDataType)3, 14970 },
	{ (Il2CppRGCTXDataType)1, 16423 },
	{ (Il2CppRGCTXDataType)3, 14971 },
	{ (Il2CppRGCTXDataType)3, 14972 },
	{ (Il2CppRGCTXDataType)3, 14973 },
	{ (Il2CppRGCTXDataType)3, 14974 },
	{ (Il2CppRGCTXDataType)3, 14975 },
	{ (Il2CppRGCTXDataType)3, 14976 },
	{ (Il2CppRGCTXDataType)3, 14977 },
	{ (Il2CppRGCTXDataType)3, 14978 },
	{ (Il2CppRGCTXDataType)3, 14979 },
	{ (Il2CppRGCTXDataType)1, 16436 },
	{ (Il2CppRGCTXDataType)3, 14980 },
	{ (Il2CppRGCTXDataType)3, 14981 },
	{ (Il2CppRGCTXDataType)3, 14982 },
	{ (Il2CppRGCTXDataType)2, 16440 },
	{ (Il2CppRGCTXDataType)1, 18855 },
	{ (Il2CppRGCTXDataType)3, 14983 },
	{ (Il2CppRGCTXDataType)3, 14984 },
	{ (Il2CppRGCTXDataType)3, 14985 },
	{ (Il2CppRGCTXDataType)1, 18856 },
	{ (Il2CppRGCTXDataType)3, 14986 },
	{ (Il2CppRGCTXDataType)3, 14987 },
	{ (Il2CppRGCTXDataType)3, 14988 },
	{ (Il2CppRGCTXDataType)3, 14989 },
	{ (Il2CppRGCTXDataType)3, 14990 },
	{ (Il2CppRGCTXDataType)2, 18858 },
	{ (Il2CppRGCTXDataType)2, 18859 },
	{ (Il2CppRGCTXDataType)3, 14991 },
	{ (Il2CppRGCTXDataType)3, 14992 },
	{ (Il2CppRGCTXDataType)3, 14993 },
	{ (Il2CppRGCTXDataType)1, 16446 },
	{ (Il2CppRGCTXDataType)3, 14994 },
	{ (Il2CppRGCTXDataType)2, 18860 },
	{ (Il2CppRGCTXDataType)3, 14995 },
	{ (Il2CppRGCTXDataType)3, 14996 },
	{ (Il2CppRGCTXDataType)1, 18861 },
	{ (Il2CppRGCTXDataType)3, 14997 },
	{ (Il2CppRGCTXDataType)3, 14998 },
	{ (Il2CppRGCTXDataType)3, 14999 },
	{ (Il2CppRGCTXDataType)2, 16450 },
	{ (Il2CppRGCTXDataType)3, 15000 },
	{ (Il2CppRGCTXDataType)2, 18862 },
	{ (Il2CppRGCTXDataType)3, 15001 },
	{ (Il2CppRGCTXDataType)3, 15002 },
	{ (Il2CppRGCTXDataType)3, 15003 },
	{ (Il2CppRGCTXDataType)3, 15004 },
	{ (Il2CppRGCTXDataType)2, 16485 },
	{ (Il2CppRGCTXDataType)3, 15005 },
	{ (Il2CppRGCTXDataType)3, 15006 },
	{ (Il2CppRGCTXDataType)3, 15007 },
	{ (Il2CppRGCTXDataType)2, 16497 },
	{ (Il2CppRGCTXDataType)3, 15008 },
	{ (Il2CppRGCTXDataType)3, 15009 },
	{ (Il2CppRGCTXDataType)2, 18863 },
	{ (Il2CppRGCTXDataType)3, 15010 },
	{ (Il2CppRGCTXDataType)3, 15011 },
	{ (Il2CppRGCTXDataType)3, 15012 },
	{ (Il2CppRGCTXDataType)3, 15013 },
	{ (Il2CppRGCTXDataType)3, 15014 },
	{ (Il2CppRGCTXDataType)3, 15015 },
	{ (Il2CppRGCTXDataType)3, 15016 },
	{ (Il2CppRGCTXDataType)2, 16536 },
	{ (Il2CppRGCTXDataType)3, 15017 },
	{ (Il2CppRGCTXDataType)2, 18864 },
	{ (Il2CppRGCTXDataType)3, 15018 },
	{ (Il2CppRGCTXDataType)3, 15019 },
	{ (Il2CppRGCTXDataType)3, 15020 },
	{ (Il2CppRGCTXDataType)2, 18865 },
	{ (Il2CppRGCTXDataType)2, 18866 },
	{ (Il2CppRGCTXDataType)3, 15021 },
	{ (Il2CppRGCTXDataType)3, 15022 },
	{ (Il2CppRGCTXDataType)3, 15023 },
	{ (Il2CppRGCTXDataType)2, 16545 },
	{ (Il2CppRGCTXDataType)1, 16549 },
	{ (Il2CppRGCTXDataType)2, 16549 },
	{ (Il2CppRGCTXDataType)1, 16550 },
	{ (Il2CppRGCTXDataType)2, 16550 },
	{ (Il2CppRGCTXDataType)1, 18867 },
	{ (Il2CppRGCTXDataType)1, 18868 },
	{ (Il2CppRGCTXDataType)3, 15024 },
	{ (Il2CppRGCTXDataType)3, 15025 },
	{ (Il2CppRGCTXDataType)3, 15026 },
	{ (Il2CppRGCTXDataType)3, 15027 },
	{ (Il2CppRGCTXDataType)3, 15028 },
	{ (Il2CppRGCTXDataType)3, 15029 },
	{ (Il2CppRGCTXDataType)3, 15030 },
	{ (Il2CppRGCTXDataType)3, 15031 },
	{ (Il2CppRGCTXDataType)3, 15032 },
	{ (Il2CppRGCTXDataType)2, 16643 },
	{ (Il2CppRGCTXDataType)3, 15033 },
	{ (Il2CppRGCTXDataType)3, 15034 },
	{ (Il2CppRGCTXDataType)2, 16643 },
	{ (Il2CppRGCTXDataType)3, 15035 },
	{ (Il2CppRGCTXDataType)3, 15036 },
	{ (Il2CppRGCTXDataType)2, 18869 },
	{ (Il2CppRGCTXDataType)3, 15037 },
	{ (Il2CppRGCTXDataType)3, 15038 },
	{ (Il2CppRGCTXDataType)3, 15039 },
	{ (Il2CppRGCTXDataType)3, 15040 },
	{ (Il2CppRGCTXDataType)3, 15041 },
	{ (Il2CppRGCTXDataType)3, 15042 },
	{ (Il2CppRGCTXDataType)2, 18870 },
	{ (Il2CppRGCTXDataType)3, 15043 },
	{ (Il2CppRGCTXDataType)3, 15044 },
	{ (Il2CppRGCTXDataType)3, 15045 },
	{ (Il2CppRGCTXDataType)2, 16640 },
	{ (Il2CppRGCTXDataType)3, 15046 },
	{ (Il2CppRGCTXDataType)3, 15047 },
	{ (Il2CppRGCTXDataType)2, 18871 },
	{ (Il2CppRGCTXDataType)3, 15048 },
	{ (Il2CppRGCTXDataType)3, 15049 },
	{ (Il2CppRGCTXDataType)3, 15050 },
	{ (Il2CppRGCTXDataType)3, 15051 },
	{ (Il2CppRGCTXDataType)3, 15052 },
	{ (Il2CppRGCTXDataType)3, 15053 },
	{ (Il2CppRGCTXDataType)3, 15054 },
	{ (Il2CppRGCTXDataType)3, 15055 },
	{ (Il2CppRGCTXDataType)3, 15056 },
	{ (Il2CppRGCTXDataType)3, 15057 },
	{ (Il2CppRGCTXDataType)3, 15058 },
	{ (Il2CppRGCTXDataType)3, 15059 },
	{ (Il2CppRGCTXDataType)3, 15060 },
	{ (Il2CppRGCTXDataType)3, 15061 },
	{ (Il2CppRGCTXDataType)3, 15062 },
	{ (Il2CppRGCTXDataType)3, 15063 },
	{ (Il2CppRGCTXDataType)3, 15064 },
	{ (Il2CppRGCTXDataType)3, 15065 },
	{ (Il2CppRGCTXDataType)3, 15066 },
	{ (Il2CppRGCTXDataType)3, 15067 },
	{ (Il2CppRGCTXDataType)3, 15068 },
	{ (Il2CppRGCTXDataType)3, 15069 },
	{ (Il2CppRGCTXDataType)3, 15070 },
	{ (Il2CppRGCTXDataType)1, 16640 },
	{ (Il2CppRGCTXDataType)3, 15071 },
	{ (Il2CppRGCTXDataType)3, 15072 },
	{ (Il2CppRGCTXDataType)2, 18872 },
	{ (Il2CppRGCTXDataType)3, 15073 },
	{ (Il2CppRGCTXDataType)3, 15074 },
	{ (Il2CppRGCTXDataType)2, 16659 },
	{ (Il2CppRGCTXDataType)3, 15075 },
	{ (Il2CppRGCTXDataType)3, 15076 },
	{ (Il2CppRGCTXDataType)3, 15077 },
	{ (Il2CppRGCTXDataType)3, 15078 },
	{ (Il2CppRGCTXDataType)3, 15079 },
	{ (Il2CppRGCTXDataType)3, 15080 },
	{ (Il2CppRGCTXDataType)3, 15081 },
	{ (Il2CppRGCTXDataType)3, 15082 },
	{ (Il2CppRGCTXDataType)3, 15083 },
	{ (Il2CppRGCTXDataType)3, 15084 },
	{ (Il2CppRGCTXDataType)3, 15085 },
	{ (Il2CppRGCTXDataType)3, 15086 },
	{ (Il2CppRGCTXDataType)3, 15087 },
	{ (Il2CppRGCTXDataType)3, 15088 },
	{ (Il2CppRGCTXDataType)3, 15089 },
	{ (Il2CppRGCTXDataType)3, 15090 },
	{ (Il2CppRGCTXDataType)3, 15091 },
	{ (Il2CppRGCTXDataType)3, 15092 },
	{ (Il2CppRGCTXDataType)2, 16658 },
	{ (Il2CppRGCTXDataType)3, 15093 },
	{ (Il2CppRGCTXDataType)2, 16665 },
	{ (Il2CppRGCTXDataType)3, 15094 },
	{ (Il2CppRGCTXDataType)3, 15095 },
	{ (Il2CppRGCTXDataType)2, 16675 },
	{ (Il2CppRGCTXDataType)1, 16676 },
	{ (Il2CppRGCTXDataType)3, 15096 },
	{ (Il2CppRGCTXDataType)2, 16676 },
	{ (Il2CppRGCTXDataType)3, 15097 },
	{ (Il2CppRGCTXDataType)3, 15098 },
	{ (Il2CppRGCTXDataType)3, 15099 },
	{ (Il2CppRGCTXDataType)3, 15100 },
	{ (Il2CppRGCTXDataType)3, 15101 },
	{ (Il2CppRGCTXDataType)2, 16677 },
	{ (Il2CppRGCTXDataType)2, 18873 },
	{ (Il2CppRGCTXDataType)3, 15102 },
	{ (Il2CppRGCTXDataType)2, 16678 },
	{ (Il2CppRGCTXDataType)1, 16678 },
};
extern const Il2CppCodeGenModule g_Unity_ResourceManagerCodeGenModule;
const Il2CppCodeGenModule g_Unity_ResourceManagerCodeGenModule = 
{
	"Unity.ResourceManager.dll",
	564,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	31,
	s_rgctxIndices,
	247,
	s_rgctxValues,
	NULL,
};

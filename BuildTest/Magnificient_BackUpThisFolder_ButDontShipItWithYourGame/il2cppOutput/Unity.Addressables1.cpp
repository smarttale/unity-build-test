﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// DelegateList`1<System.Single>
struct DelegateList_1_t84A2304C03E7E50A20DA212DB29667268E5D1FF9;
// DelegateList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>
struct DelegateList_1_tFE9D647F19045792FB2F3A6F5C0423B587C3FBEF;
// DelegateList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean>>
struct DelegateList_1_t934676D0C0A439F5133F93BA4D6CAEF53246857D;
// ListWithEvents`1<UnityEngine.ResourceManagement.IUpdateReceiver>
struct ListWithEvents_1_t29F49258EC052214FA1B66613CDAA553413BE17A;
// ListWithEvents`1<UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider>
struct ListWithEvents_1_tEC855A9309D46CF2635360C0CFD982AEFA1FE8EE;
// MonoBehaviourCallbackHooks
struct MonoBehaviourCallbackHooks_tFE3B10443874A74FBD26493990143F2290FE9367;
// System.Action`1<System.Single>
struct Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B;
// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>
struct Action_1_tFD2C45936FFEFF834ADD256A019613F56F3C812D;
// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>>
struct Action_1_t389A8EC91B4195D32771077FA7561379B6C05341;
// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Object>>
struct Action_1_t7071BA67566E765F87D6F9EA78100A9F83D14126;
// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>>
struct Action_1_tBF08AE776AB54CB17683A82FBF2E04F2414102FA;
// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>
struct Action_1_tDF402D078DE7362DD66194EC7B7A3911E268248D;
// System.Action`1<UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventContext>
struct Action_1_t0B507E141BD1D7ED1A8796B004BC508F54179E6B;
// System.Action`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception>
struct Action_2_t2A796D4B5D29AB687DB34BD2CDD60E65BB960A13;
// System.Action`4<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.ResourceManager/DiagnosticEventType,System.Int32,System.Object>
struct Action_4_t3926FA8FE97D8B27DFA00F83668C64C1EF84A697;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Attribute
struct Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>
struct Dictionary_2_tC02995D395CD134B9FF0662AB7A9B00048E1ED31;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider>
struct Dictionary_2_t255FD73C3EBCB5F0EE4903D2834B25E6609FA6A0;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>
struct Dictionary_2_t651A59872C12A00EC9ABCFA3DF7473DE63B9FFCD;
// System.Collections.Generic.Dictionary`2<System.Type,System.Type>
struct Dictionary_2_t4FD61322E30F37864A2014CF2AE3865F68FE1350;
// System.Collections.Generic.HashSet`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>>
struct HashSet_1_t3B7DEDC5739276883FCD0D0292F6CFA261B2B1EC;
// System.Collections.Generic.HashSet`1<UnityEngine.ResourceManagement.ResourceManager/InstanceOperation>
struct HashSet_1_t776DAB72EEC51B345B95AFC2DC6409D149324BEC;
// System.Collections.Generic.List`1<UnityEngine.AddressableAssets.AddressablesImpl/ResourceLocatorInfo>
struct List_1_tCF67F7248D1F5DA68CE48741AFBEF7049EA7F73A;
// System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData>
struct List_1_tA175E163ACBD5E7590190434C50069AA85893EC3;
// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>
struct List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086;
// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>
struct List_1_tCDB46450860E034A164853800FCDE785B52B6215;
// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.IUpdateReceiver>
struct List_1_t2AB7039E944EA028CF58DE14BC60F5D004317E82;
// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData>
struct List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Exception
struct Exception_t;
// System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String>
struct Func_2_t3B2DDF57F77856820B955CAC0CB7D99F104EB6B8;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.EventWaitHandle
struct EventWaitHandle_t7603BF1D3D30FE42DD07A450C8D09E2684DC4D98;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AddressableAssets.AddressablesImpl
struct AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F;
// UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData
struct ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48;
// UnityEngine.AssetReferenceUILabelRestriction
struct AssetReferenceUILabelRestriction_tC86FE955656AEBB6380E056746F7B8F6BFD440FC;
// UnityEngine.AssetReferenceUIRestriction
struct AssetReferenceUIRestriction_t19426D4B2B24EABFE27348F6DC49C87CE1188372;
// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Boolean>
struct AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74;
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>
struct AsyncOperationBase_1_tD0DA112AF674DF19546EB8D3CC321170BD45E8E2;
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Collections.Generic.List`1<System.String>>
struct AsyncOperationBase_1_tCB6651FCAC9C45F199C266CF8A0CC1C38481F94A;
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>>
struct AsyncOperationBase_1_tA703A406D650003DFD899E6FF0FE268DE8F23476;
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Object>
struct AsyncOperationBase_1_t871FD4E305BA7BBB480F9360D985884672783E66;
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData>
struct AsyncOperationBase_1_t2C0A8C316A68BE5968D340B4620C07B16532CBE4;
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>
struct AsyncOperationBase_1_t1A58BC34AE713084A1207092089995B195724A1A;
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle[]
struct AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456;
// UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation
struct IAsyncOperation_t95BFE028EE1B4B3BF86C0A386CB3FE057D6E4104;
// UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation
struct InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D;
// UnityEngine.ResourceManagement.ResourceManager
struct ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99;
// UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider
struct IInstanceProvider_t77C8CD099706D5B4D157104EB25570F4E87D9148;
// UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider
struct ISceneProvider_tA53FD0CACA45676436E0C56C2A1CFDB59807CBB2;
// UnityEngine.ResourceManagement.Util.IAllocationStrategy
struct IAllocationStrategy_t5E79614EE5CAB3506B0EB1D43D1A062FE5F67CE5;
// UnityEngine.ResourceManagement.Util.ObjectInitializationData[]
struct ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t389A8EC91B4195D32771077FA7561379B6C05341_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Addressables_t8AD39E84886280F198451740C05CA7E8AF2C977D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral3C1B63DEBF79CE695956E89613C7F2BEAFC4DEBC;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralEC42778CF59128EE5C88854642BEE80ECEC71773;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_m73358EC02BB8EAA16DFB0FE0227640F292A167E9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AddressablesImpl_Release_TisIList_1_t3CE2E108B6096E5AFDF51C64286BBC99DEC503C0_mA065D73B7C91D29DC3D4D497A6DC6B2C241DA354_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncOperationBase_1_Complete_mB8120801F44252CB88D19F0465F5C0380562B981_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncOperationBase_1__ctor_m6CF95D0597E37D0B3A3296C8E8B25D60ECE6A182_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncOperationHandle_1_add_Completed_m19650AF5F2DBD7964B1A3DE4513B920662BCFA35_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncOperationHandle_1_get_DebugName_mF0F80D803F94C19CB3459DD6638D06701FEDF006_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncOperationHandle_1_get_Result_m4AD1404D8FBA522A55420CEE072E5CBF2FEB58F9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncOperationHandle_1_get_Status_m06913A00983A3310A70A1F3AD960835579053077_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m4E4C6A22F5BE48284A81551C4E485FA4A53127A7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m6631D8F567209AA1D5E64ED69435D093F7180817_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mB825ABD75EF6878CF22490D684078AAB196C41EC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InitalizationObjectsOperation_U3CExecuteU3Eb__4_0_mA8AD2C34C9075665F7B90B8A1A1A256B9AF2A1BE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mE0B35AF41D06A2B921242CDEAC5EB0E6B1BD0883_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m142C3F136F4385E36BFBB76E7C1D83696DBF5E43_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m7BC0B7F8AD988AAA732E6098D9233366BF3A0F54_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t AssetReferenceUILabelRestriction_ToString_m5DE963979BB3001AFBCC5FF6DF76423C8BC85BA6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InitalizationObjectsOperation_Execute_m57D25A512B8B7F18B7E07625179726C4142B00FF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InitalizationObjectsOperation_U3CExecuteU3Eb__4_0_mA8AD2C34C9075665F7B90B8A1A1A256B9AF2A1BE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InitalizationObjectsOperation__ctor_m4A866392D28009599D93EA895B2F860D8D0CB902_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};


// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>
struct  List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086, ____items_1)); }
	inline AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456* get__items_1() const { return ____items_1; }
	inline AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086_StaticFields, ____emptyArray_5)); }
	inline AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456* get__emptyArray_5() const { return ____emptyArray_5; }
	inline AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(AsyncOperationHandleU5BU5D_t3C85EED2766560FBE2B80D38B47902525DEE7456* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData>
struct  List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F, ____items_1)); }
	inline ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1* get__items_1() const { return ____items_1; }
	inline ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F_StaticFields, ____emptyArray_5)); }
	inline ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectInitializationDataU5BU5D_tC2A7D51F0C69BD1BBFCE12D990327C43D082AAA1* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.Text.StringBuilder
struct  StringBuilder_t  : public RuntimeObject
{
public:
	// System.Char[] System.Text.StringBuilder::m_ChunkChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_ChunkChars_0;
	// System.Text.StringBuilder System.Text.StringBuilder::m_ChunkPrevious
	StringBuilder_t * ___m_ChunkPrevious_1;
	// System.Int32 System.Text.StringBuilder::m_ChunkLength
	int32_t ___m_ChunkLength_2;
	// System.Int32 System.Text.StringBuilder::m_ChunkOffset
	int32_t ___m_ChunkOffset_3;
	// System.Int32 System.Text.StringBuilder::m_MaxCapacity
	int32_t ___m_MaxCapacity_4;

public:
	inline static int32_t get_offset_of_m_ChunkChars_0() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkChars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_ChunkChars_0() const { return ___m_ChunkChars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_ChunkChars_0() { return &___m_ChunkChars_0; }
	inline void set_m_ChunkChars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_ChunkChars_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkChars_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkPrevious_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkPrevious_1)); }
	inline StringBuilder_t * get_m_ChunkPrevious_1() const { return ___m_ChunkPrevious_1; }
	inline StringBuilder_t ** get_address_of_m_ChunkPrevious_1() { return &___m_ChunkPrevious_1; }
	inline void set_m_ChunkPrevious_1(StringBuilder_t * value)
	{
		___m_ChunkPrevious_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkPrevious_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkLength_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkLength_2)); }
	inline int32_t get_m_ChunkLength_2() const { return ___m_ChunkLength_2; }
	inline int32_t* get_address_of_m_ChunkLength_2() { return &___m_ChunkLength_2; }
	inline void set_m_ChunkLength_2(int32_t value)
	{
		___m_ChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_m_ChunkOffset_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkOffset_3)); }
	inline int32_t get_m_ChunkOffset_3() const { return ___m_ChunkOffset_3; }
	inline int32_t* get_address_of_m_ChunkOffset_3() { return &___m_ChunkOffset_3; }
	inline void set_m_ChunkOffset_3(int32_t value)
	{
		___m_ChunkOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_MaxCapacity_4)); }
	inline int32_t get_m_MaxCapacity_4() const { return ___m_MaxCapacity_4; }
	inline int32_t* get_address_of_m_MaxCapacity_4() { return &___m_MaxCapacity_4; }
	inline void set_m_MaxCapacity_4(int32_t value)
	{
		___m_MaxCapacity_4 = value;
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.ResourceManagement.ResourceManager
struct  ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99  : public RuntimeObject
{
public:
	// System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String> UnityEngine.ResourceManagement.ResourceManager::<InternalIdTransformFunc>k__BackingField
	Func_2_t3B2DDF57F77856820B955CAC0CB7D99F104EB6B8 * ___U3CInternalIdTransformFuncU3Ek__BackingField_1;
	// System.Boolean UnityEngine.ResourceManagement.ResourceManager::CallbackHooksEnabled
	bool ___CallbackHooksEnabled_2;
	// MonoBehaviourCallbackHooks UnityEngine.ResourceManagement.ResourceManager::m_CallbackHooks
	MonoBehaviourCallbackHooks_tFE3B10443874A74FBD26493990143F2290FE9367 * ___m_CallbackHooks_3;
	// ListWithEvents`1<UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider> UnityEngine.ResourceManagement.ResourceManager::m_ResourceProviders
	ListWithEvents_1_tEC855A9309D46CF2635360C0CFD982AEFA1FE8EE * ___m_ResourceProviders_4;
	// UnityEngine.ResourceManagement.Util.IAllocationStrategy UnityEngine.ResourceManagement.ResourceManager::m_allocator
	RuntimeObject* ___m_allocator_5;
	// ListWithEvents`1<UnityEngine.ResourceManagement.IUpdateReceiver> UnityEngine.ResourceManagement.ResourceManager::m_UpdateReceivers
	ListWithEvents_1_t29F49258EC052214FA1B66613CDAA553413BE17A * ___m_UpdateReceivers_6;
	// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.IUpdateReceiver> UnityEngine.ResourceManagement.ResourceManager::m_UpdateReceiversToRemove
	List_1_t2AB7039E944EA028CF58DE14BC60F5D004317E82 * ___m_UpdateReceiversToRemove_7;
	// System.Boolean UnityEngine.ResourceManagement.ResourceManager::m_UpdatingReceivers
	bool ___m_UpdatingReceivers_8;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider> UnityEngine.ResourceManagement.ResourceManager::m_providerMap
	Dictionary_2_t255FD73C3EBCB5F0EE4903D2834B25E6609FA6A0 * ___m_providerMap_9;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation> UnityEngine.ResourceManagement.ResourceManager::m_AssetOperationCache
	Dictionary_2_tC02995D395CD134B9FF0662AB7A9B00048E1ED31 * ___m_AssetOperationCache_10;
	// System.Collections.Generic.HashSet`1<UnityEngine.ResourceManagement.ResourceManager_InstanceOperation> UnityEngine.ResourceManagement.ResourceManager::m_TrackedInstanceOperations
	HashSet_1_t776DAB72EEC51B345B95AFC2DC6409D149324BEC * ___m_TrackedInstanceOperations_11;
	// DelegateList`1<System.Single> UnityEngine.ResourceManagement.ResourceManager::m_UpdateCallbacks
	DelegateList_1_t84A2304C03E7E50A20DA212DB29667268E5D1FF9 * ___m_UpdateCallbacks_12;
	// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation> UnityEngine.ResourceManagement.ResourceManager::m_DeferredCompleteCallbacks
	List_1_tCDB46450860E034A164853800FCDE785B52B6215 * ___m_DeferredCompleteCallbacks_13;
	// System.Action`4<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventType,System.Int32,System.Object> UnityEngine.ResourceManagement.ResourceManager::m_obsoleteDiagnosticsHandler
	Action_4_t3926FA8FE97D8B27DFA00F83668C64C1EF84A697 * ___m_obsoleteDiagnosticsHandler_14;
	// System.Action`1<UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext> UnityEngine.ResourceManagement.ResourceManager::m_diagnosticsHandler
	Action_1_t0B507E141BD1D7ED1A8796B004BC508F54179E6B * ___m_diagnosticsHandler_15;
	// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation> UnityEngine.ResourceManagement.ResourceManager::m_ReleaseOpNonCached
	Action_1_tDF402D078DE7362DD66194EC7B7A3911E268248D * ___m_ReleaseOpNonCached_16;
	// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation> UnityEngine.ResourceManagement.ResourceManager::m_ReleaseOpCached
	Action_1_tDF402D078DE7362DD66194EC7B7A3911E268248D * ___m_ReleaseOpCached_17;
	// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation> UnityEngine.ResourceManagement.ResourceManager::m_ReleaseInstanceOp
	Action_1_tDF402D078DE7362DD66194EC7B7A3911E268248D * ___m_ReleaseInstanceOp_18;
	// UnityEngine.Networking.CertificateHandler UnityEngine.ResourceManagement.ResourceManager::<CertificateHandlerInstance>k__BackingField
	CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0 * ___U3CCertificateHandlerInstanceU3Ek__BackingField_21;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Type> UnityEngine.ResourceManagement.ResourceManager::m_ProviderOperationTypeCache
	Dictionary_2_t4FD61322E30F37864A2014CF2AE3865F68FE1350 * ___m_ProviderOperationTypeCache_22;

public:
	inline static int32_t get_offset_of_U3CInternalIdTransformFuncU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___U3CInternalIdTransformFuncU3Ek__BackingField_1)); }
	inline Func_2_t3B2DDF57F77856820B955CAC0CB7D99F104EB6B8 * get_U3CInternalIdTransformFuncU3Ek__BackingField_1() const { return ___U3CInternalIdTransformFuncU3Ek__BackingField_1; }
	inline Func_2_t3B2DDF57F77856820B955CAC0CB7D99F104EB6B8 ** get_address_of_U3CInternalIdTransformFuncU3Ek__BackingField_1() { return &___U3CInternalIdTransformFuncU3Ek__BackingField_1; }
	inline void set_U3CInternalIdTransformFuncU3Ek__BackingField_1(Func_2_t3B2DDF57F77856820B955CAC0CB7D99F104EB6B8 * value)
	{
		___U3CInternalIdTransformFuncU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInternalIdTransformFuncU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_CallbackHooksEnabled_2() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___CallbackHooksEnabled_2)); }
	inline bool get_CallbackHooksEnabled_2() const { return ___CallbackHooksEnabled_2; }
	inline bool* get_address_of_CallbackHooksEnabled_2() { return &___CallbackHooksEnabled_2; }
	inline void set_CallbackHooksEnabled_2(bool value)
	{
		___CallbackHooksEnabled_2 = value;
	}

	inline static int32_t get_offset_of_m_CallbackHooks_3() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_CallbackHooks_3)); }
	inline MonoBehaviourCallbackHooks_tFE3B10443874A74FBD26493990143F2290FE9367 * get_m_CallbackHooks_3() const { return ___m_CallbackHooks_3; }
	inline MonoBehaviourCallbackHooks_tFE3B10443874A74FBD26493990143F2290FE9367 ** get_address_of_m_CallbackHooks_3() { return &___m_CallbackHooks_3; }
	inline void set_m_CallbackHooks_3(MonoBehaviourCallbackHooks_tFE3B10443874A74FBD26493990143F2290FE9367 * value)
	{
		___m_CallbackHooks_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CallbackHooks_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_ResourceProviders_4() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_ResourceProviders_4)); }
	inline ListWithEvents_1_tEC855A9309D46CF2635360C0CFD982AEFA1FE8EE * get_m_ResourceProviders_4() const { return ___m_ResourceProviders_4; }
	inline ListWithEvents_1_tEC855A9309D46CF2635360C0CFD982AEFA1FE8EE ** get_address_of_m_ResourceProviders_4() { return &___m_ResourceProviders_4; }
	inline void set_m_ResourceProviders_4(ListWithEvents_1_tEC855A9309D46CF2635360C0CFD982AEFA1FE8EE * value)
	{
		___m_ResourceProviders_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ResourceProviders_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_allocator_5() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_allocator_5)); }
	inline RuntimeObject* get_m_allocator_5() const { return ___m_allocator_5; }
	inline RuntimeObject** get_address_of_m_allocator_5() { return &___m_allocator_5; }
	inline void set_m_allocator_5(RuntimeObject* value)
	{
		___m_allocator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_allocator_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_UpdateReceivers_6() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_UpdateReceivers_6)); }
	inline ListWithEvents_1_t29F49258EC052214FA1B66613CDAA553413BE17A * get_m_UpdateReceivers_6() const { return ___m_UpdateReceivers_6; }
	inline ListWithEvents_1_t29F49258EC052214FA1B66613CDAA553413BE17A ** get_address_of_m_UpdateReceivers_6() { return &___m_UpdateReceivers_6; }
	inline void set_m_UpdateReceivers_6(ListWithEvents_1_t29F49258EC052214FA1B66613CDAA553413BE17A * value)
	{
		___m_UpdateReceivers_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UpdateReceivers_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_UpdateReceiversToRemove_7() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_UpdateReceiversToRemove_7)); }
	inline List_1_t2AB7039E944EA028CF58DE14BC60F5D004317E82 * get_m_UpdateReceiversToRemove_7() const { return ___m_UpdateReceiversToRemove_7; }
	inline List_1_t2AB7039E944EA028CF58DE14BC60F5D004317E82 ** get_address_of_m_UpdateReceiversToRemove_7() { return &___m_UpdateReceiversToRemove_7; }
	inline void set_m_UpdateReceiversToRemove_7(List_1_t2AB7039E944EA028CF58DE14BC60F5D004317E82 * value)
	{
		___m_UpdateReceiversToRemove_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UpdateReceiversToRemove_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_UpdatingReceivers_8() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_UpdatingReceivers_8)); }
	inline bool get_m_UpdatingReceivers_8() const { return ___m_UpdatingReceivers_8; }
	inline bool* get_address_of_m_UpdatingReceivers_8() { return &___m_UpdatingReceivers_8; }
	inline void set_m_UpdatingReceivers_8(bool value)
	{
		___m_UpdatingReceivers_8 = value;
	}

	inline static int32_t get_offset_of_m_providerMap_9() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_providerMap_9)); }
	inline Dictionary_2_t255FD73C3EBCB5F0EE4903D2834B25E6609FA6A0 * get_m_providerMap_9() const { return ___m_providerMap_9; }
	inline Dictionary_2_t255FD73C3EBCB5F0EE4903D2834B25E6609FA6A0 ** get_address_of_m_providerMap_9() { return &___m_providerMap_9; }
	inline void set_m_providerMap_9(Dictionary_2_t255FD73C3EBCB5F0EE4903D2834B25E6609FA6A0 * value)
	{
		___m_providerMap_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_providerMap_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_AssetOperationCache_10() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_AssetOperationCache_10)); }
	inline Dictionary_2_tC02995D395CD134B9FF0662AB7A9B00048E1ED31 * get_m_AssetOperationCache_10() const { return ___m_AssetOperationCache_10; }
	inline Dictionary_2_tC02995D395CD134B9FF0662AB7A9B00048E1ED31 ** get_address_of_m_AssetOperationCache_10() { return &___m_AssetOperationCache_10; }
	inline void set_m_AssetOperationCache_10(Dictionary_2_tC02995D395CD134B9FF0662AB7A9B00048E1ED31 * value)
	{
		___m_AssetOperationCache_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AssetOperationCache_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedInstanceOperations_11() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_TrackedInstanceOperations_11)); }
	inline HashSet_1_t776DAB72EEC51B345B95AFC2DC6409D149324BEC * get_m_TrackedInstanceOperations_11() const { return ___m_TrackedInstanceOperations_11; }
	inline HashSet_1_t776DAB72EEC51B345B95AFC2DC6409D149324BEC ** get_address_of_m_TrackedInstanceOperations_11() { return &___m_TrackedInstanceOperations_11; }
	inline void set_m_TrackedInstanceOperations_11(HashSet_1_t776DAB72EEC51B345B95AFC2DC6409D149324BEC * value)
	{
		___m_TrackedInstanceOperations_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedInstanceOperations_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_UpdateCallbacks_12() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_UpdateCallbacks_12)); }
	inline DelegateList_1_t84A2304C03E7E50A20DA212DB29667268E5D1FF9 * get_m_UpdateCallbacks_12() const { return ___m_UpdateCallbacks_12; }
	inline DelegateList_1_t84A2304C03E7E50A20DA212DB29667268E5D1FF9 ** get_address_of_m_UpdateCallbacks_12() { return &___m_UpdateCallbacks_12; }
	inline void set_m_UpdateCallbacks_12(DelegateList_1_t84A2304C03E7E50A20DA212DB29667268E5D1FF9 * value)
	{
		___m_UpdateCallbacks_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UpdateCallbacks_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_DeferredCompleteCallbacks_13() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_DeferredCompleteCallbacks_13)); }
	inline List_1_tCDB46450860E034A164853800FCDE785B52B6215 * get_m_DeferredCompleteCallbacks_13() const { return ___m_DeferredCompleteCallbacks_13; }
	inline List_1_tCDB46450860E034A164853800FCDE785B52B6215 ** get_address_of_m_DeferredCompleteCallbacks_13() { return &___m_DeferredCompleteCallbacks_13; }
	inline void set_m_DeferredCompleteCallbacks_13(List_1_tCDB46450860E034A164853800FCDE785B52B6215 * value)
	{
		___m_DeferredCompleteCallbacks_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DeferredCompleteCallbacks_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_obsoleteDiagnosticsHandler_14() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_obsoleteDiagnosticsHandler_14)); }
	inline Action_4_t3926FA8FE97D8B27DFA00F83668C64C1EF84A697 * get_m_obsoleteDiagnosticsHandler_14() const { return ___m_obsoleteDiagnosticsHandler_14; }
	inline Action_4_t3926FA8FE97D8B27DFA00F83668C64C1EF84A697 ** get_address_of_m_obsoleteDiagnosticsHandler_14() { return &___m_obsoleteDiagnosticsHandler_14; }
	inline void set_m_obsoleteDiagnosticsHandler_14(Action_4_t3926FA8FE97D8B27DFA00F83668C64C1EF84A697 * value)
	{
		___m_obsoleteDiagnosticsHandler_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_obsoleteDiagnosticsHandler_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_diagnosticsHandler_15() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_diagnosticsHandler_15)); }
	inline Action_1_t0B507E141BD1D7ED1A8796B004BC508F54179E6B * get_m_diagnosticsHandler_15() const { return ___m_diagnosticsHandler_15; }
	inline Action_1_t0B507E141BD1D7ED1A8796B004BC508F54179E6B ** get_address_of_m_diagnosticsHandler_15() { return &___m_diagnosticsHandler_15; }
	inline void set_m_diagnosticsHandler_15(Action_1_t0B507E141BD1D7ED1A8796B004BC508F54179E6B * value)
	{
		___m_diagnosticsHandler_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_diagnosticsHandler_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_ReleaseOpNonCached_16() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_ReleaseOpNonCached_16)); }
	inline Action_1_tDF402D078DE7362DD66194EC7B7A3911E268248D * get_m_ReleaseOpNonCached_16() const { return ___m_ReleaseOpNonCached_16; }
	inline Action_1_tDF402D078DE7362DD66194EC7B7A3911E268248D ** get_address_of_m_ReleaseOpNonCached_16() { return &___m_ReleaseOpNonCached_16; }
	inline void set_m_ReleaseOpNonCached_16(Action_1_tDF402D078DE7362DD66194EC7B7A3911E268248D * value)
	{
		___m_ReleaseOpNonCached_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ReleaseOpNonCached_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_ReleaseOpCached_17() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_ReleaseOpCached_17)); }
	inline Action_1_tDF402D078DE7362DD66194EC7B7A3911E268248D * get_m_ReleaseOpCached_17() const { return ___m_ReleaseOpCached_17; }
	inline Action_1_tDF402D078DE7362DD66194EC7B7A3911E268248D ** get_address_of_m_ReleaseOpCached_17() { return &___m_ReleaseOpCached_17; }
	inline void set_m_ReleaseOpCached_17(Action_1_tDF402D078DE7362DD66194EC7B7A3911E268248D * value)
	{
		___m_ReleaseOpCached_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ReleaseOpCached_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_ReleaseInstanceOp_18() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_ReleaseInstanceOp_18)); }
	inline Action_1_tDF402D078DE7362DD66194EC7B7A3911E268248D * get_m_ReleaseInstanceOp_18() const { return ___m_ReleaseInstanceOp_18; }
	inline Action_1_tDF402D078DE7362DD66194EC7B7A3911E268248D ** get_address_of_m_ReleaseInstanceOp_18() { return &___m_ReleaseInstanceOp_18; }
	inline void set_m_ReleaseInstanceOp_18(Action_1_tDF402D078DE7362DD66194EC7B7A3911E268248D * value)
	{
		___m_ReleaseInstanceOp_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ReleaseInstanceOp_18), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCertificateHandlerInstanceU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___U3CCertificateHandlerInstanceU3Ek__BackingField_21)); }
	inline CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0 * get_U3CCertificateHandlerInstanceU3Ek__BackingField_21() const { return ___U3CCertificateHandlerInstanceU3Ek__BackingField_21; }
	inline CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0 ** get_address_of_U3CCertificateHandlerInstanceU3Ek__BackingField_21() { return &___U3CCertificateHandlerInstanceU3Ek__BackingField_21; }
	inline void set_U3CCertificateHandlerInstanceU3Ek__BackingField_21(CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0 * value)
	{
		___U3CCertificateHandlerInstanceU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCertificateHandlerInstanceU3Ek__BackingField_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_ProviderOperationTypeCache_22() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99, ___m_ProviderOperationTypeCache_22)); }
	inline Dictionary_2_t4FD61322E30F37864A2014CF2AE3865F68FE1350 * get_m_ProviderOperationTypeCache_22() const { return ___m_ProviderOperationTypeCache_22; }
	inline Dictionary_2_t4FD61322E30F37864A2014CF2AE3865F68FE1350 ** get_address_of_m_ProviderOperationTypeCache_22() { return &___m_ProviderOperationTypeCache_22; }
	inline void set_m_ProviderOperationTypeCache_22(Dictionary_2_t4FD61322E30F37864A2014CF2AE3865F68FE1350 * value)
	{
		___m_ProviderOperationTypeCache_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ProviderOperationTypeCache_22), (void*)value);
	}
};

struct ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99_StaticFields
{
public:
	// System.Action`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception> UnityEngine.ResourceManagement.ResourceManager::<ExceptionHandler>k__BackingField
	Action_2_t2A796D4B5D29AB687DB34BD2CDD60E65BB960A13 * ___U3CExceptionHandlerU3Ek__BackingField_0;
	// System.Int32 UnityEngine.ResourceManagement.ResourceManager::s_GroupOperationTypeHash
	int32_t ___s_GroupOperationTypeHash_19;
	// System.Int32 UnityEngine.ResourceManagement.ResourceManager::s_InstanceOperationTypeHash
	int32_t ___s_InstanceOperationTypeHash_20;

public:
	inline static int32_t get_offset_of_U3CExceptionHandlerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99_StaticFields, ___U3CExceptionHandlerU3Ek__BackingField_0)); }
	inline Action_2_t2A796D4B5D29AB687DB34BD2CDD60E65BB960A13 * get_U3CExceptionHandlerU3Ek__BackingField_0() const { return ___U3CExceptionHandlerU3Ek__BackingField_0; }
	inline Action_2_t2A796D4B5D29AB687DB34BD2CDD60E65BB960A13 ** get_address_of_U3CExceptionHandlerU3Ek__BackingField_0() { return &___U3CExceptionHandlerU3Ek__BackingField_0; }
	inline void set_U3CExceptionHandlerU3Ek__BackingField_0(Action_2_t2A796D4B5D29AB687DB34BD2CDD60E65BB960A13 * value)
	{
		___U3CExceptionHandlerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CExceptionHandlerU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_GroupOperationTypeHash_19() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99_StaticFields, ___s_GroupOperationTypeHash_19)); }
	inline int32_t get_s_GroupOperationTypeHash_19() const { return ___s_GroupOperationTypeHash_19; }
	inline int32_t* get_address_of_s_GroupOperationTypeHash_19() { return &___s_GroupOperationTypeHash_19; }
	inline void set_s_GroupOperationTypeHash_19(int32_t value)
	{
		___s_GroupOperationTypeHash_19 = value;
	}

	inline static int32_t get_offset_of_s_InstanceOperationTypeHash_20() { return static_cast<int32_t>(offsetof(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99_StaticFields, ___s_InstanceOperationTypeHash_20)); }
	inline int32_t get_s_InstanceOperationTypeHash_20() const { return ___s_InstanceOperationTypeHash_20; }
	inline int32_t* get_address_of_s_InstanceOperationTypeHash_20() { return &___s_InstanceOperationTypeHash_20; }
	inline void set_s_InstanceOperationTypeHash_20(int32_t value)
	{
		___s_InstanceOperationTypeHash_20 = value;
	}
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Char
struct  Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.AssetReferenceUIRestriction
struct  AssetReferenceUIRestriction_t19426D4B2B24EABFE27348F6DC49C87CE1188372  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};


// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle
struct  AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185 
{
public:
	// UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::m_InternalOp
	RuntimeObject* ___m_InternalOp_0;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_InternalOp_0() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185, ___m_InternalOp_0)); }
	inline RuntimeObject* get_m_InternalOp_0() const { return ___m_InternalOp_0; }
	inline RuntimeObject** get_address_of_m_InternalOp_0() { return &___m_InternalOp_0; }
	inline void set_m_InternalOp_0(RuntimeObject* value)
	{
		___m_InternalOp_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InternalOp_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle
struct AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185_marshaled_pinvoke
{
	RuntimeObject* ___m_InternalOp_0;
	int32_t ___m_Version_1;
};
// Native definition for COM marshalling of UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle
struct AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185_marshaled_com
{
	RuntimeObject* ___m_InternalOp_0;
	int32_t ___m_Version_1;
};

// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>
struct  AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143 
{
public:
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_InternalOp
	AsyncOperationBase_1_tD0DA112AF674DF19546EB8D3CC321170BD45E8E2 * ___m_InternalOp_0;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_InternalOp_0() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143, ___m_InternalOp_0)); }
	inline AsyncOperationBase_1_tD0DA112AF674DF19546EB8D3CC321170BD45E8E2 * get_m_InternalOp_0() const { return ___m_InternalOp_0; }
	inline AsyncOperationBase_1_tD0DA112AF674DF19546EB8D3CC321170BD45E8E2 ** get_address_of_m_InternalOp_0() { return &___m_InternalOp_0; }
	inline void set_m_InternalOp_0(AsyncOperationBase_1_tD0DA112AF674DF19546EB8D3CC321170BD45E8E2 * value)
	{
		___m_InternalOp_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InternalOp_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};


// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<System.String>>
struct  AsyncOperationHandle_1_tD513DCF6D023777BD9D4B31860F6CE33807DA134 
{
public:
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_InternalOp
	AsyncOperationBase_1_tCB6651FCAC9C45F199C266CF8A0CC1C38481F94A * ___m_InternalOp_0;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_InternalOp_0() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_tD513DCF6D023777BD9D4B31860F6CE33807DA134, ___m_InternalOp_0)); }
	inline AsyncOperationBase_1_tCB6651FCAC9C45F199C266CF8A0CC1C38481F94A * get_m_InternalOp_0() const { return ___m_InternalOp_0; }
	inline AsyncOperationBase_1_tCB6651FCAC9C45F199C266CF8A0CC1C38481F94A ** get_address_of_m_InternalOp_0() { return &___m_InternalOp_0; }
	inline void set_m_InternalOp_0(AsyncOperationBase_1_tCB6651FCAC9C45F199C266CF8A0CC1C38481F94A * value)
	{
		___m_InternalOp_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InternalOp_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_tD513DCF6D023777BD9D4B31860F6CE33807DA134, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};


// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>>
struct  AsyncOperationHandle_1_tCCD5A5B26068FD234B8318DFF5696127B1C63AF1 
{
public:
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_InternalOp
	AsyncOperationBase_1_tA703A406D650003DFD899E6FF0FE268DE8F23476 * ___m_InternalOp_0;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_InternalOp_0() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_tCCD5A5B26068FD234B8318DFF5696127B1C63AF1, ___m_InternalOp_0)); }
	inline AsyncOperationBase_1_tA703A406D650003DFD899E6FF0FE268DE8F23476 * get_m_InternalOp_0() const { return ___m_InternalOp_0; }
	inline AsyncOperationBase_1_tA703A406D650003DFD899E6FF0FE268DE8F23476 ** get_address_of_m_InternalOp_0() { return &___m_InternalOp_0; }
	inline void set_m_InternalOp_0(AsyncOperationBase_1_tA703A406D650003DFD899E6FF0FE268DE8F23476 * value)
	{
		___m_InternalOp_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InternalOp_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_tCCD5A5B26068FD234B8318DFF5696127B1C63AF1, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};


// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Object>
struct  AsyncOperationHandle_1_tA3D61DC158CEEADB7D17CA260C0AF36D84E79632 
{
public:
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_InternalOp
	AsyncOperationBase_1_t871FD4E305BA7BBB480F9360D985884672783E66 * ___m_InternalOp_0;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_InternalOp_0() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_tA3D61DC158CEEADB7D17CA260C0AF36D84E79632, ___m_InternalOp_0)); }
	inline AsyncOperationBase_1_t871FD4E305BA7BBB480F9360D985884672783E66 * get_m_InternalOp_0() const { return ___m_InternalOp_0; }
	inline AsyncOperationBase_1_t871FD4E305BA7BBB480F9360D985884672783E66 ** get_address_of_m_InternalOp_0() { return &___m_InternalOp_0; }
	inline void set_m_InternalOp_0(AsyncOperationBase_1_t871FD4E305BA7BBB480F9360D985884672783E66 * value)
	{
		___m_InternalOp_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InternalOp_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_tA3D61DC158CEEADB7D17CA260C0AF36D84E79632, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};


// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData>
struct  AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB 
{
public:
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_InternalOp
	AsyncOperationBase_1_t2C0A8C316A68BE5968D340B4620C07B16532CBE4 * ___m_InternalOp_0;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_InternalOp_0() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB, ___m_InternalOp_0)); }
	inline AsyncOperationBase_1_t2C0A8C316A68BE5968D340B4620C07B16532CBE4 * get_m_InternalOp_0() const { return ___m_InternalOp_0; }
	inline AsyncOperationBase_1_t2C0A8C316A68BE5968D340B4620C07B16532CBE4 ** get_address_of_m_InternalOp_0() { return &___m_InternalOp_0; }
	inline void set_m_InternalOp_0(AsyncOperationBase_1_t2C0A8C316A68BE5968D340B4620C07B16532CBE4 * value)
	{
		___m_InternalOp_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InternalOp_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};


// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>
struct  AsyncOperationHandle_1_t2A5159A5FE4CCB99C6BE193CB6AD0CE3168B8B89 
{
public:
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_InternalOp
	AsyncOperationBase_1_t1A58BC34AE713084A1207092089995B195724A1A * ___m_InternalOp_0;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_InternalOp_0() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t2A5159A5FE4CCB99C6BE193CB6AD0CE3168B8B89, ___m_InternalOp_0)); }
	inline AsyncOperationBase_1_t1A58BC34AE713084A1207092089995B195724A1A * get_m_InternalOp_0() const { return ___m_InternalOp_0; }
	inline AsyncOperationBase_1_t1A58BC34AE713084A1207092089995B195724A1A ** get_address_of_m_InternalOp_0() { return &___m_InternalOp_0; }
	inline void set_m_InternalOp_0(AsyncOperationBase_1_t1A58BC34AE713084A1207092089995B195724A1A * value)
	{
		___m_InternalOp_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InternalOp_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(AsyncOperationHandle_1_t2A5159A5FE4CCB99C6BE193CB6AD0CE3168B8B89, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};


// UnityEngine.ResourceManagement.Util.SerializedType
struct  SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84 
{
public:
	// System.String UnityEngine.ResourceManagement.Util.SerializedType::m_AssemblyName
	String_t* ___m_AssemblyName_0;
	// System.String UnityEngine.ResourceManagement.Util.SerializedType::m_ClassName
	String_t* ___m_ClassName_1;
	// System.Type UnityEngine.ResourceManagement.Util.SerializedType::m_CachedType
	Type_t * ___m_CachedType_2;
	// System.Boolean UnityEngine.ResourceManagement.Util.SerializedType::<ValueChanged>k__BackingField
	bool ___U3CValueChangedU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_AssemblyName_0() { return static_cast<int32_t>(offsetof(SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84, ___m_AssemblyName_0)); }
	inline String_t* get_m_AssemblyName_0() const { return ___m_AssemblyName_0; }
	inline String_t** get_address_of_m_AssemblyName_0() { return &___m_AssemblyName_0; }
	inline void set_m_AssemblyName_0(String_t* value)
	{
		___m_AssemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AssemblyName_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ClassName_1() { return static_cast<int32_t>(offsetof(SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84, ___m_ClassName_1)); }
	inline String_t* get_m_ClassName_1() const { return ___m_ClassName_1; }
	inline String_t** get_address_of_m_ClassName_1() { return &___m_ClassName_1; }
	inline void set_m_ClassName_1(String_t* value)
	{
		___m_ClassName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ClassName_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedType_2() { return static_cast<int32_t>(offsetof(SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84, ___m_CachedType_2)); }
	inline Type_t * get_m_CachedType_2() const { return ___m_CachedType_2; }
	inline Type_t ** get_address_of_m_CachedType_2() { return &___m_CachedType_2; }
	inline void set_m_CachedType_2(Type_t * value)
	{
		___m_CachedType_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedType_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CValueChangedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84, ___U3CValueChangedU3Ek__BackingField_3)); }
	inline bool get_U3CValueChangedU3Ek__BackingField_3() const { return ___U3CValueChangedU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CValueChangedU3Ek__BackingField_3() { return &___U3CValueChangedU3Ek__BackingField_3; }
	inline void set_U3CValueChangedU3Ek__BackingField_3(bool value)
	{
		___U3CValueChangedU3Ek__BackingField_3 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ResourceManagement.Util.SerializedType
struct SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84_marshaled_pinvoke
{
	char* ___m_AssemblyName_0;
	char* ___m_ClassName_1;
	Type_t * ___m_CachedType_2;
	int32_t ___U3CValueChangedU3Ek__BackingField_3;
};
// Native definition for COM marshalling of UnityEngine.ResourceManagement.Util.SerializedType
struct SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84_marshaled_com
{
	Il2CppChar* ___m_AssemblyName_0;
	Il2CppChar* ___m_ClassName_1;
	Type_t * ___m_CachedType_2;
	int32_t ___U3CValueChangedU3Ek__BackingField_3;
};

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.AddressableAssets.AddressablesImpl
struct  AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F  : public RuntimeObject
{
public:
	// UnityEngine.ResourceManagement.ResourceManager UnityEngine.AddressableAssets.AddressablesImpl::m_ResourceManager
	ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * ___m_ResourceManager_0;
	// UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider UnityEngine.AddressableAssets.AddressablesImpl::m_InstanceProvider
	RuntimeObject* ___m_InstanceProvider_1;
	// UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider UnityEngine.AddressableAssets.AddressablesImpl::SceneProvider
	RuntimeObject* ___SceneProvider_3;
	// System.Collections.Generic.List`1<UnityEngine.AddressableAssets.AddressablesImpl_ResourceLocatorInfo> UnityEngine.AddressableAssets.AddressablesImpl::m_ResourceLocators
	List_1_tCF67F7248D1F5DA68CE48741AFBEF7049EA7F73A * ___m_ResourceLocators_4;
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator> UnityEngine.AddressableAssets.AddressablesImpl::m_InitializationOperation
	AsyncOperationHandle_1_t2A5159A5FE4CCB99C6BE193CB6AD0CE3168B8B89  ___m_InitializationOperation_5;
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<System.String>> UnityEngine.AddressableAssets.AddressablesImpl::m_ActiveCheckUpdateOperation
	AsyncOperationHandle_1_tD513DCF6D023777BD9D4B31860F6CE33807DA134  ___m_ActiveCheckUpdateOperation_6;
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.IResourceLocator>> UnityEngine.AddressableAssets.AddressablesImpl::m_ActiveUpdateOperation
	AsyncOperationHandle_1_tCCD5A5B26068FD234B8318DFF5696127B1C63AF1  ___m_ActiveUpdateOperation_7;
	// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.AddressableAssets.AddressablesImpl::m_OnHandleCompleteAction
	Action_1_tFD2C45936FFEFF834ADD256A019613F56F3C812D * ___m_OnHandleCompleteAction_8;
	// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>> UnityEngine.AddressableAssets.AddressablesImpl::m_OnSceneHandleCompleteAction
	Action_1_tBF08AE776AB54CB17683A82FBF2E04F2414102FA * ___m_OnSceneHandleCompleteAction_9;
	// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.AddressableAssets.AddressablesImpl::m_OnHandleDestroyedAction
	Action_1_tFD2C45936FFEFF834ADD256A019613F56F3C812D * ___m_OnHandleDestroyedAction_10;
	// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.AddressableAssets.AddressablesImpl::m_resultToHandle
	Dictionary_2_t651A59872C12A00EC9ABCFA3DF7473DE63B9FFCD * ___m_resultToHandle_11;
	// System.Collections.Generic.HashSet`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>> UnityEngine.AddressableAssets.AddressablesImpl::m_SceneInstances
	HashSet_1_t3B7DEDC5739276883FCD0D0292F6CFA261B2B1EC * ___m_SceneInstances_12;
	// System.Boolean UnityEngine.AddressableAssets.AddressablesImpl::hasStartedInitialization
	bool ___hasStartedInitialization_13;

public:
	inline static int32_t get_offset_of_m_ResourceManager_0() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_ResourceManager_0)); }
	inline ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * get_m_ResourceManager_0() const { return ___m_ResourceManager_0; }
	inline ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 ** get_address_of_m_ResourceManager_0() { return &___m_ResourceManager_0; }
	inline void set_m_ResourceManager_0(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * value)
	{
		___m_ResourceManager_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ResourceManager_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_InstanceProvider_1() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_InstanceProvider_1)); }
	inline RuntimeObject* get_m_InstanceProvider_1() const { return ___m_InstanceProvider_1; }
	inline RuntimeObject** get_address_of_m_InstanceProvider_1() { return &___m_InstanceProvider_1; }
	inline void set_m_InstanceProvider_1(RuntimeObject* value)
	{
		___m_InstanceProvider_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InstanceProvider_1), (void*)value);
	}

	inline static int32_t get_offset_of_SceneProvider_3() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___SceneProvider_3)); }
	inline RuntimeObject* get_SceneProvider_3() const { return ___SceneProvider_3; }
	inline RuntimeObject** get_address_of_SceneProvider_3() { return &___SceneProvider_3; }
	inline void set_SceneProvider_3(RuntimeObject* value)
	{
		___SceneProvider_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SceneProvider_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_ResourceLocators_4() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_ResourceLocators_4)); }
	inline List_1_tCF67F7248D1F5DA68CE48741AFBEF7049EA7F73A * get_m_ResourceLocators_4() const { return ___m_ResourceLocators_4; }
	inline List_1_tCF67F7248D1F5DA68CE48741AFBEF7049EA7F73A ** get_address_of_m_ResourceLocators_4() { return &___m_ResourceLocators_4; }
	inline void set_m_ResourceLocators_4(List_1_tCF67F7248D1F5DA68CE48741AFBEF7049EA7F73A * value)
	{
		___m_ResourceLocators_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ResourceLocators_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_InitializationOperation_5() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_InitializationOperation_5)); }
	inline AsyncOperationHandle_1_t2A5159A5FE4CCB99C6BE193CB6AD0CE3168B8B89  get_m_InitializationOperation_5() const { return ___m_InitializationOperation_5; }
	inline AsyncOperationHandle_1_t2A5159A5FE4CCB99C6BE193CB6AD0CE3168B8B89 * get_address_of_m_InitializationOperation_5() { return &___m_InitializationOperation_5; }
	inline void set_m_InitializationOperation_5(AsyncOperationHandle_1_t2A5159A5FE4CCB99C6BE193CB6AD0CE3168B8B89  value)
	{
		___m_InitializationOperation_5 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_InitializationOperation_5))->___m_InternalOp_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_ActiveCheckUpdateOperation_6() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_ActiveCheckUpdateOperation_6)); }
	inline AsyncOperationHandle_1_tD513DCF6D023777BD9D4B31860F6CE33807DA134  get_m_ActiveCheckUpdateOperation_6() const { return ___m_ActiveCheckUpdateOperation_6; }
	inline AsyncOperationHandle_1_tD513DCF6D023777BD9D4B31860F6CE33807DA134 * get_address_of_m_ActiveCheckUpdateOperation_6() { return &___m_ActiveCheckUpdateOperation_6; }
	inline void set_m_ActiveCheckUpdateOperation_6(AsyncOperationHandle_1_tD513DCF6D023777BD9D4B31860F6CE33807DA134  value)
	{
		___m_ActiveCheckUpdateOperation_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ActiveCheckUpdateOperation_6))->___m_InternalOp_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_ActiveUpdateOperation_7() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_ActiveUpdateOperation_7)); }
	inline AsyncOperationHandle_1_tCCD5A5B26068FD234B8318DFF5696127B1C63AF1  get_m_ActiveUpdateOperation_7() const { return ___m_ActiveUpdateOperation_7; }
	inline AsyncOperationHandle_1_tCCD5A5B26068FD234B8318DFF5696127B1C63AF1 * get_address_of_m_ActiveUpdateOperation_7() { return &___m_ActiveUpdateOperation_7; }
	inline void set_m_ActiveUpdateOperation_7(AsyncOperationHandle_1_tCCD5A5B26068FD234B8318DFF5696127B1C63AF1  value)
	{
		___m_ActiveUpdateOperation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ActiveUpdateOperation_7))->___m_InternalOp_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_OnHandleCompleteAction_8() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_OnHandleCompleteAction_8)); }
	inline Action_1_tFD2C45936FFEFF834ADD256A019613F56F3C812D * get_m_OnHandleCompleteAction_8() const { return ___m_OnHandleCompleteAction_8; }
	inline Action_1_tFD2C45936FFEFF834ADD256A019613F56F3C812D ** get_address_of_m_OnHandleCompleteAction_8() { return &___m_OnHandleCompleteAction_8; }
	inline void set_m_OnHandleCompleteAction_8(Action_1_tFD2C45936FFEFF834ADD256A019613F56F3C812D * value)
	{
		___m_OnHandleCompleteAction_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnHandleCompleteAction_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnSceneHandleCompleteAction_9() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_OnSceneHandleCompleteAction_9)); }
	inline Action_1_tBF08AE776AB54CB17683A82FBF2E04F2414102FA * get_m_OnSceneHandleCompleteAction_9() const { return ___m_OnSceneHandleCompleteAction_9; }
	inline Action_1_tBF08AE776AB54CB17683A82FBF2E04F2414102FA ** get_address_of_m_OnSceneHandleCompleteAction_9() { return &___m_OnSceneHandleCompleteAction_9; }
	inline void set_m_OnSceneHandleCompleteAction_9(Action_1_tBF08AE776AB54CB17683A82FBF2E04F2414102FA * value)
	{
		___m_OnSceneHandleCompleteAction_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnSceneHandleCompleteAction_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnHandleDestroyedAction_10() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_OnHandleDestroyedAction_10)); }
	inline Action_1_tFD2C45936FFEFF834ADD256A019613F56F3C812D * get_m_OnHandleDestroyedAction_10() const { return ___m_OnHandleDestroyedAction_10; }
	inline Action_1_tFD2C45936FFEFF834ADD256A019613F56F3C812D ** get_address_of_m_OnHandleDestroyedAction_10() { return &___m_OnHandleDestroyedAction_10; }
	inline void set_m_OnHandleDestroyedAction_10(Action_1_tFD2C45936FFEFF834ADD256A019613F56F3C812D * value)
	{
		___m_OnHandleDestroyedAction_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnHandleDestroyedAction_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_resultToHandle_11() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_resultToHandle_11)); }
	inline Dictionary_2_t651A59872C12A00EC9ABCFA3DF7473DE63B9FFCD * get_m_resultToHandle_11() const { return ___m_resultToHandle_11; }
	inline Dictionary_2_t651A59872C12A00EC9ABCFA3DF7473DE63B9FFCD ** get_address_of_m_resultToHandle_11() { return &___m_resultToHandle_11; }
	inline void set_m_resultToHandle_11(Dictionary_2_t651A59872C12A00EC9ABCFA3DF7473DE63B9FFCD * value)
	{
		___m_resultToHandle_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_resultToHandle_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_SceneInstances_12() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___m_SceneInstances_12)); }
	inline HashSet_1_t3B7DEDC5739276883FCD0D0292F6CFA261B2B1EC * get_m_SceneInstances_12() const { return ___m_SceneInstances_12; }
	inline HashSet_1_t3B7DEDC5739276883FCD0D0292F6CFA261B2B1EC ** get_address_of_m_SceneInstances_12() { return &___m_SceneInstances_12; }
	inline void set_m_SceneInstances_12(HashSet_1_t3B7DEDC5739276883FCD0D0292F6CFA261B2B1EC * value)
	{
		___m_SceneInstances_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SceneInstances_12), (void*)value);
	}

	inline static int32_t get_offset_of_hasStartedInitialization_13() { return static_cast<int32_t>(offsetof(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F, ___hasStartedInitialization_13)); }
	inline bool get_hasStartedInitialization_13() const { return ___hasStartedInitialization_13; }
	inline bool* get_address_of_hasStartedInitialization_13() { return &___hasStartedInitialization_13; }
	inline void set_hasStartedInitialization_13(bool value)
	{
		___hasStartedInitialization_13 = value;
	}
};


// UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData
struct  ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48  : public RuntimeObject
{
public:
	// System.String UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_buildTarget
	String_t* ___m_buildTarget_1;
	// System.String UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_SettingsHash
	String_t* ___m_SettingsHash_2;
	// System.Collections.Generic.List`1<UnityEngine.AddressableAssets.ResourceLocators.ResourceLocationData> UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_CatalogLocations
	List_1_tA175E163ACBD5E7590190434C50069AA85893EC3 * ___m_CatalogLocations_3;
	// System.Boolean UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_ProfileEvents
	bool ___m_ProfileEvents_4;
	// System.Boolean UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_LogResourceManagerExceptions
	bool ___m_LogResourceManagerExceptions_5;
	// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData> UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_ExtraInitializationData
	List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F * ___m_ExtraInitializationData_6;
	// System.Boolean UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_DisableCatalogUpdateOnStart
	bool ___m_DisableCatalogUpdateOnStart_7;
	// System.Boolean UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_IsLocalCatalogInBundle
	bool ___m_IsLocalCatalogInBundle_8;
	// UnityEngine.ResourceManagement.Util.SerializedType UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::m_CertificateHandlerType
	SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  ___m_CertificateHandlerType_9;

public:
	inline static int32_t get_offset_of_m_buildTarget_1() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_buildTarget_1)); }
	inline String_t* get_m_buildTarget_1() const { return ___m_buildTarget_1; }
	inline String_t** get_address_of_m_buildTarget_1() { return &___m_buildTarget_1; }
	inline void set_m_buildTarget_1(String_t* value)
	{
		___m_buildTarget_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_buildTarget_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SettingsHash_2() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_SettingsHash_2)); }
	inline String_t* get_m_SettingsHash_2() const { return ___m_SettingsHash_2; }
	inline String_t** get_address_of_m_SettingsHash_2() { return &___m_SettingsHash_2; }
	inline void set_m_SettingsHash_2(String_t* value)
	{
		___m_SettingsHash_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SettingsHash_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_CatalogLocations_3() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_CatalogLocations_3)); }
	inline List_1_tA175E163ACBD5E7590190434C50069AA85893EC3 * get_m_CatalogLocations_3() const { return ___m_CatalogLocations_3; }
	inline List_1_tA175E163ACBD5E7590190434C50069AA85893EC3 ** get_address_of_m_CatalogLocations_3() { return &___m_CatalogLocations_3; }
	inline void set_m_CatalogLocations_3(List_1_tA175E163ACBD5E7590190434C50069AA85893EC3 * value)
	{
		___m_CatalogLocations_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CatalogLocations_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_ProfileEvents_4() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_ProfileEvents_4)); }
	inline bool get_m_ProfileEvents_4() const { return ___m_ProfileEvents_4; }
	inline bool* get_address_of_m_ProfileEvents_4() { return &___m_ProfileEvents_4; }
	inline void set_m_ProfileEvents_4(bool value)
	{
		___m_ProfileEvents_4 = value;
	}

	inline static int32_t get_offset_of_m_LogResourceManagerExceptions_5() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_LogResourceManagerExceptions_5)); }
	inline bool get_m_LogResourceManagerExceptions_5() const { return ___m_LogResourceManagerExceptions_5; }
	inline bool* get_address_of_m_LogResourceManagerExceptions_5() { return &___m_LogResourceManagerExceptions_5; }
	inline void set_m_LogResourceManagerExceptions_5(bool value)
	{
		___m_LogResourceManagerExceptions_5 = value;
	}

	inline static int32_t get_offset_of_m_ExtraInitializationData_6() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_ExtraInitializationData_6)); }
	inline List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F * get_m_ExtraInitializationData_6() const { return ___m_ExtraInitializationData_6; }
	inline List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F ** get_address_of_m_ExtraInitializationData_6() { return &___m_ExtraInitializationData_6; }
	inline void set_m_ExtraInitializationData_6(List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F * value)
	{
		___m_ExtraInitializationData_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ExtraInitializationData_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableCatalogUpdateOnStart_7() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_DisableCatalogUpdateOnStart_7)); }
	inline bool get_m_DisableCatalogUpdateOnStart_7() const { return ___m_DisableCatalogUpdateOnStart_7; }
	inline bool* get_address_of_m_DisableCatalogUpdateOnStart_7() { return &___m_DisableCatalogUpdateOnStart_7; }
	inline void set_m_DisableCatalogUpdateOnStart_7(bool value)
	{
		___m_DisableCatalogUpdateOnStart_7 = value;
	}

	inline static int32_t get_offset_of_m_IsLocalCatalogInBundle_8() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_IsLocalCatalogInBundle_8)); }
	inline bool get_m_IsLocalCatalogInBundle_8() const { return ___m_IsLocalCatalogInBundle_8; }
	inline bool* get_address_of_m_IsLocalCatalogInBundle_8() { return &___m_IsLocalCatalogInBundle_8; }
	inline void set_m_IsLocalCatalogInBundle_8(bool value)
	{
		___m_IsLocalCatalogInBundle_8 = value;
	}

	inline static int32_t get_offset_of_m_CertificateHandlerType_9() { return static_cast<int32_t>(offsetof(ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48, ___m_CertificateHandlerType_9)); }
	inline SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  get_m_CertificateHandlerType_9() const { return ___m_CertificateHandlerType_9; }
	inline SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84 * get_address_of_m_CertificateHandlerType_9() { return &___m_CertificateHandlerType_9; }
	inline void set_m_CertificateHandlerType_9(SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  value)
	{
		___m_CertificateHandlerType_9 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_CertificateHandlerType_9))->___m_AssemblyName_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_CertificateHandlerType_9))->___m_ClassName_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_CertificateHandlerType_9))->___m_CachedType_2), (void*)NULL);
		#endif
	}
};


// UnityEngine.AddressableAssets.Utility.SerializationUtilities_ObjectType
struct  ObjectType_t9E5ED83BD39951E53AF5C9055468CCCFB52A28F7 
{
public:
	// System.Int32 UnityEngine.AddressableAssets.Utility.SerializationUtilities_ObjectType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectType_t9E5ED83BD39951E53AF5C9055468CCCFB52A28F7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.AssetReferenceUILabelRestriction
struct  AssetReferenceUILabelRestriction_tC86FE955656AEBB6380E056746F7B8F6BFD440FC  : public AssetReferenceUIRestriction_t19426D4B2B24EABFE27348F6DC49C87CE1188372
{
public:
	// System.String[] UnityEngine.AssetReferenceUILabelRestriction::m_AllowedLabels
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_AllowedLabels_0;
	// System.String UnityEngine.AssetReferenceUILabelRestriction::m_CachedToString
	String_t* ___m_CachedToString_1;

public:
	inline static int32_t get_offset_of_m_AllowedLabels_0() { return static_cast<int32_t>(offsetof(AssetReferenceUILabelRestriction_tC86FE955656AEBB6380E056746F7B8F6BFD440FC, ___m_AllowedLabels_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_AllowedLabels_0() const { return ___m_AllowedLabels_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_AllowedLabels_0() { return &___m_AllowedLabels_0; }
	inline void set_m_AllowedLabels_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_AllowedLabels_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AllowedLabels_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedToString_1() { return static_cast<int32_t>(offsetof(AssetReferenceUILabelRestriction_tC86FE955656AEBB6380E056746F7B8F6BFD440FC, ___m_CachedToString_1)); }
	inline String_t* get_m_CachedToString_1() const { return ___m_CachedToString_1; }
	inline String_t** get_address_of_m_CachedToString_1() { return &___m_CachedToString_1; }
	inline void set_m_CachedToString_1(String_t* value)
	{
		___m_CachedToString_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedToString_1), (void*)value);
	}
};


// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus
struct  AsyncOperationStatus_t598BB2ACEBD2067A004500935C76AA6D845EE005 
{
public:
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AsyncOperationStatus_t598BB2ACEBD2067A004500935C76AA6D845EE005, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ResourceManagement.Util.ObjectInitializationData
struct  ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42 
{
public:
	// System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::m_Id
	String_t* ___m_Id_0;
	// UnityEngine.ResourceManagement.Util.SerializedType UnityEngine.ResourceManagement.Util.ObjectInitializationData::m_ObjectType
	SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  ___m_ObjectType_1;
	// System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::m_Data
	String_t* ___m_Data_2;

public:
	inline static int32_t get_offset_of_m_Id_0() { return static_cast<int32_t>(offsetof(ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42, ___m_Id_0)); }
	inline String_t* get_m_Id_0() const { return ___m_Id_0; }
	inline String_t** get_address_of_m_Id_0() { return &___m_Id_0; }
	inline void set_m_Id_0(String_t* value)
	{
		___m_Id_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Id_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ObjectType_1() { return static_cast<int32_t>(offsetof(ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42, ___m_ObjectType_1)); }
	inline SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  get_m_ObjectType_1() const { return ___m_ObjectType_1; }
	inline SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84 * get_address_of_m_ObjectType_1() { return &___m_ObjectType_1; }
	inline void set_m_ObjectType_1(SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  value)
	{
		___m_ObjectType_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ObjectType_1))->___m_AssemblyName_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ObjectType_1))->___m_ClassName_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ObjectType_1))->___m_CachedType_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Data_2() { return static_cast<int32_t>(offsetof(ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42, ___m_Data_2)); }
	inline String_t* get_m_Data_2() const { return ___m_Data_2; }
	inline String_t** get_address_of_m_Data_2() { return &___m_Data_2; }
	inline void set_m_Data_2(String_t* value)
	{
		___m_Data_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Data_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ResourceManagement.Util.ObjectInitializationData
struct ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42_marshaled_pinvoke
{
	char* ___m_Id_0;
	SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84_marshaled_pinvoke ___m_ObjectType_1;
	char* ___m_Data_2;
};
// Native definition for COM marshalling of UnityEngine.ResourceManagement.Util.ObjectInitializationData
struct ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42_marshaled_com
{
	Il2CppChar* ___m_Id_0;
	SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84_marshaled_com ___m_ObjectType_1;
	Il2CppChar* ___m_Data_2;
};

// System.Collections.Generic.List`1_Enumerator<UnityEngine.ResourceManagement.Util.ObjectInitializationData>
struct  Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C, ___list_0)); }
	inline List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F * get_list_0() const { return ___list_0; }
	inline List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C, ___current_3)); }
	inline ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  get_current_3() const { return ___current_3; }
	inline ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_Id_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___m_ObjectType_1))->___m_AssemblyName_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___m_ObjectType_1))->___m_ClassName_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___m_ObjectType_1))->___m_CachedType_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___m_Data_2), (void*)NULL);
		#endif
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Boolean>
struct  AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74  : public RuntimeObject
{
public:
	// TObject UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::<Result>k__BackingField
	bool ___U3CResultU3Ek__BackingField_0;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_referenceCount
	int32_t ___m_referenceCount_1;
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_Status
	int32_t ___m_Status_2;
	// System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_Error
	Exception_t * ___m_Error_3;
	// UnityEngine.ResourceManagement.ResourceManager UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_RM
	ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * ___m_RM_4;
	// System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_Version
	int32_t ___m_Version_5;
	// DelegateList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_CompletedAction
	DelegateList_1_tFE9D647F19045792FB2F3A6F5C0423B587C3FBEF * ___m_CompletedAction_6;
	// DelegateList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_DestroyedAction
	DelegateList_1_tFE9D647F19045792FB2F3A6F5C0423B587C3FBEF * ___m_DestroyedAction_7;
	// DelegateList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_CompletedActionT
	DelegateList_1_t934676D0C0A439F5133F93BA4D6CAEF53246857D * ___m_CompletedActionT_8;
	// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_OnDestroyAction
	Action_1_tDF402D078DE7362DD66194EC7B7A3911E268248D * ___m_OnDestroyAction_9;
	// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_dependencyCompleteAction
	Action_1_tFD2C45936FFEFF834ADD256A019613F56F3C812D * ___m_dependencyCompleteAction_10;
	// System.Threading.EventWaitHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_waitHandle
	EventWaitHandle_t7603BF1D3D30FE42DD07A450C8D09E2684DC4D98 * ___m_waitHandle_11;
	// System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_InDeferredCallbackQueue
	bool ___m_InDeferredCallbackQueue_12;
	// DelegateList`1<System.Single> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_UpdateCallbacks
	DelegateList_1_t84A2304C03E7E50A20DA212DB29667268E5D1FF9 * ___m_UpdateCallbacks_13;
	// System.Action`1<System.Single> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::m_UpdateCallback
	Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B * ___m_UpdateCallback_14;

public:
	inline static int32_t get_offset_of_U3CResultU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74, ___U3CResultU3Ek__BackingField_0)); }
	inline bool get_U3CResultU3Ek__BackingField_0() const { return ___U3CResultU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CResultU3Ek__BackingField_0() { return &___U3CResultU3Ek__BackingField_0; }
	inline void set_U3CResultU3Ek__BackingField_0(bool value)
	{
		___U3CResultU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_referenceCount_1() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74, ___m_referenceCount_1)); }
	inline int32_t get_m_referenceCount_1() const { return ___m_referenceCount_1; }
	inline int32_t* get_address_of_m_referenceCount_1() { return &___m_referenceCount_1; }
	inline void set_m_referenceCount_1(int32_t value)
	{
		___m_referenceCount_1 = value;
	}

	inline static int32_t get_offset_of_m_Status_2() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74, ___m_Status_2)); }
	inline int32_t get_m_Status_2() const { return ___m_Status_2; }
	inline int32_t* get_address_of_m_Status_2() { return &___m_Status_2; }
	inline void set_m_Status_2(int32_t value)
	{
		___m_Status_2 = value;
	}

	inline static int32_t get_offset_of_m_Error_3() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74, ___m_Error_3)); }
	inline Exception_t * get_m_Error_3() const { return ___m_Error_3; }
	inline Exception_t ** get_address_of_m_Error_3() { return &___m_Error_3; }
	inline void set_m_Error_3(Exception_t * value)
	{
		___m_Error_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Error_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_RM_4() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74, ___m_RM_4)); }
	inline ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * get_m_RM_4() const { return ___m_RM_4; }
	inline ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 ** get_address_of_m_RM_4() { return &___m_RM_4; }
	inline void set_m_RM_4(ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * value)
	{
		___m_RM_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RM_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Version_5() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74, ___m_Version_5)); }
	inline int32_t get_m_Version_5() const { return ___m_Version_5; }
	inline int32_t* get_address_of_m_Version_5() { return &___m_Version_5; }
	inline void set_m_Version_5(int32_t value)
	{
		___m_Version_5 = value;
	}

	inline static int32_t get_offset_of_m_CompletedAction_6() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74, ___m_CompletedAction_6)); }
	inline DelegateList_1_tFE9D647F19045792FB2F3A6F5C0423B587C3FBEF * get_m_CompletedAction_6() const { return ___m_CompletedAction_6; }
	inline DelegateList_1_tFE9D647F19045792FB2F3A6F5C0423B587C3FBEF ** get_address_of_m_CompletedAction_6() { return &___m_CompletedAction_6; }
	inline void set_m_CompletedAction_6(DelegateList_1_tFE9D647F19045792FB2F3A6F5C0423B587C3FBEF * value)
	{
		___m_CompletedAction_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CompletedAction_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_DestroyedAction_7() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74, ___m_DestroyedAction_7)); }
	inline DelegateList_1_tFE9D647F19045792FB2F3A6F5C0423B587C3FBEF * get_m_DestroyedAction_7() const { return ___m_DestroyedAction_7; }
	inline DelegateList_1_tFE9D647F19045792FB2F3A6F5C0423B587C3FBEF ** get_address_of_m_DestroyedAction_7() { return &___m_DestroyedAction_7; }
	inline void set_m_DestroyedAction_7(DelegateList_1_tFE9D647F19045792FB2F3A6F5C0423B587C3FBEF * value)
	{
		___m_DestroyedAction_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DestroyedAction_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_CompletedActionT_8() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74, ___m_CompletedActionT_8)); }
	inline DelegateList_1_t934676D0C0A439F5133F93BA4D6CAEF53246857D * get_m_CompletedActionT_8() const { return ___m_CompletedActionT_8; }
	inline DelegateList_1_t934676D0C0A439F5133F93BA4D6CAEF53246857D ** get_address_of_m_CompletedActionT_8() { return &___m_CompletedActionT_8; }
	inline void set_m_CompletedActionT_8(DelegateList_1_t934676D0C0A439F5133F93BA4D6CAEF53246857D * value)
	{
		___m_CompletedActionT_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CompletedActionT_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDestroyAction_9() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74, ___m_OnDestroyAction_9)); }
	inline Action_1_tDF402D078DE7362DD66194EC7B7A3911E268248D * get_m_OnDestroyAction_9() const { return ___m_OnDestroyAction_9; }
	inline Action_1_tDF402D078DE7362DD66194EC7B7A3911E268248D ** get_address_of_m_OnDestroyAction_9() { return &___m_OnDestroyAction_9; }
	inline void set_m_OnDestroyAction_9(Action_1_tDF402D078DE7362DD66194EC7B7A3911E268248D * value)
	{
		___m_OnDestroyAction_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDestroyAction_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_dependencyCompleteAction_10() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74, ___m_dependencyCompleteAction_10)); }
	inline Action_1_tFD2C45936FFEFF834ADD256A019613F56F3C812D * get_m_dependencyCompleteAction_10() const { return ___m_dependencyCompleteAction_10; }
	inline Action_1_tFD2C45936FFEFF834ADD256A019613F56F3C812D ** get_address_of_m_dependencyCompleteAction_10() { return &___m_dependencyCompleteAction_10; }
	inline void set_m_dependencyCompleteAction_10(Action_1_tFD2C45936FFEFF834ADD256A019613F56F3C812D * value)
	{
		___m_dependencyCompleteAction_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_dependencyCompleteAction_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_waitHandle_11() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74, ___m_waitHandle_11)); }
	inline EventWaitHandle_t7603BF1D3D30FE42DD07A450C8D09E2684DC4D98 * get_m_waitHandle_11() const { return ___m_waitHandle_11; }
	inline EventWaitHandle_t7603BF1D3D30FE42DD07A450C8D09E2684DC4D98 ** get_address_of_m_waitHandle_11() { return &___m_waitHandle_11; }
	inline void set_m_waitHandle_11(EventWaitHandle_t7603BF1D3D30FE42DD07A450C8D09E2684DC4D98 * value)
	{
		___m_waitHandle_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_waitHandle_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_InDeferredCallbackQueue_12() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74, ___m_InDeferredCallbackQueue_12)); }
	inline bool get_m_InDeferredCallbackQueue_12() const { return ___m_InDeferredCallbackQueue_12; }
	inline bool* get_address_of_m_InDeferredCallbackQueue_12() { return &___m_InDeferredCallbackQueue_12; }
	inline void set_m_InDeferredCallbackQueue_12(bool value)
	{
		___m_InDeferredCallbackQueue_12 = value;
	}

	inline static int32_t get_offset_of_m_UpdateCallbacks_13() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74, ___m_UpdateCallbacks_13)); }
	inline DelegateList_1_t84A2304C03E7E50A20DA212DB29667268E5D1FF9 * get_m_UpdateCallbacks_13() const { return ___m_UpdateCallbacks_13; }
	inline DelegateList_1_t84A2304C03E7E50A20DA212DB29667268E5D1FF9 ** get_address_of_m_UpdateCallbacks_13() { return &___m_UpdateCallbacks_13; }
	inline void set_m_UpdateCallbacks_13(DelegateList_1_t84A2304C03E7E50A20DA212DB29667268E5D1FF9 * value)
	{
		___m_UpdateCallbacks_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UpdateCallbacks_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_UpdateCallback_14() { return static_cast<int32_t>(offsetof(AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74, ___m_UpdateCallback_14)); }
	inline Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B * get_m_UpdateCallback_14() const { return ___m_UpdateCallback_14; }
	inline Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B ** get_address_of_m_UpdateCallback_14() { return &___m_UpdateCallback_14; }
	inline void set_m_UpdateCallback_14(Action_1_tCBF7F8C203F735692364E5850B0A960E0EABD03B * value)
	{
		___m_UpdateCallback_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UpdateCallback_14), (void*)value);
	}
};


// System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>>
struct  Action_1_t389A8EC91B4195D32771077FA7561379B6C05341  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation
struct  InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D  : public AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74
{
public:
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData> UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::m_RtdOp
	AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB  ___m_RtdOp_15;
	// UnityEngine.AddressableAssets.AddressablesImpl UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::m_Addressables
	AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * ___m_Addressables_16;
	// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::m_DepOp
	AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143  ___m_DepOp_17;

public:
	inline static int32_t get_offset_of_m_RtdOp_15() { return static_cast<int32_t>(offsetof(InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D, ___m_RtdOp_15)); }
	inline AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB  get_m_RtdOp_15() const { return ___m_RtdOp_15; }
	inline AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB * get_address_of_m_RtdOp_15() { return &___m_RtdOp_15; }
	inline void set_m_RtdOp_15(AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB  value)
	{
		___m_RtdOp_15 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_RtdOp_15))->___m_InternalOp_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_Addressables_16() { return static_cast<int32_t>(offsetof(InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D, ___m_Addressables_16)); }
	inline AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * get_m_Addressables_16() const { return ___m_Addressables_16; }
	inline AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F ** get_address_of_m_Addressables_16() { return &___m_Addressables_16; }
	inline void set_m_Addressables_16(AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * value)
	{
		___m_Addressables_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Addressables_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_DepOp_17() { return static_cast<int32_t>(offsetof(InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D, ___m_DepOp_17)); }
	inline AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143  get_m_DepOp_17() const { return ___m_DepOp_17; }
	inline AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143 * get_address_of_m_DepOp_17() { return &___m_DepOp_17; }
	inline void set_m_DepOp_17(AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143  value)
	{
		___m_DepOp_17 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_DepOp_17))->___m_InternalOp_0), (void*)NULL);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Object>::get_Result()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * AsyncOperationHandle_1_get_Result_m902819C6D5FC29C361E0889E795B08C0A45E068E_gshared (AsyncOperationHandle_1_tA3D61DC158CEEADB7D17CA260C0AF36D84E79632 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7BC0B7F8AD988AAA732E6098D9233366BF3A0F54_gshared (List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C  List_1_GetEnumerator_m142C3F136F4385E36BFBB76E7C1D83696DBF5E43_gshared (List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.ResourceManagement.Util.ObjectInitializationData>::get_Current()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  Enumerator_get_Current_mB825ABD75EF6878CF22490D684078AAB196C41EC_gshared_inline (Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE0B35AF41D06A2B921242CDEAC5EB0E6B1BD0883_gshared (List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086 * __this, AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185  ___item0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.ResourceManagement.Util.ObjectInitializationData>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m6631D8F567209AA1D5E64ED69435D093F7180817_gshared (Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.ResourceManagement.Util.ObjectInitializationData>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_m4E4C6A22F5BE48284A81551C4E485FA4A53127A7_gshared (Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C * __this, const RuntimeMethod* method);
// System.Void System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Object>>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mAF60DE24E9F0E6DE397577ED5FB68DFDA1772DFA_gshared (Action_1_t7071BA67566E765F87D6F9EA78100A9F83D14126 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Object>::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<!0>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncOperationHandle_1_add_Completed_mA09472A996D3150A1FC495DD2BD3F797382DA2A6_gshared (AsyncOperationHandle_1_tA3D61DC158CEEADB7D17CA260C0AF36D84E79632 * __this, Action_1_t7071BA67566E765F87D6F9EA78100A9F83D14126 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Boolean>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncOperationBase_1__ctor_m6CF95D0597E37D0B3A3296C8E8B25D60ECE6A182_gshared (AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74 * __this, const RuntimeMethod* method);
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Object>::get_Status()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AsyncOperationHandle_1_get_Status_mFC6275B17A8594442179B6EF1C2709ACD9411957_gshared (AsyncOperationHandle_1_tA3D61DC158CEEADB7D17CA260C0AF36D84E79632 * __this, const RuntimeMethod* method);
// System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Object>::get_DebugName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AsyncOperationHandle_1_get_DebugName_mABDF53D77A8A7B7DF7B3A0A0E6543F089A6BA261_gshared (AsyncOperationHandle_1_tA3D61DC158CEEADB7D17CA260C0AF36D84E79632 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Boolean>::Complete(!0,System.Boolean,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncOperationBase_1_Complete_mB8120801F44252CB88D19F0465F5C0380562B981_gshared (AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74 * __this, bool ___result0, bool ___success1, String_t* ___errorMsg2, const RuntimeMethod* method);
// System.Void UnityEngine.AddressableAssets.AddressablesImpl::Release<System.Object>(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddressablesImpl_Release_TisRuntimeObject_m53FA44149B72176B5FB3F92CFC8BC7B8478B262C_gshared (AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * __this, AsyncOperationHandle_1_tA3D61DC158CEEADB7D17CA260C0AF36D84E79632  ___handle0, const RuntimeMethod* method);

// System.Void UnityEngine.AssetReferenceUIRestriction::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssetReferenceUIRestriction__ctor_m22E79885F35583A38F38085048423B6B69FAD3FC (AssetReferenceUIRestriction_t19426D4B2B24EABFE27348F6DC49C87CE1188372 * __this, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_m05C12F58ADC2D807613A9301DF438CB3CD09B75A (StringBuilder_t * __this, Il2CppChar ___value0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260 (StringBuilder_t * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Attribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0 (Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74 * __this, const RuntimeMethod* method);
// !0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData>::get_Result()
inline ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48 * AsyncOperationHandle_1_get_Result_m4AD1404D8FBA522A55420CEE072E5CBF2FEB58F9 (AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB * __this, const RuntimeMethod* method)
{
	return ((  ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48 * (*) (AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB *, const RuntimeMethod*))AsyncOperationHandle_1_get_Result_m902819C6D5FC29C361E0889E795B08C0A45E068E_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>::.ctor()
inline void List_1__ctor_m7BC0B7F8AD988AAA732E6098D9233366BF3A0F54 (List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086 *, const RuntimeMethod*))List_1__ctor_m7BC0B7F8AD988AAA732E6098D9233366BF3A0F54_gshared)(__this, method);
}
// System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData> UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData::get_InitializationObjects()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F * ResourceManagerRuntimeData_get_InitializationObjects_m33078507C6F754A39577BCC08D6E6E82EF89D612 (ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.ResourceManagement.Util.ObjectInitializationData>::GetEnumerator()
inline Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C  List_1_GetEnumerator_m142C3F136F4385E36BFBB76E7C1D83696DBF5E43 (List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C  (*) (List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F *, const RuntimeMethod*))List_1_GetEnumerator_m142C3F136F4385E36BFBB76E7C1D83696DBF5E43_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.ResourceManagement.Util.ObjectInitializationData>::get_Current()
inline ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  Enumerator_get_Current_mB825ABD75EF6878CF22490D684078AAB196C41EC_inline (Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C * __this, const RuntimeMethod* method)
{
	return ((  ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  (*) (Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C *, const RuntimeMethod*))Enumerator_get_Current_mB825ABD75EF6878CF22490D684078AAB196C41EC_gshared_inline)(__this, method);
}
// UnityEngine.ResourceManagement.Util.SerializedType UnityEngine.ResourceManagement.Util.ObjectInitializationData::get_ObjectType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  ObjectInitializationData_get_ObjectType_mB1B59BD388442F732B6FFA421AE1ED0AE3AE4532 (ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42 * __this, const RuntimeMethod* method);
// System.Type UnityEngine.ResourceManagement.Util.SerializedType::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * SerializedType_get_Value_m412D3B47698A74EFA8A3B29B4D102842714BD6B0 (SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84 * __this, const RuntimeMethod* method);
// System.Boolean System.Type::op_Equality(System.Type,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Type_op_Equality_m7040622C9E1037EFC73E1F0EDB1DD241282BE3D8 (Type_t * ___left0, Type_t * ___right1, const RuntimeMethod* method);
// UnityEngine.ResourceManagement.ResourceManager UnityEngine.AddressableAssets.AddressablesImpl::get_ResourceManager()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * AddressablesImpl_get_ResourceManager_mE53CB81CFC445C16556F59FA7AFDA7166A2500AF (AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * __this, const RuntimeMethod* method);
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.Util.ObjectInitializationData::GetAsyncInitHandle(UnityEngine.ResourceManagement.ResourceManager,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185  ObjectInitializationData_GetAsyncInitHandle_m3050F492A6972087E9AA5371C7946CBE5F29AC42 (ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42 * __this, ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * ___rm0, String_t* ___idOverride1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>::Add(!0)
inline void List_1_Add_mE0B35AF41D06A2B921242CDEAC5EB0E6B1BD0883 (List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086 * __this, AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185  ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086 *, AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185 , const RuntimeMethod*))List_1_Add_mE0B35AF41D06A2B921242CDEAC5EB0E6B1BD0883_gshared)(__this, ___item0, method);
}
// System.Void UnityEngine.AddressableAssets.Addressables::LogErrorFormat(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Addressables_LogErrorFormat_mB95D820554CA6DC6C93F8B85439E15491C90DA2E (String_t* ___format0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.ResourceManagement.Util.ObjectInitializationData>::MoveNext()
inline bool Enumerator_MoveNext_m6631D8F567209AA1D5E64ED69435D093F7180817 (Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C *, const RuntimeMethod*))Enumerator_MoveNext_m6631D8F567209AA1D5E64ED69435D093F7180817_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.ResourceManagement.Util.ObjectInitializationData>::Dispose()
inline void Enumerator_Dispose_m4E4C6A22F5BE48284A81551C4E485FA4A53127A7 (Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C *, const RuntimeMethod*))Enumerator_Dispose_m4E4C6A22F5BE48284A81551C4E485FA4A53127A7_gshared)(__this, method);
}
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::CreateGenericGroupOperation(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143  ResourceManager_CreateGenericGroupOperation_m637EFAA4DDCCCB4DB5BDE75BBB4B53C61848D8F2 (ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * __this, List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086 * ___operations0, bool ___releasedCachedOpOnComplete1, const RuntimeMethod* method);
// System.Void System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m73358EC02BB8EAA16DFB0FE0227640F292A167E9 (Action_1_t389A8EC91B4195D32771077FA7561379B6C05341 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t389A8EC91B4195D32771077FA7561379B6C05341 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAF60DE24E9F0E6DE397577ED5FB68DFDA1772DFA_gshared)(__this, ___object0, ___method1, method);
}
// System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<!0>>)
inline void AsyncOperationHandle_1_add_Completed_m19650AF5F2DBD7964B1A3DE4513B920662BCFA35 (AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143 * __this, Action_1_t389A8EC91B4195D32771077FA7561379B6C05341 * ___value0, const RuntimeMethod* method)
{
	((  void (*) (AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143 *, Action_1_t389A8EC91B4195D32771077FA7561379B6C05341 *, const RuntimeMethod*))AsyncOperationHandle_1_add_Completed_mA09472A996D3150A1FC495DD2BD3F797382DA2A6_gshared)(__this, ___value0, method);
}
// System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Boolean>::.ctor()
inline void AsyncOperationBase_1__ctor_m6CF95D0597E37D0B3A3296C8E8B25D60ECE6A182 (AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74 * __this, const RuntimeMethod* method)
{
	((  void (*) (AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74 *, const RuntimeMethod*))AsyncOperationBase_1__ctor_m6CF95D0597E37D0B3A3296C8E8B25D60ECE6A182_gshared)(__this, method);
}
// UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>::get_Status()
inline int32_t AsyncOperationHandle_1_get_Status_m06913A00983A3310A70A1F3AD960835579053077 (AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143 *, const RuntimeMethod*))AsyncOperationHandle_1_get_Status_mFC6275B17A8594442179B6EF1C2709ACD9411957_gshared)(__this, method);
}
// System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>::get_DebugName()
inline String_t* AsyncOperationHandle_1_get_DebugName_mF0F80D803F94C19CB3459DD6638D06701FEDF006 (AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143 * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143 *, const RuntimeMethod*))AsyncOperationHandle_1_get_DebugName_mABDF53D77A8A7B7DF7B3A0A0E6543F089A6BA261_gshared)(__this, method);
}
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<System.Boolean>::Complete(!0,System.Boolean,System.String)
inline void AsyncOperationBase_1_Complete_mB8120801F44252CB88D19F0465F5C0380562B981 (AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74 * __this, bool ___result0, bool ___success1, String_t* ___errorMsg2, const RuntimeMethod* method)
{
	((  void (*) (AsyncOperationBase_1_t7CF15F8BF9FE9142831F7747E6C64F276A3E5F74 *, bool, bool, String_t*, const RuntimeMethod*))AsyncOperationBase_1_Complete_mB8120801F44252CB88D19F0465F5C0380562B981_gshared)(__this, ___result0, ___success1, ___errorMsg2, method);
}
// System.Void UnityEngine.AddressableAssets.AddressablesImpl::Release<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
inline void AddressablesImpl_Release_TisIList_1_t3CE2E108B6096E5AFDF51C64286BBC99DEC503C0_mA065D73B7C91D29DC3D4D497A6DC6B2C241DA354 (AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * __this, AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143  ___handle0, const RuntimeMethod* method)
{
	((  void (*) (AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F *, AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143 , const RuntimeMethod*))AddressablesImpl_Release_TisRuntimeObject_m53FA44149B72176B5FB3F92CFC8BC7B8478B262C_gshared)(__this, ___handle0, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AssetReferenceUILabelRestriction::.ctor(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssetReferenceUILabelRestriction__ctor_m4AF8ACAD8F5638454D69CC43ABC0597E381D9479 (AssetReferenceUILabelRestriction_tC86FE955656AEBB6380E056746F7B8F6BFD440FC * __this, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___allowedLabels0, const RuntimeMethod* method)
{
	{
		// public AssetReferenceUILabelRestriction(params string[] allowedLabels)
		AssetReferenceUIRestriction__ctor_m22E79885F35583A38F38085048423B6B69FAD3FC(__this, /*hidden argument*/NULL);
		// m_AllowedLabels = allowedLabels;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_0 = ___allowedLabels0;
		__this->set_m_AllowedLabels_0(L_0);
		// }
		return;
	}
}
// System.Boolean UnityEngine.AssetReferenceUILabelRestriction::ValidateAsset(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AssetReferenceUILabelRestriction_ValidateAsset_m0FC3607B4B8291BAC2F0DBDA50832C28E6862EAF (AssetReferenceUILabelRestriction_tC86FE955656AEBB6380E056746F7B8F6BFD440FC * __this, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// return true;
		V_0 = (bool)1;
		goto IL_0005;
	}

IL_0005:
	{
		// }
		bool L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.AssetReferenceUILabelRestriction::ValidateAsset(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AssetReferenceUILabelRestriction_ValidateAsset_m81585357FB02801962010AE448EBE2C6309300E4 (AssetReferenceUILabelRestriction_tC86FE955656AEBB6380E056746F7B8F6BFD440FC * __this, String_t* ___path0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// return true;
		V_0 = (bool)1;
		goto IL_0005;
	}

IL_0005:
	{
		// }
		bool L_0 = V_0;
		return L_0;
	}
}
// System.String UnityEngine.AssetReferenceUILabelRestriction::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AssetReferenceUILabelRestriction_ToString_m5DE963979BB3001AFBCC5FF6DF76423C8BC85BA6 (AssetReferenceUILabelRestriction_tC86FE955656AEBB6380E056746F7B8F6BFD440FC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetReferenceUILabelRestriction_ToString_m5DE963979BB3001AFBCC5FF6DF76423C8BC85BA6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	StringBuilder_t * V_1 = NULL;
	bool V_2 = false;
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* V_3 = NULL;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	bool V_6 = false;
	String_t* V_7 = NULL;
	{
		// if (m_CachedToString == null)
		String_t* L_0 = __this->get_m_CachedToString_1();
		V_0 = (bool)((((RuntimeObject*)(String_t*)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0064;
		}
	}
	{
		// StringBuilder sb = new StringBuilder();
		StringBuilder_t * L_2 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E(L_2, /*hidden argument*/NULL);
		V_1 = L_2;
		// bool first = true;
		V_2 = (bool)1;
		// foreach (var t in m_AllowedLabels)
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_3 = __this->get_m_AllowedLabels_0();
		V_3 = L_3;
		V_4 = 0;
		goto IL_0050;
	}

IL_0024:
	{
		// foreach (var t in m_AllowedLabels)
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_4 = V_3;
		int32_t L_5 = V_4;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		String_t* L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_5 = L_7;
		// if (!first)
		bool L_8 = V_2;
		V_6 = (bool)((((int32_t)L_8) == ((int32_t)0))? 1 : 0);
		bool L_9 = V_6;
		if (!L_9)
		{
			goto IL_003e;
		}
	}
	{
		// sb.Append(',');
		StringBuilder_t * L_10 = V_1;
		NullCheck(L_10);
		StringBuilder_Append_m05C12F58ADC2D807613A9301DF438CB3CD09B75A(L_10, ((int32_t)44), /*hidden argument*/NULL);
	}

IL_003e:
	{
		// first = false;
		V_2 = (bool)0;
		// sb.Append(t);
		StringBuilder_t * L_11 = V_1;
		String_t* L_12 = V_5;
		NullCheck(L_11);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_11, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0050:
	{
		// foreach (var t in m_AllowedLabels)
		int32_t L_14 = V_4;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_15 = V_3;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_15)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		// m_CachedToString = sb.ToString();
		StringBuilder_t * L_16 = V_1;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		__this->set_m_CachedToString_1(L_17);
	}

IL_0064:
	{
		// return m_CachedToString;
		String_t* L_18 = __this->get_m_CachedToString_1();
		V_7 = L_18;
		goto IL_006e;
	}

IL_006e:
	{
		// }
		String_t* L_19 = V_7;
		return L_19;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean UnityEngine.AssetReferenceUIRestriction::ValidateAsset(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AssetReferenceUIRestriction_ValidateAsset_mB16598DB91529807CDE4D8797E026B939A7754C1 (AssetReferenceUIRestriction_t19426D4B2B24EABFE27348F6DC49C87CE1188372 * __this, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// return true;
		V_0 = (bool)1;
		goto IL_0005;
	}

IL_0005:
	{
		// }
		bool L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.AssetReferenceUIRestriction::ValidateAsset(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AssetReferenceUIRestriction_ValidateAsset_mB29C9B428ABE11D9553EB14B7C3D5A84B09FFD0A (AssetReferenceUIRestriction_t19426D4B2B24EABFE27348F6DC49C87CE1188372 * __this, String_t* ___path0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// return true;
		V_0 = (bool)1;
		goto IL_0005;
	}

IL_0005:
	{
		// }
		bool L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.AssetReferenceUIRestriction::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssetReferenceUIRestriction__ctor_m22E79885F35583A38F38085048423B6B69FAD3FC (AssetReferenceUIRestriction_t19426D4B2B24EABFE27348F6DC49C87CE1188372 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m45CAD4B01265CC84CC5A84F62EE2DBE85DE89EC0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.AddressableAssets.Initialization.ResourceManagerRuntimeData>,UnityEngine.AddressableAssets.AddressablesImpl)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InitalizationObjectsOperation_Init_m08F5EC686FDDA4FCF0A4018C505BF435F7F7B2E7 (InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D * __this, AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB  ___rtdOp0, AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * ___addressables1, const RuntimeMethod* method)
{
	{
		// m_RtdOp = rtdOp;
		AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB  L_0 = ___rtdOp0;
		__this->set_m_RtdOp_15(L_0);
		// m_Addressables = addressables;
		AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * L_1 = ___addressables1;
		__this->set_m_Addressables_16(L_1);
		// }
		return;
	}
}
// System.Void UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::Execute()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InitalizationObjectsOperation_Execute_m57D25A512B8B7F18B7E07625179726C4142B00FF (InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InitalizationObjectsOperation_Execute_m57D25A512B8B7F18B7E07625179726C4142B00FF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48 * V_0 = NULL;
	List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086 * V_1 = NULL;
	Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C  V_2;
	memset((&V_2), 0, sizeof(V_2));
	ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  V_3;
	memset((&V_3), 0, sizeof(V_3));
	bool V_4 = false;
	SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  V_5;
	memset((&V_5), 0, sizeof(V_5));
	AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185  V_6;
	memset((&V_6), 0, sizeof(V_6));
	Exception_t * V_7 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 3);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// var rtd = m_RtdOp.Result;
		AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB * L_0 = __this->get_address_of_m_RtdOp_15();
		ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48 * L_1 = AsyncOperationHandle_1_get_Result_m4AD1404D8FBA522A55420CEE072E5CBF2FEB58F9((AsyncOperationHandle_1_t219162447BD007CA6E25F4BDCDB60B01583424FB *)L_0, /*hidden argument*/AsyncOperationHandle_1_get_Result_m4AD1404D8FBA522A55420CEE072E5CBF2FEB58F9_RuntimeMethod_var);
		V_0 = L_1;
		// List<AsyncOperationHandle> initOperations = new List<AsyncOperationHandle>();
		List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086 * L_2 = (List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086 *)il2cpp_codegen_object_new(List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086_il2cpp_TypeInfo_var);
		List_1__ctor_m7BC0B7F8AD988AAA732E6098D9233366BF3A0F54(L_2, /*hidden argument*/List_1__ctor_m7BC0B7F8AD988AAA732E6098D9233366BF3A0F54_RuntimeMethod_var);
		V_1 = L_2;
		// foreach (var i in rtd.InitializationObjects)
		ResourceManagerRuntimeData_t562EA1602D58F6D2ED65188CE5D9038B03639E48 * L_3 = V_0;
		NullCheck(L_3);
		List_1_t8B033B1F2D8761D0C65A85A2A3A037B7EC82134F * L_4 = ResourceManagerRuntimeData_get_InitializationObjects_m33078507C6F754A39577BCC08D6E6E82EF89D612(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C  L_5 = List_1_GetEnumerator_m142C3F136F4385E36BFBB76E7C1D83696DBF5E43(L_4, /*hidden argument*/List_1_GetEnumerator_m142C3F136F4385E36BFBB76E7C1D83696DBF5E43_RuntimeMethod_var);
		V_2 = L_5;
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0098;
		}

IL_0022:
		{
			// foreach (var i in rtd.InitializationObjects)
			ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  L_6 = Enumerator_get_Current_mB825ABD75EF6878CF22490D684078AAB196C41EC_inline((Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C *)(&V_2), /*hidden argument*/Enumerator_get_Current_mB825ABD75EF6878CF22490D684078AAB196C41EC_RuntimeMethod_var);
			V_3 = L_6;
			// if (i.ObjectType.Value == null)
			SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84  L_7 = ObjectInitializationData_get_ObjectType_mB1B59BD388442F732B6FFA421AE1ED0AE3AE4532((ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42 *)(&V_3), /*hidden argument*/NULL);
			V_5 = L_7;
			Type_t * L_8 = SerializedType_get_Value_m412D3B47698A74EFA8A3B29B4D102842714BD6B0((SerializedType_tB1DA04E1A1C6B139586FA06F849C50CC729B7E84 *)(&V_5), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			bool L_9 = Type_op_Equality_m7040622C9E1037EFC73E1F0EDB1DD241282BE3D8(L_8, (Type_t *)NULL, /*hidden argument*/NULL);
			V_4 = L_9;
			bool L_10 = V_4;
			if (!L_10)
			{
				goto IL_004a;
			}
		}

IL_0047:
		{
			// continue;
			goto IL_0098;
		}

IL_004a:
		{
		}

IL_004b:
		try
		{ // begin try (depth: 2)
			// var o = i.GetAsyncInitHandle(m_Addressables.ResourceManager);
			AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * L_11 = __this->get_m_Addressables_16();
			NullCheck(L_11);
			ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * L_12 = AddressablesImpl_get_ResourceManager_mE53CB81CFC445C16556F59FA7AFDA7166A2500AF(L_11, /*hidden argument*/NULL);
			AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185  L_13 = ObjectInitializationData_GetAsyncInitHandle_m3050F492A6972087E9AA5371C7946CBE5F29AC42((ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42 *)(&V_3), L_12, (String_t*)NULL, /*hidden argument*/NULL);
			V_6 = L_13;
			// initOperations.Add(o);
			List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086 * L_14 = V_1;
			AsyncOperationHandle_tAC7DCCEDF2D1435FBF227C116E1B91768F2B2185  L_15 = V_6;
			NullCheck(L_14);
			List_1_Add_mE0B35AF41D06A2B921242CDEAC5EB0E6B1BD0883(L_14, L_15, /*hidden argument*/List_1_Add_mE0B35AF41D06A2B921242CDEAC5EB0E6B1BD0883_RuntimeMethod_var);
			goto IL_0097;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
				goto CATCH_006d;
			throw e;
		}

CATCH_006d:
		{ // begin catch(System.Exception)
			// catch (Exception ex)
			V_7 = ((Exception_t *)__exception_local);
			// Addressables.LogErrorFormat("Exception thrown during initialization of object {0}: {1}", i,
			//     ex.ToString());
			ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_16 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)2);
			ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_17 = L_16;
			ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  L_18 = V_3;
			ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  L_19 = L_18;
			RuntimeObject * L_20 = Box(ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42_il2cpp_TypeInfo_var, &L_19);
			NullCheck(L_17);
			ArrayElementTypeCheck (L_17, L_20);
			(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_20);
			ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_21 = L_17;
			Exception_t * L_22 = V_7;
			NullCheck(L_22);
			String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
			NullCheck(L_21);
			ArrayElementTypeCheck (L_21, L_23);
			(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_23);
			IL2CPP_RUNTIME_CLASS_INIT(Addressables_t8AD39E84886280F198451740C05CA7E8AF2C977D_il2cpp_TypeInfo_var);
			Addressables_LogErrorFormat_mB95D820554CA6DC6C93F8B85439E15491C90DA2E(_stringLiteral3C1B63DEBF79CE695956E89613C7F2BEAFC4DEBC, L_21, /*hidden argument*/NULL);
			goto IL_0097;
		} // end catch (depth: 2)

IL_0097:
		{
		}

IL_0098:
		{
			// foreach (var i in rtd.InitializationObjects)
			bool L_24 = Enumerator_MoveNext_m6631D8F567209AA1D5E64ED69435D093F7180817((Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m6631D8F567209AA1D5E64ED69435D093F7180817_RuntimeMethod_var);
			if (L_24)
			{
				goto IL_0022;
			}
		}

IL_00a1:
		{
			IL2CPP_LEAVE(0xB2, FINALLY_00a3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00a3;
	}

FINALLY_00a3:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4E4C6A22F5BE48284A81551C4E485FA4A53127A7((Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C *)(&V_2), /*hidden argument*/Enumerator_Dispose_m4E4C6A22F5BE48284A81551C4E485FA4A53127A7_RuntimeMethod_var);
		IL2CPP_END_FINALLY(163)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(163)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0xB2, IL_00b2)
	}

IL_00b2:
	{
		// m_DepOp = m_Addressables.ResourceManager.CreateGenericGroupOperation(initOperations, true);
		AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * L_25 = __this->get_m_Addressables_16();
		NullCheck(L_25);
		ResourceManager_t44D4D3B811BA184EEF3398DEE6880D1506AC7F99 * L_26 = AddressablesImpl_get_ResourceManager_mE53CB81CFC445C16556F59FA7AFDA7166A2500AF(L_25, /*hidden argument*/NULL);
		List_1_t24170957FE03AE4F95B75AACE351DD2A8FF52086 * L_27 = V_1;
		NullCheck(L_26);
		AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143  L_28 = ResourceManager_CreateGenericGroupOperation_m637EFAA4DDCCCB4DB5BDE75BBB4B53C61848D8F2(L_26, L_27, (bool)1, /*hidden argument*/NULL);
		__this->set_m_DepOp_17(L_28);
		// m_DepOp.Completed += (obj) =>
		// {
		//     bool success = obj.Status == AsyncOperationStatus.Succeeded;
		//     Complete(true, success, success ? "" : $"{obj.DebugName} failed initialization.");
		//     m_Addressables.Release(m_DepOp);
		// };
		AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143 * L_29 = __this->get_address_of_m_DepOp_17();
		Action_1_t389A8EC91B4195D32771077FA7561379B6C05341 * L_30 = (Action_1_t389A8EC91B4195D32771077FA7561379B6C05341 *)il2cpp_codegen_object_new(Action_1_t389A8EC91B4195D32771077FA7561379B6C05341_il2cpp_TypeInfo_var);
		Action_1__ctor_m73358EC02BB8EAA16DFB0FE0227640F292A167E9(L_30, __this, (intptr_t)((intptr_t)InitalizationObjectsOperation_U3CExecuteU3Eb__4_0_mA8AD2C34C9075665F7B90B8A1A1A256B9AF2A1BE_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m73358EC02BB8EAA16DFB0FE0227640F292A167E9_RuntimeMethod_var);
		AsyncOperationHandle_1_add_Completed_m19650AF5F2DBD7964B1A3DE4513B920662BCFA35((AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143 *)L_29, L_30, /*hidden argument*/AsyncOperationHandle_1_add_Completed_m19650AF5F2DBD7964B1A3DE4513B920662BCFA35_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InitalizationObjectsOperation__ctor_m4A866392D28009599D93EA895B2F860D8D0CB902 (InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InitalizationObjectsOperation__ctor_m4A866392D28009599D93EA895B2F860D8D0CB902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncOperationBase_1__ctor_m6CF95D0597E37D0B3A3296C8E8B25D60ECE6A182(__this, /*hidden argument*/AsyncOperationBase_1__ctor_m6CF95D0597E37D0B3A3296C8E8B25D60ECE6A182_RuntimeMethod_var);
		return;
	}
}
// System.Void UnityEngine.ResourceManagement.AsyncOperations.InitalizationObjectsOperation::<Execute>b__4_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InitalizationObjectsOperation_U3CExecuteU3Eb__4_0_mA8AD2C34C9075665F7B90B8A1A1A256B9AF2A1BE (InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D * __this, AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143  ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InitalizationObjectsOperation_U3CExecuteU3Eb__4_0_mA8AD2C34C9075665F7B90B8A1A1A256B9AF2A1BE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool G_B2_0 = false;
	int32_t G_B2_1 = 0;
	InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D * G_B2_2 = NULL;
	bool G_B1_0 = false;
	int32_t G_B1_1 = 0;
	InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D * G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	bool G_B3_1 = false;
	int32_t G_B3_2 = 0;
	InitalizationObjectsOperation_t43487E6AC19A3F0890753DEF0CA6B396E8F9009D * G_B3_3 = NULL;
	{
		// bool success = obj.Status == AsyncOperationStatus.Succeeded;
		int32_t L_0 = AsyncOperationHandle_1_get_Status_m06913A00983A3310A70A1F3AD960835579053077((AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143 *)(&___obj0), /*hidden argument*/AsyncOperationHandle_1_get_Status_m06913A00983A3310A70A1F3AD960835579053077_RuntimeMethod_var);
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
		// Complete(true, success, success ? "" : $"{obj.DebugName} failed initialization.");
		bool L_1 = V_0;
		bool L_2 = V_0;
		G_B1_0 = L_1;
		G_B1_1 = 1;
		G_B1_2 = __this;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = 1;
			G_B2_2 = __this;
			goto IL_0025;
		}
	}
	{
		String_t* L_3 = AsyncOperationHandle_1_get_DebugName_mF0F80D803F94C19CB3459DD6638D06701FEDF006((AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143 *)(&___obj0), /*hidden argument*/AsyncOperationHandle_1_get_DebugName_mF0F80D803F94C19CB3459DD6638D06701FEDF006_RuntimeMethod_var);
		String_t* L_4 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_3, _stringLiteralEC42778CF59128EE5C88854642BEE80ECEC71773, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_002a;
	}

IL_0025:
	{
		G_B3_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_002a:
	{
		NullCheck(G_B3_3);
		AsyncOperationBase_1_Complete_mB8120801F44252CB88D19F0465F5C0380562B981(G_B3_3, (bool)G_B3_2, G_B3_1, G_B3_0, /*hidden argument*/AsyncOperationBase_1_Complete_mB8120801F44252CB88D19F0465F5C0380562B981_RuntimeMethod_var);
		// m_Addressables.Release(m_DepOp);
		AddressablesImpl_tDF22AC05F33BBDA9B6B35DBFD4F27E98AE4ED08F * L_5 = __this->get_m_Addressables_16();
		AsyncOperationHandle_1_tE2BCD2CAB5E4D1BFACCFD07756C2B61BC7450143  L_6 = __this->get_m_DepOp_17();
		NullCheck(L_5);
		AddressablesImpl_Release_TisIList_1_t3CE2E108B6096E5AFDF51C64286BBC99DEC503C0_mA065D73B7C91D29DC3D4D497A6DC6B2C241DA354(L_5, L_6, /*hidden argument*/AddressablesImpl_Release_TisIList_1_t3CE2E108B6096E5AFDF51C64286BBC99DEC503C0_mA065D73B7C91D29DC3D4D497A6DC6B2C241DA354_RuntimeMethod_var);
		// };
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  Enumerator_get_Current_mB825ABD75EF6878CF22490D684078AAB196C41EC_gshared_inline (Enumerator_tA3943EAD55888D4D100DDFC1EC13A17C94090E4C * __this, const RuntimeMethod* method)
{
	{
		ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42  L_0 = (ObjectInitializationData_tC00BF9C8049326F9F0DAD00A4337AD174723EE42 )__this->get_current_3();
		return L_0;
	}
}

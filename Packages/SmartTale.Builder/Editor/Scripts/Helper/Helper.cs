﻿namespace SmartTale.Framework.Builder
{
    using System.IO;
    using UnityEngine;
    using System.Reflection;

    internal static class Helper
    {

        public enum PathType { File, Directory }

        /// <summary>
        /// Creates a directory at the selected path if it doesn't exist yet
        /// </summary>
        /// <param name="a_fullPath"></param>
        /// <param name="a_pathType"></param>
        public static void CreateRecursiveDirectory(string a_fullPath, PathType a_pathType)
        {
            if (a_pathType == PathType.Directory)
                if (Directory.Exists(a_fullPath))
                {
                    return;
                }

            string[] subfolders = a_fullPath.Split('/');

            //If the final directory is a file instead (and thus having an extension), then skip the last item in the subfolders
            int maxLength = subfolders.Length;
            if (a_pathType == PathType.File) maxLength--;

            //Starting from 1 to skip the drive
            string constructingPath = subfolders[0];
            for (int i = 1; i < maxLength; i++)
            {
                if (subfolders[i] == "") continue;
                constructingPath = constructingPath + "/" + subfolders[i];
                if (!Directory.Exists(constructingPath))
                {
                    Directory.CreateDirectory(constructingPath);
                }
            }
        }


        /// <summary>
        /// Clear the console logs.
        /// </summary>
        /// <remarks> 
        /// The reason for using this is because Debug.ClearDeveloperConsole() has some restrictions that prevents it from working in some context.
        /// More info: https://answers.unity.com/questions/578393/clear-console-through-code-in-development-build.html#
        /// </remarks>
        public static void ClearLogs()
        {
            Assembly editorAssembly = System.Reflection.Assembly.GetAssembly(typeof(UnityEditor.Editor));
            System.Type logEntriesType = editorAssembly.GetType("UnityEditor.LogEntries");
            MethodInfo clearMethod = logEntriesType.GetMethod("Clear");
            clearMethod.Invoke(new object(), null);
        }

        /// <summary>
        /// Sets the "Clear on Build" toggle in the console to the given value.
        /// </summary>
        public static void SetClearOnBuild(bool a_value)
        {
            Assembly editorAssembly = System.Reflection.Assembly.GetAssembly(typeof(UnityEditor.Editor));
            System.Type logEntriesType = editorAssembly.GetType("UnityEditor.LogEntries");
            PropertyInfo consoleOptions = logEntriesType.GetProperty("consoleFlags");
            if (consoleOptions == null)
            {
                Debug.LogWarning("[SmartTales.Builder] Couldn't find the given property through reflection!");
                return;
            }
            int hideClearOnBuild = (int)consoleOptions.GetValue(logEntriesType);
            
            //The parameter works as a flag, and thus we need to work on its given bit, in this case the 11th
            if (a_value)
            {
                hideClearOnBuild |= (1 << 11);
            }
            else
            {
                hideClearOnBuild &= ~(1 << 11);
            }

            consoleOptions.SetValue(logEntriesType, hideClearOnBuild);
        }

        /// <summary>
        /// Checks wether there's a file in the given directory with the given name no matter the extension.
        /// </summary>
        /// <example>
        /// If there's a "maNamaJeff.exe" and a "maNamaJeff.nspd" file in a folder, and we provide the "maNamaJeff" argument, it will return true
        /// </example>
        /// <param name="a_directory"></param>
        /// <param name="a_fileName"></param>
        /// <returns></returns>
        public static bool DoesFileExistsNoExtension(string a_directory, string a_fileName)
        {
            string[] files = System.IO.Directory.GetFiles(a_directory, a_fileName + ".*");
            return files.Length > 0;
        }



        /// <summary>
        /// The string that stops the build process in Team City
        /// </summary>
        /// <remarks>
        /// Team City looks for this exact string to trigger a failure, and so both need to be identical for it to work.
        /// </remarks>
        private const string TC_TRIGGERFAIL = "[BUILD FAIL]";
        /// <summary>
        /// Stops the build process in Team City with a debug log.
        /// </summary>
        /// <param name="a_message">The message to display.</param>
        internal static void TriggerFail(string a_message)
        {
            Debug.Log(TC_TRIGGERFAIL + a_message);
            throw new SmartTale.Framework.Core.Runtime.Exceptions.SmartTaleException(a_message);
        }
    }

}
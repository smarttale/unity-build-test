﻿namespace SmartTale.Framework.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;
    /// <summary>
    /// Class containing the Stack Traces states of this preset.
    /// </summary>
    [System.Serializable]
    public class BuilderVStackTrace : BaseBuilderVariable
    {
        public BuilderVStackTrace(bool enabled = false)
        {
            Enabled = enabled;
        }

        public BuilderVStackTraceSingle m_error;
        public BuilderVStackTraceSingle m_warning;
        public BuilderVStackTraceSingle m_assert;
        public BuilderVStackTraceSingle m_log;
        public BuilderVStackTraceSingle m_exception;

        /// <summary>
        /// Sets the enabled stack traces values to the player settings.
        /// </summary>
        public void ApplyStackTraces()
        {
            if (Enabled)
            {
                if (m_error.Enabled) PlayerSettings.SetStackTraceLogType(LogType.Error, m_error.Value);
                if (m_warning.Enabled) PlayerSettings.SetStackTraceLogType(LogType.Warning, m_warning.Value);
                if (m_assert.Enabled) PlayerSettings.SetStackTraceLogType(LogType.Assert, m_assert.Value);
                if (m_log.Enabled) PlayerSettings.SetStackTraceLogType(LogType.Log, m_log.Value);
                if (m_exception.Enabled) PlayerSettings.SetStackTraceLogType(LogType.Exception, m_exception.Value);

            }        
        }


        /// <summary>
        /// Overrides the stack traces varues of this preset by the ones in the project settings if the given stack trace type is enabled.
        /// </summary>
        public void OverrideStackTraces()
        {
            if (Enabled)
            {
                if (m_error.Enabled) m_error.Value = PlayerSettings.GetStackTraceLogType(LogType.Error);
                if (m_warning.Enabled) m_warning.Value = PlayerSettings.GetStackTraceLogType(LogType.Warning);
                if (m_assert.Enabled) m_assert.Value = PlayerSettings.GetStackTraceLogType(LogType.Assert);
                if (m_log.Enabled) m_log.Value = PlayerSettings.GetStackTraceLogType(LogType.Log);
                if (m_exception.Enabled) m_exception.Value = PlayerSettings.GetStackTraceLogType(LogType.Exception);

            }
        }
    }

}
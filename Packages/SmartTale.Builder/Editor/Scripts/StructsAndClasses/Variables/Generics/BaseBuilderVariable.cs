﻿namespace SmartTale.Framework.Builder
{ 
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// The base class used in the variables of BuildAndPlayerSettings.
    /// </summary>
    /// <remarks>
    /// This class is used to have an additional toggle to dictate wether or not this specific variable will influence the player settings or viceversa.
    /// Having the toggled on will mean that when applying the preset, this specific variable will be applied in the settings, and when we copy the player/build settings, this variable will also get the value.
    /// Otherwise, if it is toggled off, the variable will stay idle and don't influence.
    /// Later on with a custom inspector the variable itself will be visibly disabled if it's toggled off for an easier use.
    /// </remarks>
    [System.Serializable]
    public abstract class BaseBuilderVariable 
    {
        [SerializeField] private bool m_enabled = false;
        //Description could be used as tooltip
        [SerializeField] protected string m_description;

        /// <summary>
        /// Dictates wether or not this variable will be influenced/is going to influence the player/build settings
        /// </summary>
        public bool Enabled
        {
            get => m_enabled;
            set { m_enabled = value; }
        }


        
    }
}
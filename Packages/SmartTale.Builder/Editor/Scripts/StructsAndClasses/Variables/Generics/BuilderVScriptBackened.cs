﻿namespace SmartTale.Framework.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;
    [System.Serializable]
    public class BuilderVScriptBackened : BuilderVGeneric<ScriptingImplementation>
    {

        public BuilderVScriptBackened(ScriptingImplementation a_value, string a_tooltip, bool a_enabled = false) : base(a_value, a_tooltip, a_enabled) { }
        public BuilderVScriptBackened() { }
        /// <summary>
        /// Applies this current script backend on the player settings.
        /// </summary>
        /// <remarks>
        /// To be used only on PC since the other platforms have their own fixed script back end (such as the PS4 with LI2CPP)
        /// </remarks>
        public void ApplyScriptingImplementation()
        {
            if (Enabled)
            {
                PlayerSettings.SetScriptingBackend(BuildTargetGroup.Standalone, Value);
            }
        }

        /// <summary>
        /// Overrides the current scripting backend with the one in the player settings.
        /// </summary>
        /// <remarks>
        /// To be used only on Standalone platform since the other platforms have their own fixed script back end (such as the PS4 with LI2CPP)
        /// </remarks>
        internal void OverrideScriptingImplementation()
        {
            if (Enabled)
            {
                Value = PlayerSettings.GetScriptingBackend(BuildTargetGroup.Standalone);
            }
        }
    }

}
﻿namespace SmartTale.Framework.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;


    /// <summary>
    /// Individual Stack Trace Type.
    /// </summary>
    /// <remarks>
    /// There should be 5 in total: Error, Warning, Log, Assert and Exception.
    /// </remarks>
    [System.Serializable]
    public class BuilderVStackTraceSingle : BuilderVGeneric<StackTraceLogType>
    {
        
    }

}
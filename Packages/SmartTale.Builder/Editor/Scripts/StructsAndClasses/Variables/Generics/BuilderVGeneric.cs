﻿namespace SmartTale.Framework.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Generic class of BaseBuilderVariable.
    /// </summary>
    /// <remarks>
    /// Can be used in any type that can be handled as is without any intermediary process (like the scenes) and are able to be displayed in the inspector.
    /// </remarks>
    /// <typeparam name="T"></typeparam>
    [System.Serializable]
    public class BuilderVGeneric <T> : BaseBuilderVariable
    {
        [SerializeField] private T m_value;

        public T Value
        {
            get => m_value;
            set { m_value = value; }
        }

        public BuilderVGeneric(T a_value, bool a_enabled = true)
        {
            m_value = a_value;
            this.Enabled = a_enabled;


        }

        public BuilderVGeneric(T a_value)
        {
            m_value = a_value;
        }

        public BuilderVGeneric() 
        {            
            Enabled = false;
        }

        public BuilderVGeneric(T a_value, string tooltip, bool a_enabled = true)
        {
            m_value = a_value;
            m_description = tooltip;
            this.Enabled = a_enabled;
        }

        public static implicit operator T(BuilderVGeneric<T> lmao) => lmao.Value;
    }

}
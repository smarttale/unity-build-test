﻿
namespace SmartTale.Framework.Builder
{
    [System.Serializable]
    public class BuilderVBool : BuilderVGeneric<bool>
    {
        public BuilderVBool(bool a_value = false, bool a_enabled = false) : base(a_value, a_enabled) { }
    } 
}


﻿
namespace SmartTale.Framework.Builder
{
    [System.Serializable]
    public class BuilderVTexture2D : BuilderVGeneric<UnityEngine.Texture2D>
    {
        
        public BuilderVTexture2D(bool a_enabled = false)
        {
            Enabled = a_enabled;
        }
    }
}


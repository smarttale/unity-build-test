﻿namespace SmartTale.Framework.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;
    [System.Serializable]
    public class BuilderVPS4SubTarget : BuilderVGeneric<PS4BuildSubtarget>
    {
        public BuilderVPS4SubTarget() { }
        public BuilderVPS4SubTarget(PS4BuildSubtarget a_value, string a_tooltip, bool a_enabled = false) : base(a_value, a_tooltip, a_enabled) { }
    }

}
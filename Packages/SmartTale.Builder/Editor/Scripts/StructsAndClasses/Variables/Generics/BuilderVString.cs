﻿
namespace SmartTale.Framework.Builder
{
    [System.Serializable]
    public class BuilderVString : BuilderVGeneric<string>
    {
        public BuilderVString(string a_value, bool a_enabled = false) : base(a_value, a_enabled) { }
        public BuilderVString(string a_value, string a_tooltip, bool a_enabled = false) : base(a_value, a_tooltip, a_enabled) { }
    }
}

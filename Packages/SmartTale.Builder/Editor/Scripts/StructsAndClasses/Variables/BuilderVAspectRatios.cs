﻿namespace SmartTale.Framework.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;

    /// <summary>
    /// Builder Variable class used to handle the aspect ratio settings.
    /// </summary>
    /// <remarks>
    /// Through code we can only apply one setting at a time on the player settings, and the geniuses at Unity didn't bother making the native aspect ratios of the player settings flaggable, meaning we had to make our own flaggable enum if we wanted to have multiple aspect ratios toggled.
    /// </remarks>
    [System.Serializable]
    public class BuilderVAspectRatios : BaseBuilderVariable
    {
        [SerializeField]private CustomAspectRatios m_ratios;

        public CustomAspectRatios Value
        {
            get => m_ratios;
            set { m_ratios = value; }
        }

        

        /// <summary>
        /// Does this preset have the given aspect ratio flagged or not?
        /// </summary>
        public bool HasAspectRatioFlagged(UnityEditor.AspectRatio a_aspect)
        {
            switch (a_aspect)
            {
                case UnityEditor.AspectRatio.AspectOthers:
                    return Value.HasFlag(CustomAspectRatios.Others);
                case UnityEditor.AspectRatio.Aspect4by3:
                    return Value.HasFlag(CustomAspectRatios.Aspect4by3);
                case UnityEditor.AspectRatio.Aspect5by4:
                    return Value.HasFlag(CustomAspectRatios.Aspect5by4);
                case UnityEditor.AspectRatio.Aspect16by10:
                    return Value.HasFlag(CustomAspectRatios.Aspect16by10);
                case UnityEditor.AspectRatio.Aspect16by9:
                    return Value.HasFlag(CustomAspectRatios.Aspect16by9);
            }
            Debug.LogError("[BuildsAndPlayerPreset] No proper aspect ratio was checked!");
            return false;
        }


        /// <summary>
        /// Sets the given aspect ratio at the given value to this aspec ratio preset.
        /// </summary>        
        public void SetAspectRatioFlag(UnityEditor.AspectRatio a_aspect, bool a_valueToSet)
        {

            switch (a_aspect)
            {
                case UnityEditor.AspectRatio.AspectOthers:
                    if (a_valueToSet) Value |= CustomAspectRatios.Others;
                    else Value &= ~CustomAspectRatios.Others;
                    break;
                case UnityEditor.AspectRatio.Aspect4by3:
                    if (a_valueToSet) Value |= CustomAspectRatios.Aspect4by3;
                    else Value &= ~CustomAspectRatios.Aspect4by3;
                    break;
                case UnityEditor.AspectRatio.Aspect5by4:
                    if (a_valueToSet) Value |= CustomAspectRatios.Aspect5by4;
                    else Value &= ~CustomAspectRatios.Aspect5by4;
                    break;
                case UnityEditor.AspectRatio.Aspect16by10:
                    if (a_valueToSet) Value |= CustomAspectRatios.Aspect16by10;
                    else Value &= ~CustomAspectRatios.Aspect16by10;
                    break;
                case UnityEditor.AspectRatio.Aspect16by9:
                    if (a_valueToSet) Value |= CustomAspectRatios.Aspect16by9;
                    else Value &= ~CustomAspectRatios.Aspect16by9;
                    break;
                default:
                    Debug.LogError("[BuildsAndPlayerPreset] No proper aspect ratio was checked!");
                    break;
            }
        }
    }

}
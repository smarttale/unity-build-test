﻿namespace SmartTale.Framework.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    /// <summary>
    /// Variable used to state the SDK used when building in PS4 platform.
    /// </summary>
    /// <remarks>
    /// WARNING! Do not use the Value enum as is, preferably use the ValuePath.
    /// </remarks>
    [System.Serializable]
    internal class BuilderVPS4SDKOverride : BuilderVGeneric<string>
    {
        public BuilderVPS4SDKOverride(string sdk)
        {
            Value = sdk;
        }

        public string ValuePath
        {
            get
            {
                if(Value == "")
                {
                    Helper.TriggerFail("Chose to override SDK but none was given!");
                }
                else if (Value.Length != 5)
                {
                    Helper.TriggerFail("SDKs name length doesn't correspond to the X.XXX format! Value: " + Value);
                }
                //Instead of the typical /, Unity asks for paths with \ in it
                return System.Environment.GetEnvironmentVariable("SCE_ROOT_DIR") + "\\ORBIS SDKs\\" + Value;
            }
            set
            {
                string[] subs = value.Split('\\');
                string chosenSDK = subs[subs.Length - 1];
                if (chosenSDK.Length != 5)
                {                
                    Helper.TriggerFail("The SDK retrieved from the Player Settings couldn't be handled! \n" +
                        $"Full path: {value}, SDK chosen:{chosenSDK}");
                }
                else
                {
                    Value = chosenSDK;
                }
            }
        }
    }

}
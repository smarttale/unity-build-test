﻿namespace SmartTale.Framework.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;
   /// <summary>
   /// A class that manages the storage of the scenes in the BuildAndPlayerSettings struct.
   /// </summary>
   /// remarks>
   /// The need for a separate class is because there are two different fields of scene assets in unity:
   ///  -One found in the project as we know them
   ///  -A second one used by the build settings that contains the path and wether or not they're included in the build
   ///  
   /// This class manages both of them and can convert from one another depending on the needs of the builder.
   /// </remarks>
    [System.Serializable]
    public class BuilderVScenes : BaseBuilderVariable
    {

        [SerializeField] public List<SceneAsset> m_value;

        public List<SceneAsset> Value
        {
            get => m_value;
            set { m_value = value; }
        }

        public BuilderVScenes(bool a_enabled = true)
        {
            this.Enabled = a_enabled;           
        }

        /// <summary>
        /// Gets the scenes of this settings in an EditorBuildSettingsScene array format.
        /// </summary>        
        public UnityEditor.EditorBuildSettingsScene[] GetPresetScenes()
        {
            List<EditorBuildSettingsScene> buildScenes = new List<EditorBuildSettingsScene>();
            for (int i = 0; i < Value.Count; i++)
            {
                string path = AssetDatabase.GetAssetPath(Value[i]);
                if (path == "")
                {
                    throw new SmartTale.Framework.Core.Runtime.Exceptions.SmartTaleException($"One of the scene slots was empty!");                    
                }
                else buildScenes.Add(new EditorBuildSettingsScene(path, true));
            }
            
            return buildScenes.ToArray();
        }

        /// <summary>
        /// Gets the scenes of this preset and returns and orderly array of its local paths (e.g Assets/Scenes/SimpleScene.scene)
        /// </summary>       
        /// <remarks>
        /// This is the format that the build setting options need in the BuildPlayer script
        /// </remarks>
        public string[] GetPresetScenesPaths()
        {
            List<string> paths = new List<string>();
            if (Enabled)
            {
                for (int i = 0; i < Value.Count; i++)
                {
                    string path = AssetDatabase.GetAssetPath(Value[i]);
                    if (path == "") throw new SmartTale.Framework.Core.Runtime.Exceptions.SmartTaleException($"Unable to get the scene path at index{i} on this build preset!");
                    else paths.Add(path);
                }
            }
            else
            {
                if (EditorBuildSettings.scenes.Length == 0)
                {
                    throw new SmartTale.Framework.Core.Runtime.Exceptions.SmartTaleException($"There were no scenes added to the unity build settings!!");
                }
                for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
                {
                    paths.Add(EditorBuildSettings.scenes[i].path);
                }
            }

            return paths.ToArray();
        }

        /// <summary>
        /// Sets the current scenes in the Build Settings to this preset.
        /// </summary>        
        public void SetCurrentToPresetScenes()
        {
            EditorBuildSettingsScene[] currentScenes = EditorBuildSettings.scenes;
            Value = new List<SceneAsset>();
            foreach (EditorBuildSettingsScene scene in currentScenes)
            {
                if (scene.enabled)
                {
                    SceneAsset sceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(scene.path);
                    if (sceneAsset == null) throw new SmartTale.Framework.Core.Runtime.Exceptions.SmartTaleException($"Unable to get one of the scenes on the build settings!");
                    else Value.Add(sceneAsset);
                }
            }
        }
    }

}
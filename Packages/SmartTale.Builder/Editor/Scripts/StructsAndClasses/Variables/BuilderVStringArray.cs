﻿namespace SmartTale.Framework.Builder
{
    using System.Collections.Generic;
    using UnityEngine;

    [System.Serializable]
    public class BuilderVStringArray : BaseBuilderVariable
    {
        [SerializeField]internal List<BuilderVString> m_value = new List<BuilderVString>();
    }

}
﻿namespace SmartTale.Framework.Builder
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;
    using System;


    /// <summary>
    /// The struct containing the actual preset of player and build settings.
    /// </summary>
    /// <remarks>
    /// The reason it is separated from the scriptable object is because it is easier to compare the presets in the JSON, and to separate the actual job of the scriptable object (buttons, asset, json, etc.) 
    /// and the struct (just storing the info). This way the struct can also handle itself the convertion between a serialized format and the format needed by the PlayerSettings or other (e.g. build settings scenes, or aspect ratios)
    /// It also needs to be public, or else Unity won't show it on the inspector.
    /// </remarks>
    [System.Serializable]
    internal class BuildAndPlayerSettings 
    {
        public enum SDK
        {
            SevenDotFive,
            EightDotZero
        }

        [Header("Player Settings")]
        [SerializeField] internal BuilderVString m_companyName = new BuilderVString("Company Name", "The name of the company", false);
        [SerializeField] internal BuilderVString m_productName = new BuilderVString("Product Name");
        [SerializeField] internal BuilderVTexture2D m_gameIcon = new BuilderVTexture2D();
        [SerializeField] internal BuilderVAspectRatios m_aspectRatios = new BuilderVAspectRatios();
        [SerializeField] internal BuilderVStringArray m_defines = new BuilderVStringArray();
        [SerializeField] internal BuilderVStackTrace m_stackTraces = new BuilderVStackTrace();
        [Header("Build Settings")]
        [SerializeField] internal BuilderVScenes m_scenes = new BuilderVScenes();
        [SerializeField] internal BuilderVPS4SubTarget m_ps4BuildSubtarget = new BuilderVPS4SubTarget();
        [SerializeField] internal BuilderVPS4SDKOverride m_ps4SDKOverride = new BuilderVPS4SDKOverride("8.000");
        [SerializeField] internal BuilderVBool m_developmentMode = new BuilderVBool(false, true);
        [SerializeField] internal BuilderVBool m_autoConnectProfiler = new BuilderVBool(false,false);
        [SerializeField] internal BuilderVBool m_scriptDebugging = new BuilderVBool(false, false);     
        [SerializeField] internal BuilderVBool m_waitForManagedDebugger = new BuilderVBool(false, false);
        [SerializeField] internal BuilderVBool m_scriptsOnly = new BuilderVBool(false, false);
        //[SerializeField] internal BuilderVString m_directoryTest = new BuilderVString("Directory test");
        [SerializeField] internal BuilderVScriptBackened m_scriptBackened = new BuilderVScriptBackened();
    } 
}

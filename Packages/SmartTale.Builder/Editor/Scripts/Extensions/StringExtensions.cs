﻿namespace SmartTale.Framework.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;


    internal static class StringExtensions
    {
        private static string applicationDataPath = "";
        private static string _ApplicationDataPath
        {
            get
            {
                if (applicationDataPath == "") applicationDataPath = Application.dataPath;
                return applicationDataPath;
            }
        }

        /// <summary>
        /// Converts the given path to a relative project path.
        /// </summary>
        /// <remarks>
        /// Supports paths outside of the project, but doesn't support paths from another drive (C:, D:, etc.)
        /// </remarks>
        /// <example>
        /// C:/Toto/Momo/Floflo/MyProject-> full path directory
        /// C:/Toto/Momo/Floflo/MyProject/Assets/Images/Icon.png ->file A
        /// C:/Toto/Papa/Koko/AnotherIcon.png ->fileB
        /// File A would return "Assets/Images/Icon.png"
        /// File B would return "../../../Papa/Koko/AnotherIcon.png"
        /// </example>
        /// <param name="a_fullPath"></param>
        /// <returns></returns>
        public static string ConvertFullPathToProjectPath(this string a_fullPath)
        {
            if (a_fullPath.Length == 0)
            {
                Debug.LogWarning("Given path was empty!");
                return "";
            }
            string projectPath = _ApplicationDataPath.Replace("Assets", "");
            if (a_fullPath.Contains(projectPath)) return a_fullPath.Replace(projectPath, "");
            //The file isn't included inside the project folder and therefore needs more relative pathing "../"
            //First find the folder from which they're separated

            else
            {
                Debug.Log($"Full path: {a_fullPath}, project path: {projectPath}");

                string[] argumentSubfolders = a_fullPath.Split('/');
                string[] projectSubfolders = projectPath.Split('/');
                int maxFolderCount = Mathf.Max(argumentSubfolders.Length, projectSubfolders.Length);
                string toReturn = "";

                for (int i = 0; i < maxFolderCount; i++)
                {
                    //It means the previous folder was common but not this one
                    if (argumentSubfolders[i] != projectSubfolders[i])
                    {
                        //Assuming it's in the same drive
                        int amountOfDots = (projectSubfolders.Length - i);
                        for (int j = 0; j < amountOfDots; j++)
                        {
                            toReturn += "../";
                        }
                        for (int l = i; l < argumentSubfolders.Length; l++)
                        {
                            string addOn = (l == argumentSubfolders.Length - 1) ? "" : "/";
                            toReturn += $"{argumentSubfolders[l]}{addOn}";
                        }
                        if (toReturn[0] == '/')
                        {
                            toReturn = toReturn.Remove(0, 1);
                        }
                        return toReturn;
                    }
                }
                return toReturn;
            }
        }

        /// <summary>
        /// Shortcuts the path to the one starting from the given subfolder.
        /// Note that 
        /// </summary>
        /// <example>
        /// If C:/Toto/Lulu/Pepe/Image.png is given with a subfolder of Lulu, then Pepe/Image.png is returned
        /// </example>
        /// <remarks>
        /// If the subfolder is not found, then an emtpy string is returned.
        /// </remarks>
        /// <param name="a_currentPath"></param>
        /// <param name="a_subfolder"></param>
        /// <returns></returns>
        public static string GetPathAfterSubfolder(this string a_currentPath, string a_subfolder)
        {
            string[] subfolders = a_currentPath.Split('/');
            string localPath = "";
            for (int i = 0; i < subfolders.Length; i++)
            {
                if (subfolders[i] == a_subfolder)
                {
                    localPath += "Assets/";
                    for (int j = i + 1; j < subfolders.Length; j++)
                    {
                        localPath += subfolders[j];
                        if (j != subfolders.Length - 1) localPath += "/";
                    }
                    return localPath;
                }
            }
            return localPath;
        }

        /// <summary>
        /// Converts a local path starting from the project folder or assets folder to a full path (starting from the drive).
        /// </summary>
        /// <remarks>
        /// It simply appends the Application DataPath to find the full project path.
        /// Unity uses relative paths starting from the project folder, for example "Assets/MyIcon.png", but Windows use full paths like "C:/Toto/Floflo/BigBangProject/Assets/MyIcon.png"
        /// </remarks>
        /// <param name="a_projectOrAssetsPath">The project relative path, be either directory or asset.</param>
        /// <returns></returns>
        public static string ConvertToFullPath(this string a_projectOrAssetsPath)
        {
            string currentProjectPath = _ApplicationDataPath.Replace("Assets", "");
            return currentProjectPath + a_projectOrAssetsPath;
        }

        /// <summary>
        /// Removes the final slash of a directory path, if there is one, and returns the resulting path.
        /// </summary>
        /// <param name="a_path"></param>
        /// <returns></returns>
        public static string RemoveFinalFolderSlash(this string a_path)
        {
            if (a_path[a_path.Length - 1] == '/') return a_path.Substring(0, a_path.Length - 1);
            else return a_path;
        }

        /// <summary>
        /// Adds a slash at the end of a path if there is none, and returns it.
        /// </summary>    
        public static string AddSlash(this string a_path)
        {
            if (a_path.Length > 0 && a_path[a_path.Length - 1] != '/') a_path += "/";
            return a_path;
        }

        /// <summary>
        /// Returns wether a path is an absolute/full path (meaning it starts with a drive) or if it's a relative file (meaning it's a path relative to a subfolder).
        /// </summary>
        /// <param name="a_path"></param>
        /// <returns></returns>
        public static bool IsAbsolutePath(this string a_path)
        {
            if (a_path.Length == 0)
            {
                Debug.LogWarning("Path argument was emtpy!");
                return false;
            }
            return a_path[1] == ':';
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a_path"></param>
        /// <returns></returns>
        public static string GetDirectory(this string a_path)
        {
            string[] subfolders = a_path.Split('/');
            string localPath = "";
            for (int i = 0; i < subfolders.Length - 1; i++)
            {
                localPath += $"{subfolders[i]}/";
            }
            return localPath;
        }

        
    }

}
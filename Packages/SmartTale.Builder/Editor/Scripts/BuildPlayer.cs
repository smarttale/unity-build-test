﻿namespace SmartTale.Framework.Builder
{
    using UnityEditor;
    using UnityEngine;
    using UnityEditor.Build.Reporting;
    using System.IO;
    using System;

    /// <summary>
    /// The script used by TeamCity or other command line based CI.
    /// </summary>
    /// <remarks>
    /// Either it is called by the build preset (BuildSettingsSO) and into BuildGeneral with the profile as parameter, either plain BuildGeneral without parameters by TeamCity. The later figures out what preset to use.
    /// </remarks>
    public static class BuildPlayer
    {
        /// <summary>
        /// The build settings being worked on thorough the build preparation.
        /// </summary>
        private static BuildPlayerOptions m_buildPlayerOptions = new BuildPlayerOptions();

        /// <summary>
        /// Method called via TeamCity. It automatically searches the proper preset with the given path, and stops if it doesn't.
        /// </summary>
        public static void BuildGeneral()
        {
            // We need as parameter the path to the scriptable object
            // WARNING: It is a build parameter in team city. Modifying there or here needs to have applied the same change on the other side.
            BuildSettingsSO preset = BuildSettingsSO.SearchAndGetPreset(Environment.GetEnvironmentVariable("PresetName"));
            if (preset != null)
            {
                preset.m_pathBuild = ""; //So that it saves in default directory
                BuildGeneral(preset);
            }
            else
            {
                Helper.TriggerFail("Couldn't find the selected preset! Make sure your preset exists and is on the cloud!");
            }
        }

        /// <summary>
        /// Method called via the "Build" button manually through the scriptable object itself.
        /// </summary>
        /// <param name="a_settings">The settings to apply to this build.</param>
        public static void BuildGeneral(BuildSettingsSO a_settings)
        {
            Helper.ClearLogs();
            m_buildPlayerOptions = a_settings.GetBuildOptions();
            AssetDatabase.SaveAssets();
            Helper.SetClearOnBuild(false);
            GenericBuild(a_settings);
            Helper.SetClearOnBuild(true);
            //After building, the method will come back here, where we can reset the editor player settings back to what they were, if it's toggled on the preset
            a_settings.RestorePlayerSettings();
        }

        /// <summary>
        /// Executes the common build methods and the build itself after each platform has set its own preferences.
        /// </summary>
        private static void GenericBuild(BuildSettingsSO a_settings)
        {
            //Deleting the directory first, because ps4 has issues building in a directory where ps4 files are alredy in, contrary to standalone windows builds (where they just override existing files)
            CleanDirectory(a_settings);
            //Finally builds, with everything else prepped
            BuildReport report = BuildPipeline.BuildPlayer(m_buildPlayerOptions);
            BuildSummary summary = report.summary;
            if (summary.result == BuildResult.Succeeded)
            {
                Debug.Log($"Build succeeded: {summary.totalSize } bytes, at {m_buildPlayerOptions.locationPathName.ConvertToFullPath()}");
            }
            if (summary.result == BuildResult.Failed)
            {
                Helper.TriggerFail("The build couldn't be completed! Unkown reasons.");
            }


        }

        /// <summary>
        /// Local method to delete the build directory. 
        /// </summary>
        /// <remarks>
        /// Sometimes some platforms don't like to mess with already built files (I had some issues with the ps4 files previously).
        /// </remarks>
        /// <param name="a_buildFolder">The project relative path directory.</param>
        
        private static void CleanDirectory(BuildSettingsSO a_settings)
        {
            if (File.Exists(m_buildPlayerOptions.locationPathName)
                            && !a_settings.m_canUpdatePreviousBuild)
            {
                Debug.Log("Directory exists, deleting it.");

                string directory = m_buildPlayerOptions.locationPathName.GetDirectory();
                Debug.Log($"From file {m_buildPlayerOptions.locationPathName} we got directory {directory}");
                Directory.Delete(directory, true);
                //try
                //{
                //}
                //catch
                //{

                //    Helper.TriggerFail("Well duh can't delete this, prob something using this folder or what");
                //}
            }
            else
            {
                Debug.Log($"Decided to not clean the directory!");
            }
        }
    } 


}

﻿namespace SmartTale.Framework.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;
    using System.IO;
#if USE_ADDRESSABLES
    using UnityEditor.AddressableAssets;
    using UnityEditor.AddressableAssets.Settings;
#endif

    public partial class BuildSettingsSO //Asset manager
    {
        #region FIELDS
        #region Internal
        /// <summary>
        /// Relative path to the current preset file.
        /// </summary>
        private string m_fileSoRelativePath = "";
        /// <summary>
        /// Absolute path to the current json file.
        /// </summary>
        private string m_fileJsonAbsolutePath = "";
        /// <summary>
        /// Relative path to the current directory.
        /// </summary>
        private string m_fileDirectoryRelativePath = "";
        /// <summary>
        /// Absolute path to the current directory.
        /// </summary>
        private string m_fileDirectoryAbsolutePath = "";
        /// <summary>
        /// Backup name of the current preset, without extension.
        /// </summary>
        private string m_filePreviousName = "";

        /// <summary>
        /// The build target group saved before building (so we can switch platform before building and setting them back after building.
        /// </summary>
        //private BuildTargetGroup m_previousBuildTargetGroup;
        /// <summary>
        /// The build target saved before building (so we can switch platform before building and setting them back after building)
        /// </summary>
        private BuildTarget m_previousBuildTarget;

        /// <summary>
        /// The current group target of this preset, deduced from the target.
        /// </summary>
        public BuildTargetGroup groupTarget => GetGroupTarget(m_buildTarget);
        private BuildAndPlayerSettings m_playerSettingsBackup;  
        #endregion
        #endregion

        /// <summary>
        /// Updates all the necessary paths, starting from this very scriptable objects. The other paths are calculated based on it.
        /// </summary>
        private void UpdatePaths()
        {
            m_fileSoRelativePath = AssetDatabase.GetAssetPath(this);
            if (m_fileSoRelativePath == "") return;
            m_fileJsonAbsolutePath = m_fileSoRelativePath.ConvertToFullPath().Replace(".asset", ".json");
            m_fileDirectoryRelativePath = m_fileSoRelativePath.Replace($"{name}.asset", "");
            m_fileDirectoryAbsolutePath = m_fileDirectoryRelativePath.ConvertToFullPath();
            m_filePreviousName = name;
        }

        /// <summary>
        /// Builds the assets bundle or the addressables accordingly to the settings.
        /// TODO: Have public parameters to configure what is built and how. For now there's just a boolean
        /// </summary>
        public void BuildAssetsBundle()
        {
#if USE_ADDRESSABLES
            if (m_buildAddressablesOrAssetBundles)
            {
                Debug.Log("[BuildPreset] Starting building Asset Bundles/Addressables!");
                //Assets bundles
                //BuildAssetBundleOptions options = BuildAssetBundleOptions.None;
                //options |= BuildAssetBundleOptions.ChunkBasedCompression;
                //options |= BuildAssetBundleOptions.ForceRebuildAssetBundle;
                //BuildPipeline.BuildAssetBundles("AssetsBundleFolder", options, BuildTarget.StandaloneWindows);
                //KilianHelper.CreateRecursiveDirectory("AssetsBundleFolder".ConvertToFullPath(), KilianHelper.PathType.Directory);

                //Directory
                EditorUserBuildSettings.SwitchActiveBuildTarget(GetGroupTarget(m_buildTarget), m_buildTarget);
                Debug.Log("Will build addressables!");
                AddressableAssetSettings.BuildPlayerContent();
                Debug.Log("[BuildPreset] Finished building Asset Bundles/Addressables!");

            }
#endif
        }

        /// <summary>
        /// Creates a new preset and sets it with the given group base settings
        /// </summary>
        /// <param name="group">The group type of the preset to look for (PS4/Standalone/XBOX)</param>
        /// <param name="fileName">The name of the preset.</param>
        /// <param name="original"></param>
        [MenuItem("Assets/Create/SmartTale/Framework/Build Preset")]
        private static void CreateNewPreset()
        {
            string relativeDirectory = AssetDatabase.GetAssetPath(Selection.activeObject) + "/";
            string name = "Preset Build";
            string extension = ".asset";
            string fullDirectory = relativeDirectory.ConvertToFullPath();
            string fullFilePathWithoutExtension = fullDirectory + name;
            string fullFilePath = fullDirectory + name + extension;

            Helper.CreateRecursiveDirectory(fullDirectory, Helper.PathType.Directory);


            /*
             * Then makes sure to not override an existing file placing an increasing number to the file name (get unique path method from AssetDatabase didn't seem to work)
             * TODO: After testing I realized it only adds a 0 in the end lol
             */
            int counter = 0;
            ///string fullFilePath = fullDirectory + "/" + fileName;

            bool foundUniqueFileName = false;

            if (File.Exists(fullFilePath))
            {
                while (foundUniqueFileName == false)
                {
                    string newFilePath = fullFilePathWithoutExtension + counter + extension;
                    if (File.Exists(newFilePath))
                        counter++;

                    else
                    {
                        fullFilePath = newFilePath;
                        foundUniqueFileName = true;
                    }

                }
            }
            

            //Creates the preset, sets its value to current Player Settings values and creates its JSON file, then focus on it
            BuildSettingsSO newSettings = ScriptableObject.CreateInstance<BuildSettingsSO>();
            newSettings.isBeingCreated = true;
            EditorUtility.SetDirty(newSettings);
            newSettings.OverrideWithPlayerSettings();
            AssetDatabase.CreateAsset(newSettings, fullFilePath.ConvertFullPathToProjectPath());
            AssetDatabase.SaveAssets();
            Selection.activeObject = newSettings;
            EditorUtility.FocusProjectWindow();
            newSettings.UpdatePaths();
            if(m_useJsonAndSerialization) 
                newSettings.CreateJSONFile();
            newSettings.isBeingCreated = false;
        }


        #region APPLY OR GET SETTINGS
        /// <summary>
        /// Overrides the current preset settings with those already applied in the Player Settings or Build Settings.
        /// </summary>
        private void OverrideWithPlayerSettings()
        {
            isCopyingFromPlayer = true;
            m_settings = new BuildAndPlayerSettings();
            m_settings = OverrideCurrentSettings(m_settings, groupTarget);

            if(m_useJsonAndSerialization) SaveToJSON();
            isCopyingFromPlayer = false;
        }

        /// <summary>
        /// Applies the current preset settings to the player settings.
        /// </summary>
        /// <remarks>
        /// Method called by the custom editor.
        /// </remarks>
        public void ApplyPlayerSettings()
        {
            m_playerSettingsBackup = new BuildAndPlayerSettings();
            m_playerSettingsBackup = OverrideCurrentSettings(m_playerSettingsBackup, groupTarget);
            
            //EditorUserBuildSettings.selectedStandaloneTarget = m_buildTarget;
            //EditorUserBuildSettings.selectedBuildTargetGroup = m_buildTargetGroup;
            
            
            m_previousBuildTarget = m_buildTarget;
            
            ApplyPlayerSettings(m_settings, groupTarget);
        }

        /// <summary>
        /// Called by the UI button to override the current enabled settings.
        /// </summary>
        internal void OverrideCurrentPresetSettings()
        {
            m_settings = OverrideCurrentSettings(m_settings, groupTarget);
        }


        /// <summary>
        /// Gets the current settings applied in the Player Settings and the Build Settings
        /// and overrides every value IF they are enabled.
        /// </summary>
        /// <returns></returns>

        private static BuildAndPlayerSettings OverrideCurrentSettings(BuildAndPlayerSettings a_settings, BuildTargetGroup group)

        {
            #region PLAYER SETTINGS

            #region Main Settings
            if (a_settings.m_productName.Enabled)
                a_settings.m_productName.Value = PlayerSettings.productName;
            if (a_settings.m_companyName.Enabled)
                a_settings.m_companyName.Value = PlayerSettings.companyName;

            

            if (a_settings.m_gameIcon.Enabled)
            {
                Texture2D[] icons = PlayerSettings.GetIconsForTargetGroup(BuildTargetGroup.Unknown);
                if (icons.Length > 0 && icons[0] != null) a_settings.m_gameIcon.Value = icons[0];
            }
            #endregion
            #region Resolution

            if (a_settings.m_aspectRatios.Enabled)
            {
                a_settings.m_aspectRatios.SetAspectRatioFlag(AspectRatio.AspectOthers, PlayerSettings.HasAspectRatio(AspectRatio.AspectOthers));
                a_settings.m_aspectRatios.SetAspectRatioFlag(AspectRatio.Aspect5by4, PlayerSettings.HasAspectRatio(AspectRatio.Aspect5by4));
                a_settings.m_aspectRatios.SetAspectRatioFlag(AspectRatio.Aspect4by3, PlayerSettings.HasAspectRatio(AspectRatio.Aspect4by3));
                a_settings.m_aspectRatios.SetAspectRatioFlag(AspectRatio.Aspect16by9, PlayerSettings.HasAspectRatio(AspectRatio.Aspect16by9));
                a_settings.m_aspectRatios.SetAspectRatioFlag(AspectRatio.Aspect16by10, PlayerSettings.HasAspectRatio(AspectRatio.Aspect16by10));
            }
            #endregion
            #endregion

            #region BUILD SETTINGS
            if (a_settings.m_ps4BuildSubtarget.Enabled) a_settings.m_ps4BuildSubtarget.Value = EditorUserBuildSettings.ps4BuildSubtarget;

            if (a_settings.m_developmentMode.Enabled) a_settings.m_developmentMode.Value = EditorUserBuildSettings.development;
            if (a_settings.m_scenes.Enabled)
                a_settings.m_scenes.SetCurrentToPresetScenes();

            if (a_settings.m_scriptDebugging.Enabled)
            {
                a_settings.m_scriptDebugging.Value = EditorUserBuildSettings.allowDebugging;
                if (a_settings.m_waitForManagedDebugger.Enabled)
                    a_settings.m_waitForManagedDebugger.Value = EditorUserBuildSettings.waitForManagedDebugger;
            }


            if (a_settings.m_autoConnectProfiler.Enabled)
                a_settings.m_autoConnectProfiler.Value = EditorUserBuildSettings.connectProfiler;


            if (a_settings.m_scriptsOnly.Enabled) a_settings.m_scriptsOnly.Value = EditorUserBuildSettings.buildScriptsOnly;
            #endregion

            #region OTHER SETTINGS
            if (a_settings.m_defines.Enabled)
            {
                string defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(group);
                string[] separatedDefines = defines.Split(';');
                a_settings.m_defines.m_value = new List<BuilderVString>();
                for (int i = 0; i < separatedDefines.Length; i++)
                {
                    a_settings.m_defines.m_value.Add(new BuilderVString(separatedDefines[i], true));
                }
            }

            a_settings.m_stackTraces.OverrideStackTraces();
            
            if(group == BuildTargetGroup.Standalone)
            {
                a_settings.m_scriptBackened.OverrideScriptingImplementation();
            }
            //This ensures we're not overriding the SDK when applying the preset in the switch or standalone settings, for instance
            if(group == BuildTargetGroup.PS4)
            {
                if (PlayerSettings.PS4.SdkOverride != "")
                {
                    a_settings.m_ps4SDKOverride.ValuePath = PlayerSettings.PS4.SdkOverride;
                }
            }

            #endregion
            return a_settings;
        }

        /// <summary>
        /// Generic static method to apply a given settings struct.
        /// </summary>
        /// <param name="a_settings">The struct with the settings to apply.</param>
        private static void ApplyPlayerSettings(BuildAndPlayerSettings a_settings, BuildTargetGroup group)
        {
            #region PLAYER SETTINGS
            #region Main Settings
            if (a_settings.m_productName.Enabled) PlayerSettings.productName = a_settings.m_productName;
            if (a_settings.m_companyName.Enabled) PlayerSettings.companyName = a_settings.m_companyName.Value;


            //Icon needs an array of 1 tex2D, target unknown because it applies to every target group. 
            //Specifying which one sets it as if we were overriding the one already existing
            if (a_settings.m_gameIcon.Value != null && a_settings.m_gameIcon.Enabled)
            {
                Texture2D[] bruh = new Texture2D[] { a_settings.m_gameIcon };
                PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.Unknown, bruh);
            } 
            #endregion
            AssetDatabase.SaveAssets();

            #region Resolution Settings
            if (a_settings.m_aspectRatios.Enabled)
            {
                PlayerSettings.SetAspectRatio(AspectRatio.Aspect16by10, a_settings.m_aspectRatios.HasAspectRatioFlagged(AspectRatio.Aspect16by10));
                PlayerSettings.SetAspectRatio(AspectRatio.Aspect16by9, a_settings.m_aspectRatios.HasAspectRatioFlagged(AspectRatio.Aspect16by9));
                PlayerSettings.SetAspectRatio(AspectRatio.Aspect4by3, a_settings.m_aspectRatios.HasAspectRatioFlagged(AspectRatio.Aspect4by3));
                PlayerSettings.SetAspectRatio(AspectRatio.Aspect5by4, a_settings.m_aspectRatios.HasAspectRatioFlagged(AspectRatio.Aspect5by4));
                PlayerSettings.SetAspectRatio(AspectRatio.AspectOthers, a_settings.m_aspectRatios.HasAspectRatioFlagged(AspectRatio.AspectOthers));
            }  
            #endregion
            #endregion

            #region BUILD SETTINGS
            if (a_settings.m_scenes.Enabled) EditorBuildSettings.scenes = a_settings.m_scenes.GetPresetScenes();
            if (a_settings.m_developmentMode.Enabled) EditorUserBuildSettings.development = a_settings.m_developmentMode;
            if (a_settings.m_autoConnectProfiler.Enabled) EditorUserBuildSettings.connectProfiler = a_settings.m_autoConnectProfiler.Value;
            if (a_settings.m_scriptDebugging.Enabled)
            {
                EditorUserBuildSettings.allowDebugging = a_settings.m_scriptDebugging.Value;
                if (a_settings.m_waitForManagedDebugger.Enabled)
                    EditorUserBuildSettings.waitForManagedDebugger = a_settings.m_waitForManagedDebugger.Value;
                //UnityEditor.BuildPlayerWindow.DefaultBuildMethods.

            }
            if (a_settings.m_scriptsOnly.Enabled) EditorUserBuildSettings.buildScriptsOnly = a_settings.m_scriptsOnly;
            #endregion

            #region OTHER SETTINGS
            if (a_settings.m_defines.Enabled)
            {
                string definesToFeed = "";
                for (int i = 0; i < a_settings.m_defines.m_value.Count; i++)
                {
                    if (a_settings.m_defines.m_value[i].Enabled)
                    {
                        definesToFeed += $"{a_settings.m_defines.m_value[i].Value};";
                    }
                }
                if(definesToFeed != "")
                {
                    //Remove the last ';'
                    definesToFeed.Remove(definesToFeed.Length - 1);
                }
                PlayerSettings.SetScriptingDefineSymbolsForGroup(group, definesToFeed);
            }
            //Checks the Enabled themselves
            a_settings.m_stackTraces.ApplyStackTraces();


            if(group == BuildTargetGroup.Standalone)
            {
                a_settings.m_scriptBackened.ApplyScriptingImplementation();
            }

            if (group == BuildTargetGroup.PS4)
            {
                PlayerSettings.PS4.SdkOverride =  a_settings.m_ps4SDKOverride.ValuePath;
            }
            #endregion

            AssetDatabase.SaveAssets();
        }

        /// <summary>
        /// Returns a BuildOption enum with the current options selected from this preset.
        /// </summary>        
        public BuildOptions GetBuildSettings()
        {
            BuildOptions toReturn = (BuildOptions)0;

            //Development mode
            if(m_settings.m_developmentMode.Enabled && m_settings.m_developmentMode.Value)
            {
                toReturn = toReturn.SetFlag(BuildOptions.Development);
            
            }
            //Script debugging
            if(m_settings.m_scriptDebugging.Enabled && m_settings.m_scriptDebugging.Value)
            {
                toReturn = toReturn.SetFlag(BuildOptions.AllowDebugging);
            }
            

            //Script Only
            if (m_settings.m_scriptsOnly.Enabled && m_settings.m_scriptsOnly.Value)
            {
                toReturn = toReturn.SetFlag(BuildOptions.BuildScriptsOnly);
            }

            //Extras about the resultant build
            if (m_runAfterBuild)
            {
                toReturn= toReturn.SetFlag(BuildOptions.AutoRunPlayer);
            }
            else
            {
                toReturn = toReturn.SetFlag(BuildOptions.ShowBuiltPlayer);
            }

            return toReturn;
        }

        internal BuildTargetGroup GetGroupTarget(BuildTarget target)
        {
            switch (target)
            {
                case BuildTarget.StandaloneOSX:
                    {
                        return BuildTargetGroup.Standalone;
                    }
                case BuildTarget.StandaloneWindows:
                    {
                        return BuildTargetGroup.Standalone;
                    }
                case BuildTarget.iOS:
                    {
                        return BuildTargetGroup.iOS;
                    }
                case BuildTarget.Android:
                    {
                        return BuildTargetGroup.Android;
                    }
                case BuildTarget.StandaloneWindows64:
                    {
                        return BuildTargetGroup.Standalone;
                    }
                case BuildTarget.WebGL:
                    {
                        return BuildTargetGroup.WebGL;
                    }
                case BuildTarget.WSAPlayer:
                    {
                        return BuildTargetGroup.WSA;
                    }
                case BuildTarget.StandaloneLinux64:
                    {
                        return BuildTargetGroup.Standalone;
                    }
                case BuildTarget.PS4:
                    {
                        return BuildTargetGroup.PS4;
                    }
                case BuildTarget.tvOS:
                    {
                        return BuildTargetGroup.tvOS;
                    }
                case BuildTarget.Switch:
                    {
                        return BuildTargetGroup.Switch;
                    }
                case BuildTarget.Lumin:
                    {
                        return BuildTargetGroup.Lumin;
                    }
                case BuildTarget.Stadia:
                    {
                        return BuildTargetGroup.Stadia;
                    }
                case BuildTarget.CloudRendering:
                    {
                        return BuildTargetGroup.CloudRendering;
                    }
                case BuildTarget.NoTarget:
                    {
                        return BuildTargetGroup.Unknown;
                    }
                case BuildTarget.XboxOne:
                    {
                        return BuildTargetGroup.XboxOne;
                    }
                default:
                    Debug.LogWarning($"The build target {target} is either obsolete or unavailable.");
                    return BuildTargetGroup.Unknown;
                    
            }
        }

        /// <summary>
        /// Replaces both build and player settings to what they were before building if the preset wants to.
        /// </summary>
        public void RestorePlayerSettings()
        {
            if (m_restorePlayerSettingsAfterBuild) 
            {
                ApplyPlayerSettings(m_playerSettingsBackup, groupTarget);
                //if (EditorUserBuildSettings.activeBuildTarget != m_previousBuildTarget)
                //    EditorUserBuildSettings.SwitchActiveBuildTarget(m_previousBuildTargetGroup, m_previousBuildTarget);
            }

        }

        /// <summary>
        /// Returns the BuildPlayerOptions from this preset.
        /// </summary>
        /// <remarks>
        /// Also applies the player settings, build settings and builds the assets accordingly.
        /// </remarks>
        public BuildPlayerOptions GetBuildOptions()
        {
            BuildPlayerOptions playerOptions = new BuildPlayerOptions();

            //The build targets
            playerOptions.target = m_buildTarget;
            playerOptions.targetGroup = groupTarget;
            
            //Sets them to the player settings
            ApplyPlayerSettings();
            BuildAssetsBundle();

            //If no path is provided, a default path where the game will be built is made
            if(m_pathBuild.Length ==0)
            {
                playerOptions.locationPathName = $"Builds/{playerOptions.targetGroup}/{PlayerSettings.productName}";
            }
            else
            {
                string localPath = m_pathBuild.ConvertFullPathToProjectPath().AddSlash();
                playerOptions.locationPathName = $"{localPath}{PlayerSettings.productName}";
            }

            //Some build options are dependent on the platform
            if(m_buildTarget == BuildTarget.StandaloneWindows)
            {
                playerOptions.locationPathName += ".exe";
            }
            else if(m_buildTarget == BuildTarget.PS4)
            {
                EditorUserBuildSettings.ps4BuildSubtarget = m_settings.m_ps4BuildSubtarget;
                Debug.Log("PS4!");

            }

            //Scenes and the build options are set on the preset too
            playerOptions.scenes = m_settings.m_scenes.GetPresetScenesPaths();
            playerOptions.options = GetBuildSettings();
            
            return playerOptions;
        }

#endregion
    }


}

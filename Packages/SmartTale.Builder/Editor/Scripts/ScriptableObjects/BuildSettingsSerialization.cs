﻿namespace SmartTale.Framework.Builder
{


    using UnityEngine;
    using UnityEditor;
    using System.IO;
    public partial class BuildSettingsSO //Serialization and JSON
    {
        private BuildAndPlayerSettings previousBPSettings;

        //All these are toggles that prevent the object from serializing or deserializing because of some or other reason that could lead to errors
        private bool isBeingCreated = false;
        private bool isCopyingFromPlayer = false;
        private bool isSavingToJSON = false;
        private bool _canSerializeOrDeserialize => !isBeingCreated && !isCopyingFromPlayer && m_fileSoRelativePath != "" && m_useJsonAndSerialization;




        //Read the json and applies to the struct
        public void OnBeforeSerialize()
        {

            if (!_canSerializeOrDeserialize) return;
            if (m_filePreviousName == "")
            {
                UpdatePaths();
            }

            //In case we rename the file, move and rename the JSON file
            else if ((m_filePreviousName != name) || (AssetDatabase.GetAssetPath(this) != m_fileSoRelativePath))
            {
                //if (AssetDatabase.GetAssetPath(this) != fileSoRelativePath) Debug.Log("Poggers!");
                string oldJson = m_fileJsonAbsolutePath;
                string oldJsonMeta = m_fileJsonAbsolutePath + ".meta";
                UpdatePaths();
                File.Move(oldJson, m_fileJsonAbsolutePath);
                if (File.Exists(oldJsonMeta)) File.Delete(oldJsonMeta);
                AssetDatabase.Refresh();
            }
            if (!File.Exists(m_fileJsonAbsolutePath))
            {
                Debug.LogWarning($"[BuildPreset] Json from {name} at {m_fileJsonAbsolutePath} doesn't exist! Creating now...");
                CreateJSONFile();
            }
            else
            {
                if (isSavingToJSON)
                {
                    Debug.Log("It is currently saving to the JSON so I can't yet copy the JSON to the info!");
                    return;
                }


                //Converts to Json and save the changes
                string textToConvert = File.ReadAllText(m_fileJsonAbsolutePath);
                BuildAndPlayerSettings readInfo = JsonUtility.FromJson<BuildAndPlayerSettings>(textToConvert);
                if (!readInfo.Equals(m_settings))
                {
                    //Debug.Log($"There have been some changes to the JSON, so I'll apply them to the struct! " +
                    //    $"\n The Name in actual was {m_settings.ProductName} and the Name in the json was {readInfo.ProductName}, and the Name in previous was {m_settings.ProductName}");
                    m_settings = readInfo;
                    AssetDatabase.Refresh();
                }

            }
        }


        //Check changes and writes them on the json
        public void OnAfterDeserialize()
        {

            if (!_canSerializeOrDeserialize) return;

            //Should have saved by now
            if (isSavingToJSON == true) isSavingToJSON = false;

            //If the settings stored to compare are different than our current settings, it means we changed something, and thus we need to update the Json file
            if (!previousBPSettings.Equals(m_settings))
                SaveToJSON();
            
            //else
            //{
            //    Debug.Log("There weren't any changes, no need to bother saving in json");
            //    return;
            //}
        }


        /// <summary>
        /// Saves the current settings to the JSON file.
        /// </summary>
        private void SaveToJSON()
        {
            //Debug.Log($"There has been a change! Overwriting previousInfo." +
            //        $"\n Name in actual is {m_settings.ProductName} and the Name in previous was {previousBPSettings.ProductName}");


            previousBPSettings = m_settings;
            isSavingToJSON = true;
            if (!Directory.Exists(m_fileDirectoryAbsolutePath))
            {
                Debug.Log($"Creating directory: {m_fileDirectoryAbsolutePath}");
                Helper.CreateRecursiveDirectory(m_fileDirectoryAbsolutePath, Helper.PathType.Directory);
            }
            else
            {
                string toSave = JsonUtility.ToJson(m_settings, true);
                File.WriteAllText(m_fileJsonAbsolutePath, toSave);

                isSavingToJSON = false;
            }
        }


        /// <summary>
        /// Creates the JSON file.
        /// </summary>
        private void CreateJSONFile()
        {
            isSavingToJSON = true;
            Helper.CreateRecursiveDirectory(m_fileDirectoryAbsolutePath, Helper.PathType.Directory);
            string toSave = JsonUtility.ToJson(m_settings, true);
            File.WriteAllText(m_fileJsonAbsolutePath, toSave);
            isSavingToJSON = false;
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

        }
    }


}

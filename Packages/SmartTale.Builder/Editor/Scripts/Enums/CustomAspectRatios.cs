﻿namespace SmartTale.Framework.Builder
{
    [System.Flags]
    public enum CustomAspectRatios
    {
        None = 0,
        Aspect4by3 = 1 << 1,
        Aspect5by4 = 1 << 2,
        Aspect16by10 = 1 << 3,
        Aspect16by9 = 1 << 4,
        Others = 1 << 5
    }

}
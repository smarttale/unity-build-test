﻿namespace SmartTale.Framework.Builder
{
    internal static class UIHelper 
    {
        /// <summary>
        /// The default relative path to the folder where the UI editors are stored.
        /// </summary>
        private const string m_defaultPath = "Packages/com.smart-tale.builder/Editor/UI/";
        /// <summary>
        /// The name of the variable in the build preset storing the actual settings
        /// </summary>
        public const string defaultSettingsNamePreset = "m_settings";
        /// <summary>
        /// The default name of the Toggle visual elements.
        /// </summary>
        public const string defaultToggleName = "toggle";
        /// <summary>
        /// The default name of the Label visual elements.
        /// </summary>
        public const string defaultLabelName = "label";
        /// <summary>
        /// The different extensions used in the UI Editor
        /// </summary>
        public enum UIType { uxml, uss, cs}
        /// <summary>
        /// Gets the relative path towards a file of a specific UI Type.
        /// </summary>
        /// <remarks>
        /// This assumes that every script is in a folder named "FolderName" for instance, and inside are stored the different files "FolderNameEditor.cs", "FolderNameEditor.uss" and "FolderNameEditor.xmln"
        /// </remarks>
        /// <param name="fileName">The name of one of the files.</param>
        /// <param name="type">The extension file we're looking for.</param>
        /// <returns>The relative path towards the requested file.</returns>
        public static  string GetPath(string fileName, UIType type)
        {            
            return m_defaultPath
                //Adds the FileName subfolder
                + fileName.Replace("Editor", "")
                //Then looks up for the specific file
                + $"/{fileName}.{type}";
        }
    }

}
namespace SmartTale.Framework.Builder
{
    using UnityEditor;    
    using UnityEngine.UIElements;
    //Leaving this in case I may need it (visual doesn't auto complete using namespaces)
    using UnityEditor.UIElements;

    /// <summary>
    /// Editor for a similar class than Toggle.
    /// </summary>
    /// <remarks>
    /// The difference with Toggle is that the checker is placed to the left, for visual convenience.
    /// </remarks>
    public class SimpleToggleEditor : BaseField<bool>
    {
        /// <summary>
        /// The label of this toggle.
        /// </summary>
        internal string labelText
        {
            get => this.Q<Label>("label").text;
            set
            {
                this.Q<Label>("label").text = value;
            }
        }

        //Needed by the UXML factory
        public SimpleToggleEditor() : this(null,null){}
  
        //Needed by the base field parent class
        public SimpleToggleEditor(string strung, VisualElement ve) : base(strung,ve)
        {
            AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UIHelper.GetPath(nameof(SimpleToggleEditor), UIHelper.UIType.uxml)).CloneTree(this);            
        }

        /// <summary>
        /// The value of this toggle. Influences the binding path object.
        /// </summary>
        public override bool value 
        {
            get => this.Q<Toggle>("toggle").value;
            set
            {
                this.Q<Toggle>("toggle").value = value;
            }
        }
        #region UXML Factory

        public new class UxmlFactory : UxmlFactory<SimpleToggleEditor, UxmlTraits> { }

        public new class UxmlTraits : BaseField<bool>.UxmlTraits
        {
            UxmlStringAttributeDescription m_Label = new UnityEngine.UIElements.UxmlStringAttributeDescription
            {
                name = "custom-label",
                defaultValue = "My Splendid label!"
            };

            public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
            {
                SimpleToggleEditor thisInstance = ve as SimpleToggleEditor;
                thisInstance.labelText = m_Label.GetValueFromBag(bag, cc);
                base.Init(ve, bag, cc);
            }


        } 
        #endregion
    } 
}
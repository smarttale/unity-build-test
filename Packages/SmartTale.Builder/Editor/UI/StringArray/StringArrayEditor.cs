namespace SmartTale.Framework.Builder
{
    using UnityEditor;
    using UnityEngine.UIElements;
    using UnityEditor.UIElements;


    /// <summary>
    /// The scene list property drawer for the build preset.
    /// </summary>
    /// <remarks>
    /// Handled separately of the general variables because it requires a different layout and size.
    /// </remarks>
    [CustomPropertyDrawer(typeof(BuilderVStringArray))]
    public class StringArrayEditor : PropertyDrawer
    {
        VisualElement m_container;
        PropertyField m_root;
        SerializedProperty m_arrayProp;
        
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            //Root
            m_root = new PropertyField();
            var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UIHelper.GetPath(nameof(StringArrayEditor), UIHelper.UIType.uxml));
            visualTree.CloneTree(m_root);
            m_container = m_root.ElementAt(1);
            m_arrayProp = property.FindPropertyRelative("m_value");            

            //Has the toggle, a label, the property itself, and a couple of buttons to add or remove elements
            Toggle toggle = m_root.Q<Toggle>("toggle");
            toggle.bindingPath = "m_enabled";

            Label label = m_root.Q<Label>("label");
            label.text = property.displayName;

            Button addButton = m_root.Q<Button>("addButton");
            Button removeButton = m_root.Q<Button>("removeButton");


            //Create the visualization and then wether or not we display it
            m_container.Clear();
            for (int i = 0; i < m_arrayProp.arraySize; i++)
            {
                BaseStringArrayElementEditor newElement = new BaseStringArrayElementEditor(this);
                newElement.bindingPath = $"m_value.Array.data[{i}]";

                m_container.Add(newElement);
            }
            bool enabled = property.FindPropertyRelative("m_enabled").boolValue;
            m_container.style.display = enabled ? DisplayStyle.Flex : DisplayStyle.None;
            addButton.SetEnabled(enabled);
            removeButton.SetEnabled(enabled);
            label.SetEnabled(enabled);

            //Add a new element
            addButton.clicked += delegate
            {
                //Create an additional slot
                int currentSize = m_arrayProp.arraySize;
                m_arrayProp.arraySize++;                

                //Create the element, bind it and add it to this array
                BaseStringArrayElementEditor newElement = new BaseStringArrayElementEditor(this, $"{m_root.bindingPath}m_value.Array.data[{currentSize}]");
                newElement.BindProperty(property.FindPropertyRelative($"m_value.Array.data[{currentSize}]"));
                m_container.Add(newElement);

                //Save
                m_arrayProp.serializedObject.ApplyModifiedProperties();
                AssetDatabase.SaveAssets();                
            };

            //Remove the last element of the array (if there's one)
            removeButton.clicked += delegate
            {
                if (m_container.childCount > 0)
                {
                    int index = m_container.childCount - 1;
                    m_arrayProp.DeleteArrayElementAtIndex(index);
                    m_container.RemoveAt(index);
                    m_arrayProp.serializedObject.ApplyModifiedProperties();
                }
            };

            //Hide or show the array altogether when disabled or enabled
            toggle.RegisterCallback<ChangeEvent<bool>>(a_event =>
            {
                m_container.style.display = a_event.newValue ? DisplayStyle.Flex : DisplayStyle.None;

                addButton.SetEnabled(a_event.newValue);
                removeButton.SetEnabled(a_event.newValue);
                label.SetEnabled(a_event.newValue);
            });



            return m_root;
        }

        /// <summary>
        /// Removes the given element from this array.
        /// </summary>
        /// <param name="a_element">The element to remove</param>
        internal void RemoveElement(BaseStringArrayElementEditor a_element)
        {
            for (int i = 0; i < m_container.childCount; i++)
            {
                //Find the element first, then remove it and exit
                if(m_container.ElementAt(i) == a_element)
                {
                    m_arrayProp.DeleteArrayElementAtIndex(i);
                    m_container.RemoveAt(i);
                    m_arrayProp.serializedObject.ApplyModifiedProperties();
                    return;
                }
            }            
        }

    } 
}
namespace SmartTale.Framework.Builder
{
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.UIElements;
    using UnityEditor.UIElements;
    using System.Collections.Generic;

    /// <summary>
    /// The Editor for the whole build preset scriptable object.
    /// </summary>
    /// <remarks>
    /// Handles singular cases like the current build target, wether a property should be visible depending on the value of another property, etc.
    /// </remarks>
    [CustomEditor(typeof(BuildSettingsSO))]
    public class BuildPresetEditor : Editor
    {
        #region Internal

        private BuildSettingsSO m_settings;
        private VisualElement m_root;
        /// <summary>
        /// The visual element containing both Build and Build And Run buttons
        /// </summary>
        private VisualElement m_buildButtonVE;
        private VisualElement m_switchTargetButtonVE;
        private PropertyField m_waitForManagedDebug;
        private PropertyField m_scriptDebugging;
        private Toggle m_developmentModeToggle;
        private EnumField m_buildTargetField;
        private List<BlockToggleEditor> m_blockToggleList;
        /// <summary>
        /// The list of properties that are dependant on the build target.        
        /// </summary>
        /// <remarks>
        /// These properties will be displayed or not depending on build target chosen on this preset, but also on the Platform chosen in their UXML Trait.
        /// Properties without this trait will always be shown, unless some other requirement is unfulfilled (for instance, there's no point in showing the waitForDebugManager property if ScriptDebugging isn't enabled)
        /// KMS_TODO: The enum in the UXML Trait should actually be a flag since some parameter may be used by two different platforms (and not by a third platform)
        /// </remarks>
        private List<PlatformDependantPropertyEditor> m_platformDependantItems;

        /// <summary>
        /// A boolean to ensure we only initialize properties once.
        /// </summary>
        private bool m_initialized = false;
        #endregion

        #region LifeCycle

        private void OnEnable()
        {
            //Root
            m_root = new VisualElement();
            m_settings = (BuildSettingsSO)target;
            m_root.Bind(new SerializedObject(m_settings));
            string path = UIHelper.GetPath(nameof(BuildPresetEditor), UIHelper.UIType.uxml);
            VisualTreeAsset visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(path);
            visualTree.CloneTree(m_root);

            //The build target
            m_buildTargetField = m_root.Q<EnumField>("targetEnum");
            m_buildTargetField.RegisterValueChangedCallback(OnBuildTargetChange);

            //Build or switch platform buttons
            m_buildButtonVE = m_root.Q<VisualElement>("buildButtonsVE");
            m_switchTargetButtonVE = m_root.Q<VisualElement>("switchTargetButton");
            m_root.Q<Button>("switchTargetButton").clicked +=
                new System.Action(delegate {
                    EditorUserBuildSettings.SwitchActiveBuildTarget(m_settings.groupTarget, m_settings.m_buildTarget);
                });
            m_root.Q<Button>("buildButton").clicked += m_settings.Build;
            m_root.Q<Button>("buildAndRunButton").clicked += m_settings.BuildAndRun;

            //Apply or override presets
            Button applyPresetButton = m_root.Q<Button>("applyButton");
            Button overridePresetButton = m_root.Q<Button>("overrideButton");
            applyPresetButton.clicked += m_settings.ApplyPlayerSettings;
            overridePresetButton.clicked += m_settings.OverrideCurrentPresetSettings;
            m_waitForManagedDebug = m_root.Q<PropertyField>("waitForManagedDebugger");
            m_scriptDebugging = m_root.Q<PropertyField>("scriptDebugging");
            m_blockToggleList = new List<BlockToggleEditor>();
            m_blockToggleList = m_root.Query<BlockToggleEditor>().ToList();

            //Since we can't just update the visual whenever we want, we display an info box to remind the user to refresh the visual themselves
            overridePresetButton.clicked += delegate
            {
                IMGUIContainer infoContainer = new IMGUIContainer();
                infoContainer.onGUIHandler += () =>
                {
                    EditorGUILayout.HelpBox("Everything enabled has been overrided properly, but some of the elements visuals do not update automatically! Just deselect and select the preset again for it to update. This issue will be addressed later one.", MessageType.Info);
                };
                overridePresetButton.parent.parent.Add(infoContainer);
            };
        }

        public override VisualElement CreateInspectorGUI()
        {
            //Reserved for callbacks that need to read inside the property fields that haven't been yet read
            //This event is called after the editor has calculated the layout size needed to display itself, and thus has read the property fields, their layout etc. and thus they're available to be read
            //For instance, trying to get a toggle inside the property outside of this scope would just result on null since the property hasn't made the toggle available, it's known as just a property field, with the contents unknown to others
            m_root.RegisterCallback<GeometryChangedEvent>(evt =>
            {
                //Rebinds the parent of the defines
                PropertyField arrayEditorProperty = m_root.Q<PropertyField>("defines");
                arrayEditorProperty.bindingPath = "m_settings.m_defines.";

                //Since we only want to read it just once the rest
                if (m_initialized) return;

                m_initialized = true;

                //No development mode means none of the development tools should be available
                m_developmentModeToggle = GetToggleField("developmentMode");
                Toggle devModeValueToggle = m_root.Q<PropertyField>("developmentMode").Q<PropertyField>("property").Query<Toggle>().First();
                bool enableDevTools = devModeValueToggle.value && m_developmentModeToggle.value;

                //Also disable the bool values if development is turned off
                bool includeToggle = enableDevTools ? false : true;
                EnableOrDisableProperty("scriptDebugging", enableDevTools, includeToggle);
                EnableOrDisableProperty("scriptsOnly", enableDevTools, includeToggle);

                devModeValueToggle.RegisterValueChangedCallback(toggleEvt =>
                {
                    if (m_developmentModeToggle.value)
                    {
                        //Also disable the bool values if development is turned off
                        bool include = toggleEvt.newValue ? false : true;
                        EnableOrDisableProperty("scriptDebugging", toggleEvt.newValue, includeToggle);
                        EnableOrDisableProperty("scriptsOnly", toggleEvt.newValue, includeToggle); 
                    }
                });
                m_developmentModeToggle.RegisterValueChangedCallback(toggleEvt =>
                {
                    if (devModeValueToggle.value)
                    {
                        //Also disable the bool values if development is turned off
                        bool include = toggleEvt.newValue ? false : true;
                        EnableOrDisableProperty("scriptDebugging", toggleEvt.newValue, includeToggle);
                        EnableOrDisableProperty("scriptsOnly", toggleEvt.newValue, includeToggle); 
                    }
                });

                //ScriptDebugging and WaitForManagedDebug properties
                m_waitForManagedDebug.style.display = m_scriptDebugging.Q<Toggle>("toggle").value ? DisplayStyle.Flex : DisplayStyle.None;

                Toggle scriptDebuggingToggle = m_scriptDebugging.Q<PropertyField>("property").Query<Toggle>().First();
                if (scriptDebuggingToggle.value == false)
                {
                    m_waitForManagedDebug.Q<Toggle>("toggle").value = false;
                }                   
                m_waitForManagedDebug.style.display = scriptDebuggingToggle.value? DisplayStyle.Flex : DisplayStyle.None;
                scriptDebuggingToggle.RegisterValueChangedCallback(a_evt =>
                {
                    m_waitForManagedDebug.style.display = a_evt.newValue ? DisplayStyle.Flex : DisplayStyle.None;
                    if (a_evt.newValue == false)
                    {
                        m_waitForManagedDebug.Q<Toggle>("toggle").value = false;
                    }
                });

                //Update platform dependant variables
                m_platformDependantItems = new List<PlatformDependantPropertyEditor>();
                Debug.Log("A");
                RecursiveAddToList(m_root);
                for (int i = 0; i < m_platformDependantItems.Count; i++)
                {
                    m_platformDependantItems[i].UpdateStatus(m_settings.m_buildTarget);
                }

                foreach (BlockToggleEditor block in m_blockToggleList)
                {
                    block.UpdateDisplay((BuildTarget)m_buildTargetField.value);
                }
                //Update block editor visual


            });
            UpdateVisual();

            return m_root;
        }

        /// <summary>
        /// The usual stuff to display when the editor is created.
        /// </summary>
        /// <remarks>
        /// The reason it is separated in a different method is because I was trying to force a "redraw" of the editor whenever I needed it to (for instance, after overriding the values with those in the player/build settings) because some properties do not automatically redraw themselves despite being binded.
        /// The list of defines for instance, updates the value of the already created elements, but if there were 4 defines in the preset and 6 in the player settings, it won't extend the array to 6 elements in the visual, meaning the user has to exit the object clicking away and reselect it for the visual to properly update.
        /// </remarks>
        private void UpdateVisual()
        {
            //Show and hide buttons depending on wether the build target of this preset is the same of the one in the build settings
            bool syncroTarget = m_settings.m_buildTarget.Equals(EditorUserBuildSettings.activeBuildTarget);
            m_buildButtonVE.style.display = syncroTarget ? DisplayStyle.Flex : DisplayStyle.None;
            m_switchTargetButtonVE.style.display = syncroTarget ? DisplayStyle.None : DisplayStyle.Flex;


        }
        #endregion


        //Events to handle will be placed here if they're big enough
        #region HANDLERS
        /// <summary>
        /// Called when we change the build target.
        /// </summary>
        /// <remarks>
        /// Hides or shows some buttons depending on if the selected build target is the same as the actual target in Unity or not.
        /// </remarks>
        /// <param name="a_event">The event containing the info.</param>
        private void OnBuildTargetChange(ChangeEvent<System.Enum> a_event)
        {
            //buttons
            VisualElement buildButton = m_root.Q<VisualElement>("buildButtonsVE");
            VisualElement switchTargetButton = m_root.Q<VisualElement>("switchTargetButton");

            //The synchronicity of both values
            bool syncroTarget = a_event.newValue.Equals(EditorUserBuildSettings.activeBuildTarget);
            buildButton.style.display = syncroTarget ? DisplayStyle.Flex : DisplayStyle.None;
            switchTargetButton.style.display = syncroTarget ? DisplayStyle.None : DisplayStyle.Flex;

            //Show or hide platform dependant properties
            for (int i = 0; i < m_platformDependantItems.Count; i++)
            {
                m_platformDependantItems[i].UpdateStatus((BuildTarget)a_event.newValue);
            } 

            foreach(BlockToggleEditor block in m_blockToggleList)
            {
                block.UpdateDisplay((BuildTarget)a_event.newValue);
            }

        }
        #endregion


        //Helpers for this script. Some of them might be extracted as extensions in the future
        #region HELPER
        /// <summary>
        /// Visually enables or disables the given property.
        /// </summary>
        /// <remarks>
        /// The property must be a property field in the UXML.
        /// If disabled we can still see the property(slightly faded out), we're just unable to interact with it.
        /// If we want to also affect a toggle inside it, the toggle must be named "toggle".
        /// Returns the property in case we might need it.
        /// </remarks>
        /// <param name="a_name">The name of the property.</param>
        /// <param name="a_value">Wether we enabled it or not.</param>
        /// <param name="a_includeToggle">Wether or not to apply the same value to a child toggle.</param>
        /// <returns>The property affected.</returns>
        private PropertyField EnableOrDisableProperty(string a_name, bool a_value, bool a_includeToggle)
        {
            PropertyField prop = m_root.Q<PropertyField>(a_name);
            prop.SetEnabled(a_value);
            if (a_includeToggle)
            {
                // prop.Q<Toggle>("property").value = a_value;
                prop.Query<Toggle>().ForEach(toggle =>
                {
                    toggle.value = a_value;
                });
            }
            return prop;
        }

        /// <summary>
        /// Gets the Toggle visual element children of the given visual element.
        /// </summary>
        /// <remarks>
        /// The visual element given by the name must contain a Toggle as child named "toggle".
        /// </remarks>
        /// <param name="a_name">The name of the visual element containing the toggle.</param>
        /// <returns>The requested Toggle. </returns>
        private Toggle GetToggleField(string a_name)
        {
            return m_root.Q<VisualElement>(a_name).Q<Toggle>("toggle");
        }

        /// <summary>
        /// Looks up the given property, enables or disables its visibility, and returns the property itself.
        /// </summary>
        /// <param name="a_name">The name of the property to look for.</param>
        /// <param name="a_value">Wether or not to enable its visibility.</param>
        /// <returns>The requested property.</returns>
        private PropertyField DisplayOrHideProperty(string a_name, bool a_value)
        {
            PropertyField prop = m_root.Q<PropertyField>(a_name);
            prop.style.display = a_value ? DisplayStyle.Flex : DisplayStyle.None;
            return prop;
        }
        /// <summary>
        /// Enables or not the visibility of the given property.
        /// </summary>
        /// <param name="field">The property to manage.</param>
        /// <param name="value">Wether or not to enable its visibility.</param>
        private void DisplayOrHideProperty(PropertyField field, bool value)
        {
            field.style.display = value ? DisplayStyle.Flex : DisplayStyle.None;
        }

        /// <summary>
        /// Method used to recursively look and store every property that's platform dependant.
        /// </summary>
        /// <param name="ve">The visual element to check the dependency from, and their children.</param>
        private void RecursiveAddToList(VisualElement ve)
        {
            if(ve is PlatformDependantPropertyEditor propertyEditor)
            {
                if (propertyEditor != null)
                {
                    m_platformDependantItems.Add(propertyEditor);
                }
            }
            else if (ve.childCount > 0)
            {
                for (int i = 0; i < ve.childCount; i++)
                {
                    RecursiveAddToList(ve.ElementAt(i));
                }
            }
        }


        #endregion
    }
}
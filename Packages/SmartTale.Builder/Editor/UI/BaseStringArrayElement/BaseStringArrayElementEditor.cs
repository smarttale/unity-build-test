namespace SmartTale.Framework.Builder
{
    using UnityEditor;
    using UnityEngine.UIElements;

    /// <summary>
    /// The editor for the element of an array of type string.
    /// </summary>
    public class BaseStringArrayElementEditor : BindableElement
    {
        private StringArrayEditor m_arrayParent;
        private bool m_initialized;
        private TextField m_textField;

        public BaseStringArrayElementEditor(StringArrayEditor a_parent, string a_bindindPath = "")
        {
            this.m_arrayParent = a_parent;
            bindingPath = a_bindindPath;

            //Root
            VisualElement root = this;          
            var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UIHelper.GetPath(nameof(BaseStringArrayElementEditor), UIHelper.UIType.uxml));
            visualTree.CloneTree(root);
            StyleSheet stylesheet = AssetDatabase.LoadAssetAtPath<StyleSheet>(UIHelper.GetPath(nameof(BaseStringArrayElementEditor), UIHelper.UIType.uss));
            root.styleSheets.Add(stylesheet);


            //Text field
            m_textField = root.Q<TextField>("textField");
            m_textField.bindingPath = "m_value";

            //Toggle
            Toggle toggle = root.Q<Toggle>("toggle");
            toggle.bindingPath = "m_enabled";
            toggle.RegisterValueChangedCallback(a_evt =>
            {
                m_textField.SetEnabled(a_evt.newValue);
            });
            
            //Enable or disable textfield when created
            root.RegisterCallback<GeometryChangedEvent>(a_evt =>
            {
                if (!m_initialized)
                {
                    m_textField.SetEnabled(toggle.value);
                    m_initialized = true;
                }
            });

            //Button to remove this element from the array
            Button button = root.Q<Button>("button");
            button.clicked += RemoveThis;           
        }

        private void RemoveThis()
        {
            m_arrayParent.RemoveElement(this);
        }
    } 
}
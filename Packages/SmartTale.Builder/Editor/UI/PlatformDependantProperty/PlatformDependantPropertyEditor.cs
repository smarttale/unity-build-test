﻿namespace SmartTale.Framework.Builder
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UIElements;
    using UnityEditor.UIElements;
    using UnityEditor;

    public class PlatformDependantPropertyEditor : PropertyField
    {
        public BuildTarget buildTarget { get; set; }
        public PlatformDependantPropertyEditor()
        {
            Debug.Log("C");
            RegisterCallback<GeometryChangedEvent>(evt =>
            {
                Debug.Log("D");
            });
        }
        public void UpdateStatus(BuildTarget newTarget)
        {
            if(buildTarget == BuildTarget.NoTarget)
            {
                return;
            }
            bool value = newTarget == buildTarget;
            style.display = value ? DisplayStyle.Flex : DisplayStyle.None;
            Toggle toggle = this.Q<Toggle>("toggle");
            //Still verify if it is true because the block editor takes into account wether a parameter is enabled or not
            //Therefore if we turn off a parameter that was already off, the block editor subtracts one to the list of enabled parameter, even though it really hasn't changed
            if (value == false && toggle.value == true)
            {
                Debug.Log("B");
                this.Q<Toggle>("toggle").value = false;
            }
            
            
        }

        public new class UxmlFactory : UxmlFactory<PlatformDependantPropertyEditor, UxmlTraits> { }

        public new class UxmlTraits : PropertyField.UxmlTraits
        {
            public UxmlEnumAttributeDescription<BuildTarget> m_buildTarget = new UxmlEnumAttributeDescription<BuildTarget>
            {
                name = "build-target",
                defaultValue = BuildTarget.NoTarget
            };

            public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
            {
                base.Init(ve, bag, cc);                
                BuildTarget newValue = m_buildTarget.GetValueFromBag(bag, cc);
                PlatformDependantPropertyEditor prop = ve as PlatformDependantPropertyEditor;
                if (prop.buildTarget != newValue)
                    m_buildTarget.defaultValue = newValue;
                prop.buildTarget = newValue;
            }

        }
    }

}
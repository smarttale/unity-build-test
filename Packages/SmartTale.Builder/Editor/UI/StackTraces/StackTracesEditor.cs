namespace SmartTale.Framework.Builder
{
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.UIElements;
    using UnityEditor.UIElements;
    using System.Collections.Generic;

    [CustomPropertyDrawer(typeof(BuilderVStackTrace))]
    public class StackTracesEditor : PropertyDrawer
    {
        private string[] logTypes = new string[] { "log", "warning", "error", "assert", "exception" };

        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            //Root
            PropertyField root = new PropertyField();
            root.Bind(property.serializedObject);
            var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UIHelper.GetPath(nameof(StackTracesEditor), UIHelper.UIType.uxml));
            visualTree.CloneTree(root);

            //Apply the bindings
            foreach (string logType in logTypes)
            {
                PropertyField propField = root.Q<PropertyField>(logType);                
                propField.bindingPath = $"m_{logType}";

            }

            //Displays and enabled depending on the Toggles
            VisualElement logsVE = root.Q<VisualElement>("logsVE");
            Label label = root.Q<Label>("label");
            Toggle toggle = root.Q<Toggle>("toggle");
            toggle.bindingPath = "m_enabled";
            toggle.RegisterValueChangedCallback(evt =>
            {
                label.SetEnabled(evt.newValue);
                logsVE.style.display = evt.newValue ? DisplayStyle.Flex : DisplayStyle.None;
            });

            root.RegisterCallback<GeometryChangedEvent>(evt =>
            {
                label.SetEnabled(toggle.value);
                logsVE.style.display = toggle.value? DisplayStyle.Flex : DisplayStyle.None;
            });

            return root;
        }

    } 
}
namespace SmartTale.Framework.Builder
{
    using UnityEditor;
    using UnityEngine.UIElements;
    using UnityEditor.UIElements;

    /// <summary>
    /// The editor for the generic variables. The property field adjusts to the type.
    /// </summary>
    [CustomPropertyDrawer(typeof(BaseBuilderVariable), true)]
    public class BaseVariableEditor : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            PropertyField root = new PropertyField();
            VisualElement visualElement = root.Q<VisualElement>("content");            
            var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UIHelper.GetPath(nameof(BaseVariableEditor), UIHelper.UIType.uxml));
            visualTree.CloneTree(root);
            StyleSheet stylesheet = AssetDatabase.LoadAssetAtPath<StyleSheet>(UIHelper.GetPath(nameof(BaseVariableEditor), UIHelper.UIType.uss));
            root.styleSheets.Add(stylesheet);

            //Has a label, a toggle and the property itself
            Toggle toggle = root.Q<Toggle>("toggle");
            toggle.bindingPath = "m_enabled";
            toggle.Bind(property.serializedObject);

            Label label = root.Q<Label>("label");
            label.text = property.displayName;
            string tooltip = property.FindPropertyRelative("m_description").stringValue;
            if (tooltip != "") label.tooltip = tooltip;

            PropertyField prop = root.Q<PropertyField>("property");
            if (prop != null)
            {
                prop.label = "";
                prop.bindingPath = "m_value";
                prop.Bind(property.serializedObject);
                prop.SetEnabled(property.FindPropertyRelative("m_enabled").boolValue);                
            }

            //Enables or disables the variable depending on the toggle
            toggle.RegisterCallback<ChangeEvent<bool>>(evt =>
            {
                if (prop != null)
                    prop.SetEnabled(evt.newValue);
            });

            return root;
        }
    } 
}
namespace SmartTale.Framework.Builder
{
    using UnityEditor;
    using UnityEngine.UIElements;
    using UnityEditor.UIElements;

    /// <summary>
    /// The scene list property drawer for the build preset.
    /// </summary>
    /// <remarks>
    /// Handled separately of the general variables because it requires a different layout and size.
    /// </remarks>
    [CustomPropertyDrawer(typeof(BuilderVScenes))]
    public class ScenesListEditor : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            PropertyField root = new PropertyField();
            var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UIHelper.GetPath(nameof(ScenesListEditor), UIHelper.UIType.uxml));
            visualTree.CloneTree(root);

            //Has the toggle, a label, and then the property itself, in this case a list view
            Toggle toggle = root.Q<Toggle>("toggle");
            toggle.bindingPath = "m_enabled";
            toggle.Bind(property.serializedObject);

            Label label = root.Q<Label>("label");
            label.text = property.displayName;

            //Instead of just closing on and off the list view, like a foldown, or having it take space in the layout permanently, hide it if we're not going to use it
            //ListView sceneListField = root.Q<ListView>("valueList");
            //sceneListField.bindingPath = "m_value";
            //sceneListField.Bind(property.serializedObject);
            //sceneListField.style.display = property.FindPropertyRelative("m_enabled").boolValue? DisplayStyle.Flex : DisplayStyle.None;

            PropertyField scenePropertyField = root.Q<PropertyField>("property");
            scenePropertyField.bindingPath = "m_value";
            scenePropertyField.style.display = property.FindPropertyRelative("m_enabled").boolValue? DisplayStyle.Flex : DisplayStyle.None;
            //sceneListField.SetEnabled(property.FindPropertyRelative("m_enabled").boolValue);

            toggle.RegisterCallback<ChangeEvent<bool>>(a_event =>
            {
                //sceneListField.style.display = a_event.newValue ? DisplayStyle.Flex : DisplayStyle.None;
                scenePropertyField.style.display = a_event.newValue ? DisplayStyle.Flex : DisplayStyle.None;
            });

            return root;
        }
    } 
}
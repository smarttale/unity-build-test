namespace SmartTale.Framework.Builder
{
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.UIElements;
    using UnityEditor.UIElements;


    public class DirectoryButtonEditor : BindableElement
    {
        internal enum PathType { File, Directory}
        internal PathType m_currentFileType;
        internal string buttonLabel
        {
            get => this.Q<Button>("button").text;
            set => this.Q<Button>("button").text = value;
        }
        internal string m_extension;
        public DirectoryButtonEditor()
        {
            VisualTreeAsset tree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UIHelper.GetPath(nameof(DirectoryButtonEditor), UIHelper.UIType.uxml));
            tree.CloneTree(this);


            Button button = this.Q<Button>("button");
            Label textField = this.Q<Label>("property");
            Toggle toggle = this.Q<Toggle>("toggle");

            toggle.bindingPath = "m_enabled";
            textField.bindingPath = "m_value";

            toggle.RegisterValueChangedCallback(a_evt =>
            {
                button.SetEnabled(a_evt.newValue);
                textField.SetEnabled(a_evt.newValue);
            });

            button.clicked += delegate
            {
                string directory = GetPath();
                if(directory != "")
                {
                    textField.text = directory.ConvertFullPathToProjectPath();
                }
                else
                {
                    textField.text = "None.";
                }
            };

            this.RegisterCallback<GeometryChangedEvent>(a_evt =>
            {
                if(toggle.value == false)
                {
                    button.SetEnabled(false);
                    textField.SetEnabled(false);
                }
            });
            
        }

        public new class UxmlFactory : UxmlFactory<DirectoryButtonEditor, UxmlTraits> { }

        public new class UxmlTraits : BindableElement.UxmlTraits
        {
            UxmlStringAttributeDescription m_Label = new UnityEngine.UIElements.UxmlStringAttributeDescription
            {
                name = "custom-label",
                defaultValue = "Directory button"
            };

            UxmlStringAttributeDescription m_Extension = new UnityEngine.UIElements.UxmlStringAttributeDescription
            {
                name = "extension",
                defaultValue = "*"
            };

            UxmlEnumAttributeDescription<PathType> m_PathType = new UxmlEnumAttributeDescription<PathType>
            {
                name = "path-type",
                defaultValue = PathType.File
            };

            public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
            {
                DirectoryButtonEditor thisInstance = ve as DirectoryButtonEditor;
                thisInstance.buttonLabel = m_Label.GetValueFromBag(bag, cc);
                thisInstance.m_extension = m_Extension.GetValueFromBag(bag, cc);
                thisInstance.m_currentFileType = m_PathType.GetValueFromBag(bag, cc);
                base.Init(ve, bag, cc);
            }


        }

        private string GetPath()
        {
            if(m_currentFileType == PathType.Directory)
            {
                return EditorUtility.OpenFolderPanel("Location", Application.dataPath, "");
            }
            else
            {
                return EditorUtility.OpenFilePanel("File", Application.dataPath, m_extension);
            }
        }
    } 
}
namespace SmartTale.Framework.Builder
{
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.UIElements;
    using System.Collections.Generic;

    /// <summary>
    /// The Editor for a box group like custom visual element.
    /// </summary>
    /// <remarks>
    /// This visual element encompasses other properties as children and let us show them or hide them, but also affect their enabled status (enabled as in the BuilderVariable Enabled value) altogether, being able to turn them all on or off.
    /// </remarks>
    public class BlockToggleEditor : VisualElement, INotifyValueChanged<bool>
    {
        /// <summary>
        /// The label displayed as string.        
        /// </summary>
        public string labelToDisplay
        {
            get => m_label.text;
            set 
            { 
                m_label.text = value; 
            }
        }
        /// <summary>
        /// The name of the uss class for the visually disabled  label
        /// </summary>
        private const string m_disabledUSSName = "crossedOut";
        /// <summary>
        /// The name of the uss class to show a partial but not complete completion of the children toggles.
        /// When at least 1 toggle is on and there are more than 1 toggles among the children, this class will be applied to this header toggle.
        /// </summary>
        //private const string togglePartialUSSName = "toggleMiddle";
        /// <summary>
        /// The main toggle of this block.
        /// </summary>
        private Toggle m_thisToggle;
        /// <summary>
        /// The Label visual element of this block.
        /// </summary>
        private Label m_label;
        /// <summary>
        /// The list of children toggles
        /// </summary>
        private List<Toggle> m_contentToggles;
        /// <summary>
        /// The amount of children toggles currently enabled
        /// </summary>
        private int m_enabledTogglesAmount;
        /// <summary>
        /// The amounts of events to skip on the WhenContentToggles method.
        /// </summary>
        private int m_eventsToSkip = 0;
        /// <summary>
        /// Wether this class has been already initialized or not
        /// </summary>
        private bool m_initialized = false;
        /// <summary>
        /// The storage for contentContainer.
        /// </summary>
        VisualElement m_container;
        /// <summary>
        /// The visual element where the children will be placed whenever we drag & drop an element to be placed as children of this block.
        /// </summary>
        public override VisualElement contentContainer => m_container;
        /// <summary>
        /// The different states of a BlockToggle
        /// </summary>
        private enum ToggleState { Unchecked, Partial, Toggled }
        /// <summary>
        /// When enabled, an icon will be show on top of the toggle block, indicating some of the toggles inside are enabled
        /// </summary>
        private VisualElement m_partialToggleVE;

        internal BuildTarget m_buildTargetRef { get; set; }
        

        //It actually doesn't work, but I'll figure it out at some point so I'm not deleting it
        #region View data persistence
        [SerializeField] private bool showContents = true;

        private Toggle visibilityToggle;
        public bool value
        {
            get => showContents;
            set
            {
                if (value == showContents) return;
                this.Q<VisualElement>("content").style.display = value ? DisplayStyle.Flex : DisplayStyle.None;
                showContents = value;
            }
        }

        public void SetValueWithoutNotify(bool newValue)
        {
            showContents = newValue;
            contentContainer.style.display = newValue ? DisplayStyle.Flex : DisplayStyle.None;
        } 
        #endregion

        public BlockToggleEditor()
        {
            VisualTreeAsset visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UIHelper.GetPath(nameof(BlockToggleEditor), UIHelper.UIType.uxml));
            visualTree.CloneTree(this);            

            //Gets the visual elements
            m_container = this.Q<VisualElement>("content");
            m_label = this.Q<Label>(UIHelper.defaultLabelName);
            m_thisToggle = this.Q<Toggle>("headerToggle");
            visibilityToggle = this.Q<Toggle>("visibilityToggle");
            m_partialToggleVE = this.Q<VisualElement>("partialToggleVE");

            //Called when the layout space is calculated, and therefore all the property fields have been read And thus we can check its contents
            this.RegisterCallback<GeometryChangedEvent>(a_evt =>
            {
                //This is to avoid rechecking everytime we resize the window
                if (m_initialized)
                {
                    return;
                }
                m_initialized = true;

                //The open/close behaviour
                Button headerElement = this.Q<Button>("button");
                headerElement.clicked += delegate
                {
                    value = !value;
                    visibilityToggle.value = value;
                };


                

                //Whenever we click the block toggle, turn them all on or off
                m_thisToggle.RegisterCallback<ChangeEvent<bool>>(e =>
                {
                    m_eventsToSkip = 0;
                    if (e.newValue == true || m_enabledTogglesAmount < m_contentToggles.Count)
                    {
                        m_enabledTogglesAmount = m_contentToggles.Count;
                        ChangeToggleState(ToggleState.Toggled);
                    }
                    else
                    {
                        m_enabledTogglesAmount = 0;
                        ChangeToggleState(ToggleState.Unchecked);
                    }

                    for (int i = 0; i < m_contentToggles.Count; i++)
                    {
                        if (m_contentToggles[i].value != e.newValue)
                        {
                            m_eventsToSkip++;
                            m_contentToggles[i].value = e.newValue;                            
                        }
                    }
                });
            });
        }

        public void UpdateDisplay(BuildTarget a_bt)
        {
            m_buildTargetRef = a_bt;
            //First pass to know if there are toggables and save them
            m_contentToggles = new List<Toggle>();
            Debug.Log("F");
            for (int i = 0; i < m_container.childCount; i++)
            {
                VisualElement ve = m_container.ElementAt(i);
                Toggle toggle = ve.Q<Toggle>(UIHelper.defaultToggleName);
                if (toggle != null)
                {
                    //Take parameter into account only if it's from the same platform as the current one
                    PlatformDependantPropertyEditor platformDependant = ve as PlatformDependantPropertyEditor;
                    if (platformDependant == null || platformDependant.buildTarget == a_bt)
                    {
                        //Debug.Log($"Poggers as {platformDependant.name}");
                        m_contentToggles.Add(toggle);
                        toggle.RegisterCallback<ChangeEvent<bool>>(WhenContentToggles);
                        if (toggle.value == true) m_enabledTogglesAmount++;
                    }
                }
            }

            //Depending on the toggables contents, choose a display for the block
            if (m_enabledTogglesAmount == 0)
            {
                ChangeToggleState(ToggleState.Unchecked);
            }
            else if (m_enabledTogglesAmount < m_contentToggles.Count)
            {
                ChangeToggleState(ToggleState.Partial);
            }
            else if (m_contentToggles.Count > 0)
            {
                ChangeToggleState(ToggleState.Toggled);
            }

        }

        /// <summary>
        /// Changes the state of the toggle of this block.
        /// </summary>
        /// <param name="nextState">The state to apply.</param>
        private void ChangeToggleState(ToggleState nextState)
        {
            switch (nextState)
            {
                //None of the children are toggled on
                case ToggleState.Unchecked:
                    {
                        //Without notifying so it doesn't retrigger a state to the children
                        m_thisToggle.SetValueWithoutNotify(false);

                        //Removes the toggle icon if there is one
                        m_partialToggleVE.style.display = DisplayStyle.None;

                        //Grays the label
                        if (!m_label.ClassListContains(m_disabledUSSName))
                            m_label.AddToClassList(m_disabledUSSName);
                    }
                    break;

                    //At least one (but not all) of the children is toggled on
                case ToggleState.Partial:
                    {
                        m_thisToggle.SetValueWithoutNotify(false);

                        //Changes the toggle icon to show a partial completed icon. Not empty, but not toggled on either
                        m_partialToggleVE.style.display = DisplayStyle.Flex;

                        if (m_label.ClassListContains(m_disabledUSSName))
                            m_label.RemoveFromClassList(m_disabledUSSName);
                    }
                    break;

                    //Every children is toggled on
                case ToggleState.Toggled:
                    {
                        m_thisToggle.SetValueWithoutNotify(true);

                        m_partialToggleVE.style.display = DisplayStyle.None;

                        if (m_label.ClassListContains(m_disabledUSSName))
                            m_label.RemoveFromClassList(m_disabledUSSName);
                    }
                    break;
                default:
                    {
                        throw new SmartTale.Framework.Core.Runtime.Exceptions.SmartTaleException("There shouldn't be other Toggle states.");
                    }
            }
        }

        /// <summary>
        /// Method called whenever one of the properties inside this box has been toggled on or off
        /// </summary>
        /// <param name="a_event">The event information</param>
        private void WhenContentToggles(ChangeEvent<bool> a_event)
        {
            //The reason to use an int or counter is because the events are asynchrone, and thus simply a boolean wouldn't work
            //What I mean is, after the headerToggle event ends, the events from every toggle in the contents children haven't necessarily been called/completed. Some have, some haven't.
            //So instead, I use an int to make sure that every toggle event goes through here and are therefore discarded.
            //It is safe because we wouldn't need to be lightning fast to trigger the "enable/disable all" toggle AND one of the children toggles before they've been completed for it to be an issue
            if (m_eventsToSkip > 0)
            {                
                m_eventsToSkip--;
                return;
            }

            //At least one of the values is set to true when there were none
            else if (m_enabledTogglesAmount == 0 && a_event.newValue == true)
            {
                if (m_contentToggles.Count > 1)
                    ChangeToggleState(ToggleState.Partial);
                else ChangeToggleState(ToggleState.Toggled);
            }

            //Just manually turned them all off
            else if (m_enabledTogglesAmount == 1 && a_event.newValue == false)
            {
                ChangeToggleState(ToggleState.Unchecked);
            }

            //Just manually turned them all on
            else if (m_enabledTogglesAmount == m_contentToggles.Count - 1 && a_event.newValue == true)
            {
                ChangeToggleState(ToggleState.Toggled);
            }

            //They were all toggled but now one is unchecked
            else if (m_enabledTogglesAmount == m_contentToggles.Count && a_event.newValue == false)
            {
                ChangeToggleState(ToggleState.Partial);
            }

            //else it's a regular change of value
            m_enabledTogglesAmount += (a_event.newValue ? 1 : -1);
        }

        #region UXML
        public new class UxmlFactory : UxmlFactory<BlockToggleEditor, UxmlTraits> { }

        public new class UxmlTraits : VisualElement.UxmlTraits
        {
            UxmlStringAttributeDescription m_Label = new UnityEngine.UIElements.UxmlStringAttributeDescription
            {
                name = "custom-label",
                defaultValue = "My Splendid label!",
                use = UxmlAttributeDescription.Use.Required
            };

            public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
            {
                var blockInstance = ve as BlockToggleEditor;
                blockInstance.labelToDisplay = m_Label.GetValueFromBag(bag, cc).ToUpper();
                base.Init(ve, bag, cc);
            }
        }
        #endregion
    }
}
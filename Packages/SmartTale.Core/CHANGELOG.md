﻿# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.3.0-preview.3 - 2020-07-20

### Added

- RequiredInterface attribute and drawer
- ReadOnlyField drawer

### Removed

- EnumAction attribute

## 0.1.0-preview.1 - 2020-06-29

### Added

- EnumAction attribute
- MinMaxSlider attribute
- ReadOnlyField attribute
- InvalidOrderList exception
- NegativeFloat exception
- LinkedList extensions
- List extensions
- Vector3 extensions
- UnityEvents generic serialized types

## 0.0.0-preview.1 - 2020-06-26

### Added

- Required files for a package UPM
- Singleton

﻿namespace SmartTale.Framework.Core.Tests.Utilities.Extensions
{
    using Runtime.Utilities;
    using NUnit.Framework;
    using Runtime.Exceptions;
    using UnityEngine;

    public class Vector3ExtensionsTests
    {
        [Test]
        public void IsNearToPasses()
        {
            // If the epsilon tolerance is negative
            Assert.Throws<NegativeFloatException>(() =>
            {
                Vector3.zero.IsNearTo(Vector3.one, -5f);
            });
            
            // 0f parameter will output a Debug.LogWarning, disable temporarily the logger for Unit testing
            Debug.unityLogger.logEnabled = false;
            Assert.IsFalse(Vector3.zero.IsNearTo(Vector3.zero, 0f));
            Debug.unityLogger.logEnabled = true;
            
            Assert.IsTrue(new Vector3(0, 0.1f, 0f).IsNearTo(new Vector3(0, 0.15f, 0), 0.2f));
            
            Assert.IsFalse(new Vector3(0, 0.1f, 0f).IsNearTo(new Vector3(0, 0.2f, 0), 0.05f));
            
            // TODO_BG: Write more tests
        }
    }
}
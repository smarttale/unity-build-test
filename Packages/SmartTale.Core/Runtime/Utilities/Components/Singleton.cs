﻿namespace SmartTale.Framework.Core.Runtime.Utilities
{
	using System;
	using UnityEngine;

	/// <remarks>
	/// Be aware this will not prevent a non singleton constructor such as `T myT = new T();`
	/// To prevent that, add `protected T () {}` to your singleton class.
	/// </remarks>
	public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
	{
		#region FIELDS
		#region Serialized
		[SerializeField] private bool m_isPersistent = false;
		#endregion Serialized
		
		#region Internals
		private static T _instance = null;
		private static bool _isApplicationQuitting = false;
		#endregion Internals
		
		#region Contants
		private static readonly object LOCK = new object();
		#endregion Contants
		#endregion FIELDS

		#region Events
		public static event Action<T> InstanceCreated = null;
		#endregion Events

		#region Properties
		/// <summary>
		/// Return the singleton instance
		/// </summary>
		public static T Instance
		{
			get
			{
				lock (LOCK)
				{
					#if UNITY_EDITOR
					if ((Application.isPlaying == false || _isApplicationQuitting) && _instance != null)
					{
						_isApplicationQuitting = Application.isPlaying;
						return _instance;
					}

					if (_instance != null)
                    {
                        return _instance;
                    }

					_instance = FindObjectOfType<T>();
					if (_instance != null)
					{
						try
						{
							if (Application.isEditor == false)
							{
								DontDestroyOnLoad(_instance.gameObject);
							}
						}
						catch (Exception e)
						{
							Debug.LogError(e.ToString());
						}
					}

					if (FindObjectsOfType<T>().Length <= 1) return _instance;

					Debug.LogError("[Singleton] Something went really wrong - there should never be more than 1 singleton! Reopening the scene might fix it.");
					#endif

					return _instance;
				}
			}
		}

		/// <summary>
		/// Check if an instance has been created
		/// </summary>
		public static bool HasInstance => _instance != null;
		#endregion Properties

		#region METHODS
		#region Lifecycle
		protected virtual void Awake()
		{
			// If there's no instance yet make one
			if (!HasInstance)
			{
				_instance = this as T;
				InstanceCreated?.Invoke(_instance);

				if (m_isPersistent)
				{
					DontDestroyOnLoad(gameObject);
				}
			}
			// Else destroy the GameObject
			else if (_instance != this)
			{
				if (_instance == this)
					return;

				Debug.Log($"An instance of this Singleton GameObject already exists ! Destroying {gameObject.name}...", gameObject);
				Destroy(gameObject);
			}

		}

		protected virtual void OnEnable()
		{
			_isApplicationQuitting = false;
		}

		/// <remarks>
		/// When Unity quits, it destroys objects in a random order.
		/// In principle, a Singleton is only destroyed when application quits.
		/// If any script calls Instance after it have been destroyed,
		///   it will create a buggy ghost object that will stay on the Editor scene
		///   even after stopping playing the Application. Really bad!
		/// So, this was made to be sure we're not creating that buggy ghost object.
		/// </remarks>
		protected virtual void OnDestroy()
		{
			// Verify we are the one instance heir to the Singleton, and not one of those quickly discarded copies from a fresh loaded scene
			if (_instance == this as T)
			{
				_isApplicationQuitting = true;
				_instance = null;
			}
		}
		#endregion Lifecycle
		#endregion METHODS
	}
}
﻿namespace SmartTale.Framework.Core.Runtime.Utilities
{
	using UnityEngine;
	
	/// <summary>
	/// Use this component to highlight placeholder assets 
	/// </summary>
	[HelpURL("https://smart-tale.atlassian.net/l/c/0mwm2i50"), AddComponentMenu("SmartTale/Framework/Utilities/Placeholder")]
	public class Placeholder : MonoBehaviour { }
}
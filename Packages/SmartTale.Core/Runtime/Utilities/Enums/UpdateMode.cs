﻿namespace SmartTale.Framework.Core.Runtime.Utilities.Enums
{
    /// <summary>
    /// Used to control Unity update mode of a MonoBehavior
    /// </summary>
    public enum UpdateMode
    {
        Update = 0,
        FixedUpdate = 1,
        LateUpdate = 2
    }
}
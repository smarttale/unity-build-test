﻿namespace SmartTale.Framework.Core.Runtime.Utilities
{
    using Runtime.Exceptions;
    using UnityEngine;

    public static class Vector3Extensions
    {
        /// <summary>
        /// Check if a position is near to an other
        /// </summary>
        /// <param name="a_this"></param>
        /// <param name="a_to"></param>
        /// <param name="a_tolerance">Strictly positive distance tolerance</param>
        /// <returns></returns>
        public static bool IsNearTo(this Vector3 a_this, Vector3 a_to, float a_tolerance)
        {
            if (a_tolerance < 0) throw new NegativeFloatException();
            if (a_tolerance == 0) Debug.LogWarning("A zero float epsilon will always return false"); 
            return Vector3.Distance(a_this, a_to) < a_tolerance;
        }
    }
}
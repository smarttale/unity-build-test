﻿namespace SmartTale.Framework.Core.Runtime.Utilities.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Exceptions;
    using Random = UnityEngine.Random;

    public static class ListExtensions
    {
        /// <summary>
        /// Get a random item from the List
        /// </summary>
        /// <param name="a_list"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T RandomItem<T>(this List<T> a_list)
        {
            if(a_list.Count <= 0) throw new ArgumentOutOfRangeException();
            int random = Random.Range(0, a_list.Count);
            return a_list[Random.Range(0, a_list.Count)];
        }

        /// <summary>
        /// Shuffle the items of the List
        /// </summary>
        /// <param name="a_list"></param>
        /// <typeparam name="T"></typeparam>
        public static void Shuffle<T>(this List<T> a_list)
        {
            T[] temp = new T[a_list.Count];
            int cpt = 0;
            while (a_list.Count > 0)
            {
                int random = Random.Range(0, a_list.Count);
                temp[cpt] = a_list[random];
                a_list.RemoveAt(random);
                cpt++;
            }
            a_list.AddRange(temp);
        }

        /// <summary>
        /// Order by a list of integer priority
        /// </summary>
        /// <param name="a_list"></param>
        /// <param name="a_orderList"></param>
        /// <typeparam name="T"></typeparam>
        /// <remarks>If items have the same priority, order is preserved</remarks>
        public static void ReOrder<T>(this List<T> a_list, List<int> a_orderList)
        {
            if (a_list.Count != a_orderList.Count) throw new InvalidOrderListException();

            // TODO: Find a more efficient way to do the sort
            List<Tuple<T, int>> sortingTuple = a_list.Select((a_item, a_index) => new Tuple<T, int>(a_item, a_orderList[a_index])).ToList();

            Tuple<T, int>[] orderedResult = sortingTuple.OrderBy((a_tuple) => a_tuple.Item2).ToArray();

            // In order to modify the array, we must change it's elements directly (can't modify the reference)
            for (int i = 0; i < a_list.Count; i++)
            {
                a_list[i] = orderedResult[i].Item1;
            }
        }
    }
}
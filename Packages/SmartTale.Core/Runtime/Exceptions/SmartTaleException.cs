﻿namespace SmartTale.Framework.Core.Runtime.Exceptions
{
	using System;

	public class SmartTaleException : Exception
	{
		public SmartTaleException()
		{
		}

		public SmartTaleException(string message)
			: base(message)
		{
		}

		public SmartTaleException(string message, Exception inner)
			: base(message, inner)
		{
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class InteractableManager : MonoBehaviour
{
    public UnityEngine.UI.Image m_Image;
    public Color m_ColorA = Color.blue;
    public Color m_ColorB = Color.red;
    public UnityEngine.UI.Text m_Text;
    public AssetReference m_SpriteA;
    public AssetReference m_SpriteB;

    private bool colorToggle = false;
    private bool imageToggle = false;

    private static InteractableManager sInstance;
    private void Awake()
    {
        sInstance = this;
        // m_Image.color = m_ColorA;
        //m_Image.color = _colorA.color;
#if UNITY_PS4
        ChangeText("PS4");
        #elif UNITY_STANDALONE
        ChangeText("PC");
        #elif UNITY_XBOXONE
        ChangeText("XBOX ONE");

        #endif
    }



    private void Update()
    {
#if UNITY_PS4
       
        if (Input.GetKeyDown(KeyCode.Joystick1Button0)) ChangeImage();

#elif UNITY_STANDALONE
        ChangeText("PC");
        if (Input.GetKeyDown(KeyCode.Space)) ChangeImage();
#elif UNITY_XBOXONE
       if (Input.GetKeyDown(KeyCode.Joystick1Button1)) ChangeImage();

#endif
        //if (Input.GetKeyDown(KeyCode.Joystick1Button0) || Input.GetKeyDown(KeyCode.Space)) ChangeColor();
    }

    public void ChangeColor()
    {
        // if (colorToggle) m_Image.color = m_ColorA.color;
        // else m_Image.color = m_ColorB.color;
        //else m_Image.color = _colorB.color;
        colorToggle = !colorToggle;
    }



    
    public void ChangeImage()
    {
        Addressables.LoadAssetAsync<Sprite>(imageToggle ? m_SpriteA : m_SpriteB).Completed += ActualImageChange;
        imageToggle = !imageToggle;
        m_Text.color = imageToggle ? m_ColorA : m_ColorB;
    }
    private void ActualImageChange(AsyncOperationHandle<Sprite> image)
    {
        m_Image.sprite = image.Result;
    }

    public static void ChangeText(string newText)
    {
        sInstance.m_Text.text = newText;
    }

  
    
}

﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.IO;
using UnityEditor;
using SmartTale.Framework.Attributes;

public class TestPlayerSettings : MonoBehaviour
{
    public Material mat;
    public Texture2D tex;
    public string Name = "Low";
    public Settings.Options publicOption;

    string _path
    {
        get
        {
            string basePath = $"JSONS/{Name}.json";
            if (Directory.Exists(basePath) == false)
            {
                Directory.CreateDirectory("JSONS");
            }
            return basePath;
        }
    }

    public class Settings
    {

        public string SettingName = "Huhu";
        public Texture2D settingTex;
        public enum Options
        {
            OptionA,
            OptionB,
            None
        }
        public Options option = Options.OptionB;
    }

    //[NaughtyAttributes.Button(enabledMode: NaughtyAttributes.EButtonEnableMode.Editor)]
    
    public void SaveSettings()
    {
        Settings settings = new Settings();
        settings.SettingName = Name;
        settings.settingTex = tex;
        settings.option = publicOption;
        string toSave = JsonUtility.ToJson(settings, true);

        File.WriteAllText(_path, toSave);

        

    }

    //[NaughtyAttributes.Button()]
    public void ReadJSON()
    {
        string toRead = File.ReadAllText(_path);
        Settings settings = JsonUtility.FromJson(toRead, typeof(Settings)) as Settings;
        Debug.Log(settings.SettingName + settings.option);
        mat.SetTexture("_MainTex", settings.settingTex);

    }
} 
#endif

using System.Collections.Generic;
using UnityEngine;

namespace SmartTale.Framework.Attributes.Test
{
	public class _NaughtyComponent : MonoBehaviour
	{
	}

	[System.Serializable]
	public class MyClass
	{
	}

	[System.Serializable]
	public struct MyStruct
	{
	}
}

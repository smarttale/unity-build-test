﻿namespace SmartTale.Framework.Attributes
{
    using System;
    using UnityEngine;
    
    /// <summary>
    /// For SerializedField 
    /// </summary>
    /// <example>
    /// public class InterfaceExample : MonoBehaviour
    /// </example>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class RequireInterfaceAttribute : DrawerAttribute
    {
        public Type RequiredType { get; private set; }

        public RequireInterfaceAttribute(Type a_type) { RequiredType = a_type; }
    }
}
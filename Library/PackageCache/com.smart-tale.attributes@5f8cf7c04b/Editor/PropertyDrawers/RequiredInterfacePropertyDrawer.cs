﻿using UnityEngine;
using UnityEditor;

namespace SmartTale.Framework.Attributes.Editor
{
	[CustomPropertyDrawer(typeof(RequireInterfaceAttribute))]
	public class RequireInterfacePropertyDrawer : PropertyDrawerBase
	{
		protected override float GetPropertyHeight_Internal(SerializedProperty property, GUIContent label)
		{
			return GetPropertyHeight(property);
		}

		protected override void OnGUI_Internal(Rect rect, SerializedProperty property, GUIContent label)
		{
			if (property.propertyType == SerializedPropertyType.ObjectReference)
			{
				RequireInterfaceAttribute requireInterfaceAttribute = attribute as RequireInterfaceAttribute;
				EditorGUI.BeginProperty(rect, label, property);
				if (requireInterfaceAttribute != null)
					property.objectReferenceValue = EditorGUI.ObjectField(rect, label,
						property.objectReferenceValue, requireInterfaceAttribute.RequiredType, true);
				EditorGUI.EndProperty();
			}
			else
			{
				// If field is not reference, show error message.
				// Save previous color and change GUI to red.
				Color previousColor = GUI.color;
				GUI.color = Color.red;
				// Display label with error message.
				EditorGUI.LabelField(rect, label, new GUIContent("Property is not a reference type"));
				// Revert color change.
				GUI.color = previousColor;
			}
		}
	}
}